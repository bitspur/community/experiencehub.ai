import { config } from 'app/config';
import { LocalAi } from 'app/engines/types';
import React, { useEffect, useState } from 'react';
import { LocalAiApi, LocalAiProvider as OriginalLocalAiProvider } from 'app/engines/localai';
import { useModels } from 'app/state/models';

export function LocalAiProvider({ children, localAi, ...props }: { children: React.ReactNode; localAi?: LocalAi }) {
  const [localAiInstance, setLocalAiInstance] = useState<LocalAi | undefined>(undefined);
  const baseURL = config.get('LOCALAI_BASE_URL');
  const modelsState = useModels();

  useEffect(() => {
    if (!baseURL || localAiInstance) return;
    (async () => {
      const { instance, models } = await LocalAiApi.connect({ baseURL });
      setLocalAiInstance(instance);
      modelsState.setLocalAiModels(models);
    })();
  }, [baseURL]);

  if (!localAiInstance) return <>{children}</>;
  return (
    <OriginalLocalAiProvider {...props} localAi={localAiInstance}>
      {children}
    </OriginalLocalAiProvider>
  );
}
