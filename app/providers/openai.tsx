import { config } from 'app/config';
import { OpenAIApis } from 'app/engines/openai';
import { OpenAi } from 'app/engines/types';
import React, { useEffect, useState } from 'react';
import { OpenAiProvider as OriginalOpenAiProvider } from 'app/engines/openai';
import { useApiKeys } from 'app/state/apiKeys';
import { KeyTypes } from 'app/state/types';
import { useModels } from 'app/state/models';

export function OpenAiProvider({ children, openAi, ...props }: { children: React.ReactNode; openAi?: OpenAi }) {
  const [openAiInstance, setOpenAiInstance] = useState<OpenAIApis | undefined>(undefined);
  const apiKeysState = useApiKeys();
  const baseURL = config.get('OPENAI_BASE_URL');
  const modelsState = useModels();

  useEffect(() => {
    const apiKey = apiKeysState.keys.find((key) => key.name === KeyTypes.OPENAI)?.key;
    if (!apiKey || openAiInstance || !baseURL) return;
    (async () => {
      const { instance, models } = await OpenAIApis.connect({ apiKey, baseURL });
      setOpenAiInstance(instance);
      modelsState.setOpenAiModels(models);
    })();
  }, [baseURL, apiKeysState.keys.length]);

  if (!openAiInstance) return <>{children}</>;
  return (
    <OriginalOpenAiProvider openAi={openAiInstance} {...props}>
      {children}
    </OriginalOpenAiProvider>
  );
}
