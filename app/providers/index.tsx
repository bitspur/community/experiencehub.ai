import React, { Suspense } from 'react';
import type { KeycloakProviderProps } from './keycloak';
import type { ProviderProps } from './types';
import type { TamaguiInternalConfig } from 'ui';
import type { TamaguiProviderProps } from './tamagui';
import { KeycloakProvider } from './keycloak';
import { NavigationProvider } from './navigation';
import { TamaguiProvider } from './tamagui';
import { OpenAiProvider } from './openai';
import { SolitoImageProvider } from 'solito/image';
import { LocalAiProvider } from './localai';
import { HuggingFaceProvider } from './huggingFace';

export type GlobalProviderKeycloak = Omit<KeycloakProviderProps, 'disabled' | 'cookies' | 'children'>;

export type GlobalProviderProps = ProviderProps &
  Omit<TamaguiProviderProps, 'config'> & {
    cookies?: unknown;
    keycloak?: GlobalProviderKeycloak;
    tamaguiConfig?: TamaguiInternalConfig;
  };

export function GlobalProvider({ children, keycloak, cookies, tamaguiConfig, ...props }: GlobalProviderProps) {
  return (
    <KeycloakProvider disabled={!keycloak} cookies={cookies} {...keycloak}>
      <OpenAiProvider>
        <LocalAiProvider>
          <HuggingFaceProvider>
            <SolitoImageProvider
              loader={({ src }) => {
                return src;
              }}
            >
              <NavigationProvider>
                <TamaguiProvider config={tamaguiConfig} {...props}>
                  <Suspense>{children}</Suspense>
                </TamaguiProvider>
              </NavigationProvider>
            </SolitoImageProvider>
          </HuggingFaceProvider>
        </LocalAiProvider>
      </OpenAiProvider>
    </KeycloakProvider>
  );
}

export * from './navigation';
export * from './tamagui';
export * from './types';
