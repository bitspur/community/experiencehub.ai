import React, { useState, useEffect } from 'react';
import { HuggingFace } from 'app/engines/types';
import { HuggingFaceApi, HuggingFaceProvider as OriginalHuggingFaceProvider } from 'app/engines/huggingFace';
import { config } from 'app/config';

export function HuggingFaceProvider({ children }: { children: React.ReactNode }) {
  const [huggingFaceInstance, setHuggingFaceInstance] = useState<HuggingFace | undefined>(undefined);
  const baseURL = config.get('HUGGINGFACE_BASE_URL');
  useEffect(() => {
    if (!baseURL || huggingFaceInstance) return;
    const instance = HuggingFaceApi.connect({ baseURL });
    setHuggingFaceInstance(instance);
  }, [baseURL]);

  if (!huggingFaceInstance) return <>{children}</>;
  return <OriginalHuggingFaceProvider huggingFace={huggingFaceInstance}>{children}</OriginalHuggingFaceProvider>;
}
