/*
 *  File: /protocols/openai/image-generation.ts
 *  Project: app
 *  File Created: 25-08-2023 12:02:24
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 30-08-2023 00:41:51
 *  Modified By: Lalit Rajak
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Environment } from 'app/environments';
import { IImageGenerationProtocol, ImageGenerationConfig } from '../image-generation';

export class OpenAiImageGenerationProtocol implements IImageGenerationProtocol {
  constructor(private environment: Environment, private config: ImageGenerationConfig) {}

  async generateImage(): Promise<string[]> {
    return (await this.environment.api.post('v1/images/generations', this.config)).data.data.map((url) => url.url);
  }
}
