/*
 *  File: /protocols/openai/chat.ts
 *  Project: app
 *  File Created: 19-08-2023 12:02:59
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 14-09-2023 10:25:39
 *  Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import { Environment } from 'app/environments';
import { ChatConfig, IChatProtocol } from '../chat';

export class OpenAIChatProtocol implements IChatProtocol {
  constructor(private environment: Environment, private config: ChatConfig) {}

  async chatCompletion(): Promise<any> {
    return (await this.environment.api.post('v1/chat/completions', this.config)).data;
  }
}
