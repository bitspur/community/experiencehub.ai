/*
 *  File: /protocols/proxy/chat.ts
 *  Project: app
 *  File Created: 19-08-2023 12:03:52
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 14-09-2023 10:25:16
 *  Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ProxyEnvironment } from 'app/environments';
import { IChatProtocol, ChatConfig } from '../chat';

export class ProxyChatProtocol implements IChatProtocol {
  constructor(private environment: ProxyEnvironment, private config: ChatConfig) {}

  async chatCompletion(): Promise<any> {
    return (await this.environment.api.post('/chat', this.config)).data;
  }
}
