/*
 *  File: /protocols/image-generation.ts
 *  Project: app
 *  File Created: 25-08-2023 12:01:37
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 13-09-2023 18:18:44
 *  Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { OpenAiImageGenerationProtocol } from './openai/image-generation';
import { ProxyImageGenerationProtocol } from './proxy/image-generation';
import { IChat } from 'app/screens/chat';
import { Protocol } from './protocol';
import { useEffect, useState } from 'react';
import { Environment } from 'app/environments';

export class ImageGenerationProtocol implements ImageGenerationHandler {
  constructor() {}
  async autoGenerate(environment: Environment, config: ImageGenerationConfig): Promise<IChat | undefined> {
    const protocol = this.getProtocol(environment, config);
    if (!protocol.generateImage) return;
    const images = await protocol.generateImage(config);
    return {
      id: Protocol.generateRandomString(16),
      role: 'bot',
      experience: 'image-generation',
      images,
    };
  }

  protected getProtocol(environment: Environment, config: ImageGenerationConfig): IImageGenerationProtocol {
    switch (environment.name) {
      case 'openai':
        return new OpenAiImageGenerationProtocol(environment, config);
      default:
        return new ProxyImageGenerationProtocol(environment, config);
    }
  }
}

export function useImageGenerationProtocol() {
  const [protocol, setProtocol] = useState<ImageGenerationHandler | undefined>(undefined);
  useEffect(() => {
    setProtocol(new ImageGenerationProtocol());
  }, []);
  return protocol;
}

export interface ImageGenerationHandler {
  autoGenerate(environment: Environment, config: ImageGenerationConfig): Promise<IChat | undefined>;
}

export interface IImageGenerationProtocol {
  generateImage?(config: ImageGenerationConfig): Promise<string[]>;
}

export interface ImageGenerationConfig {
  prompt: string;
  n?: NumberOfImages;
  size?: ImageSize;
}

export enum ImageSize {
  'SMALL' = '256x256',
  'MEDIUM' = '512x512',
  'LARGE' = '1024x1024',
}

export enum NumberOfImages {
  'ONE' = 1,
  'TWO' = 2,
  'THREE' = 3,
  'FOUR' = 4,
  'FIVE' = 5,
  'SIX' = 6,
  'SEVEN' = 7,
  'EIGHT' = 8,
  'NINE' = 9,
  'TEN' = 10,
}
