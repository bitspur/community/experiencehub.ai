/*
 *  File: /protocols/chat.ts
 *  Project: app
 *  File Created: 19-08-2023 11:59:52
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 14-09-2023 15:44:45
 *  Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import { OpenAIChatProtocol } from './openai/chat';
import { LocalAIChatProtocol } from './localai/chat';
import { ProxyChatProtocol } from './proxy/chat';
import { IChat } from 'app/screens/chat';
import { useEffect, useState } from 'react';
import { Environment } from 'app/environments';
import { Role } from 'app/utils';

export class ChatProtocol implements IChatHandler {
  constructor() {}
  async autoGenerate(environment: Environment, config: ChatConfig): Promise<IChat | undefined> {
    const protocol = this.getProtocol(environment, config);
    if (!protocol.chatCompletion) return;
    const response = await protocol.chatCompletion();
    return {
      id: response.id,
      prompt: response.choices[0].message.content,
      role: 'assistant',
      experience: 'text-generation',
    };
  }

  protected getProtocol(environment: Environment, config: ChatConfig): IChatProtocol {
    switch (environment.name) {
      case 'openai':
        return new OpenAIChatProtocol(environment, config);
      case 'localai':
        return new LocalAIChatProtocol(environment, config);
      default:
        return new ProxyChatProtocol(environment, config);
    }
  }
}

export function useChatProtocol() {
  const [protocol, setProtocol] = useState<IChatHandler | undefined>(undefined);
  useEffect(() => {
    setProtocol(new ChatProtocol());
  }, []);
  return protocol;
}

export interface ChatConfig {
  model: string;
  temperature: number;
  messages: { role: Role; content: string }[];
}

export interface IChatProtocol {
  chatCompletion?(): Promise<any>;
}

export interface IChatHandler {
  autoGenerate(environment: Environment, config: ChatConfig): Promise<any | undefined>;
}
