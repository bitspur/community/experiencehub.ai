import React from 'react';
import type { PropsWithChildren } from 'react';
import { createContext, useContext } from 'react';
import { OpenAi } from '../types';

const OpenAiContext = createContext<OpenAi | undefined>(undefined);

export interface OpenAiProps extends PropsWithChildren {
  openAi: OpenAi;
}

export function useOpenAi() {
  return useContext(OpenAiContext);
}

export function OpenAiProvider({ children, openAi }: OpenAiProps) {
  return <OpenAiContext.Provider value={openAi}>{children}</OpenAiContext.Provider>;
}
