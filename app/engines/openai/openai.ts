/*
 *  File: /engines/openai/openai.ts
 *  Project: app
 *  File Created: 30-08-2023 00:34:46
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 01-09-2023 15:40:24
 *  Modified By: Lalit Rajak
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import { Configuration, OpenAIApi } from 'openai';
import axios from 'axios';
import { OpenAi, ImageGeneratorRequestBody, chatRequest, EnginesConfig, AudioToTextRequestBody } from '../types';
import { AxiosInstance } from 'axios';

const logger = console;

export class OpenAIApis implements OpenAi {
  private api: AxiosInstance;

  static async connect(config: EnginesConfig) {
    if (!config.apiKey) throw new Error('OpenAI API Key not found');
    const configuration = new Configuration({
      organization: 'org-jwLZs0441pYsEFFlJToIscQh',
      apiKey: config.apiKey,
    });
    const api = new OpenAIApi(configuration);
    try {
      const models = await api.listModels();
      if (models.status === 200 || models.statusText.toLowerCase() === 'ok') logger.info('INFO: Connected to OpenAI');
      return {
        instance: new OpenAIApis(config),
        models: models.data.data.map((model) => model.id) as string[],
      };
    } catch (error) {
      console.error('Error occurred while fetching engine list:', error);
      throw error;
    }
  }

  constructor(config: EnginesConfig) {
    this.api = axios.create({
      baseURL: config.baseURL,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${config.apiKey}`,
      },
    });
  }

  async getChatResponse(requestData: chatRequest) {
    return (await this.api.post('v1/chat/completions', requestData)).data;
  }

  async getGeneratedImages(requestBody: ImageGeneratorRequestBody) {
    return (await this.api.post('v1/images/generations', requestBody)).data.data.map((url) => url.url);
  }

  async getAudioToText(requestData: AudioToTextRequestBody) {
    return (
      await this.api.post('v1/audio/translations', requestData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
    ).data;
  }
}
