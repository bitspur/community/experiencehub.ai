import React from 'react';
import type { PropsWithChildren } from 'react';
import { createContext, useContext } from 'react';
import { LocalAi } from '../types';

const LocalAiContext = createContext<LocalAi | undefined>(undefined);

export interface LocalAiProps extends PropsWithChildren {
  localAi: LocalAi;
}

export function useLocalAi() {
  return useContext(LocalAiContext);
}

export function LocalAiProvider({ children, localAi }: LocalAiProps) {
  return <LocalAiContext.Provider value={localAi}>{children}</LocalAiContext.Provider>;
}
