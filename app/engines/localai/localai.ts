/*
 *  File: /engines/localai/localai.ts
 *  Project: app
 *  File Created: 01-08-2023 12:40:21
 *  Author: Lalit Rajak
 *  -----
 * Last Modified: Fr-10-2023 15:22:32
 * Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Configuration, OpenAIApi } from 'openai';
import { EnginesConfig, LocalAi, chatRequest } from '../types';
import axios, { AxiosInstance } from 'axios';

const logger = console;

export class LocalAiApi implements LocalAi {
  baseUrl: string;
  private api: AxiosInstance;

  static async connect(config: EnginesConfig) {
    const configuration = new Configuration({
      basePath: `${config.baseURL}/v1`,
    });
    // const localAi = new OpenAIApi(configuration);
    // const models =  await localAi.listModels();
    // if (models.status === 200 || models.statusText.toUpperCase() === 'OK') logger.log('INFO: Connected to LocalAI');
    // return { instance: new LocalAiApi(config), models: models.data.data.map((model) => model.id) as string[] };
    return { instance: new LocalAiApi(config), models: [] as string[] };
  }

  constructor(config: EnginesConfig) {
    this.api = axios.create({
      baseURL: config.baseURL,
    });
  }

  async getChatResponse(requestData: chatRequest) {
    return (await this.api.post('v1/chat/completions', requestData)).data;
  }
}
