/*
 *  File: /engines/types.ts
 *  Project: app
 *  File Created: 21-08-2023 10:16:09
 *  Author: Lavanya Katari
 *  -----
 *  Last Modified: 14-09-2023 15:47:37
 *  Modified By: Clay Risser
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { OpenAiChatCompletions } from 'app/utils';

export interface OpenAi {
  getChatResponse: (requestData: chatRequest) => any;
  getGeneratedImages: (chatGptImageGeneratorRequestBody: ImageGeneratorRequestBody) => any;
  getAudioToText: (requestData: AudioToTextRequestBody) => any;
}

export interface LocalAi {
  getChatResponse: (requestData: chatRequest) => any;
  getGeneratedImages?: (chatGptImageGeneratorRequestBody: ImageGeneratorRequestBody) => any;
}

export interface HuggingFace {
  getChatResponse: (requestData: chatRequest) => any;
  getGeneratedImages: (chatGptImageGeneratorRequestBody: ImageGeneratorRequestBody) => Promise<string>;
  getAudioResponse: (audioData: audioRequest) => any;
  getImageResponse: (blob: Blob) => any;
  getImageSegmentationResponse: (blob: Blob) => any;
  getObjectDetectionResponse: (blob: Blob) => any;
  getAnswerFromQuestion: (requestData: questionAnswerRequest) => any;
  getTextClassificationResponse: (requestData: TextClassifierResponse) => any;
  getSummarizationResponse: (requestBody: SummerizationInputRequest) => any;
  getFillMaskResponse: (requestData: fillMaskResponse) => any;
  getZeroShotClassificationResponse: (requestData: zeroShotClassifierResponse) => any;
  getAudioClassificationResponse: (blob: Blob) => any;
  getZeroShotImageClassificationResponse: (requestData: zeroShotImageClassifierResponse) => any;
  getTranslatedText: (requestData: TranslatedTextData) => any;
  getConversationResponse: (requestData: conversationResponse) => any;
  getSentenceSimilarity: (config: SentenceSimilarityConfig) => any;
  getTokenClassification: (requestData: fillMaskResponse) => any;
  getTableQuestionAnsweringResponse: (requestBody: TableQuestionAnsweringResponse) => any;
  getAutomaticSpeechRecognition: (blob: Blob) => any;
  getDocumentQuestionAnsweringResponse: (body: DocumentQuestionAnsweringRequestBody) => any;
  getFeatureExtraction: (requestBody: featureExtractionRequestBody) => any;
  getTextGeneration: (requestBody: getTextGenerationResponseBody) => any;
  getImageToTextResponse: (body: getImageToTextRequest) => any;
  getImageToImageResponse: (blob: Blob) => any;
  getVisualQuestionAnsweringResponse: (body: VisualQuestionAnsweringRequestBody) => any;
}

export interface DocumentQuestionAnsweringRequestBody {
  inputs: {
    image: string;
    question: string;
  };
}

export interface VisualQuestionAnsweringRequestBody {
  inputs: {
    image: string;
    question: string;
  };
}

export interface SentenceSimilarityConfig {
  inputs: {
    sentences: string[];
    source_sentence: string;
  };
}

export interface conversationResponse {
  inputs: string;
  options: {
    use_cache?: false;
  };
}

export interface TranslatedTextData {
  inputs: string;
  sourceLanguageCode: string;
  targetLanguageCode: string;
}

export interface chatRequest {
  model: string;
  messages: [{ role: string; content: string }];
  temperature: number;
}

export interface TextClassifierResponse {
  prompt: string;
}

export interface getImageToTextRequest {
  inputs: {
    image: string | undefined;
  };
}

export interface SummerizationInputRequest {
  prompt: string;
}

export interface zeroShotImageClassifierResponse {
  image: string;
  parameters: {
    candidate_labels: string;
  };
}

export interface zeroShotClassifierResponse {
  inputs: string;
  parameters: {
    candidate_labels: string;
    multi_class?: boolean;
  };
}

export interface getTextGenerationResponseBody {
  inputs: string;
}

export interface fillMaskResponse {
  prompt: string;
}

export interface featureExtractionRequestBody {
  inputs: string;
}

export interface audioRequest {
  prompt: string;
}

export interface audioRequestOutputBody {
  prompt: Blob | null;
}

export interface questionAnswerRequest {
  question: string;
  context: string;
}

export interface ImageClassifierRequestBody {
  file: ReadableStream;
}

export interface TableQuestionAnsweringResponse {
  prompt: string;
  table: {
    Repository: string[];
    Stars: string[];
    Contributors: string[];
    ProgrammingLanguage: string[];
  };
}

export interface Model {
  name: string;
  value: string;
}

export interface ImageGeneratorRequestBody {
  prompt: string;
  model: OpenAiChatCompletions | HuggingFaceModel | LocalAiModel;
  n?: NumberOfImages;
  size?: ImageSize;
  temperature?: number;
}

export interface AudioToTextRequestBody {
  file: ReadableStream;
  model: string;
}

export interface ObjectDetectionRequestBody {
  file: ReadableStream;
}

export enum ImageSize {
  'SMALL' = '256x256',
  'MEDIUM' = '512x512',
  'LARGE' = '1024x1024',
}

export enum NumberOfImages {
  'ONE' = 1,
  'TWO' = 2,
  'THREE' = 3,
  'FOUR' = 4,
  'FIVE' = 5,
  'SIX' = 6,
  'SEVEN' = 7,
  'EIGHT' = 8,
  'NINE' = 9,
  'TEN' = 10,
}

export enum Engines {
  'OPENAI' = 'openai',
  'LOCALAI' = 'localai',
  'HUGGINGFACE' = 'huggingface',
}

export enum Role {
  'USER' = 'user',
  'BOT' = 'bot',
}

export interface EnginesConfig {
  apiKey?: string;
  baseURL: string;
}

export enum HuggingFaceModel {
  'stable-diffusion-2-1-base' = 'stable-diffusion-2-1-base',
  'stable-diffusion-2-1' = 'stable-diffusion-2-1',
  'goofyai/3d_render_style_xl' = 'goofyai/3d_render_style_xl',
  'gsdf/Counterfeit-V2.5' = 'gsdf/Counterfeit-V2.5',
  'stabilityai/stable-diffusion-2' = 'stabilityai/stable-diffusion-2',
  '22h/vintedois-diffusion-v0-1' = '22h/vintedois-diffusion-v0-1',
  'digiplay/DreamShaper_8' = 'digiplay/DreamShaper_8',
  'emilianJR/chilloutmix_NiPrunedFp32Fix' = 'emilianJR/chilloutmix_NiPrunedFp32Fix',
}

export enum LocalAiModel {
  'ggml-gpt4all-j' = 'ggml-gpt4all-j',
  'baichuan-vicuna-7b.ggmlv3.q4_0' = 'baichuan-vicuna-7b.ggmlv3.q4_0',
  'open-llama-7B-open-instruct.ggmlv3.q4_0' = 'open-llama-7B-open-instruct.ggmlv3.q4_0',
  'ggml-mpt-7b-base' = 'ggml-mpt-7b-base',
  'ggml-mpt-7b-instruct' = 'ggml-mpt-7b-instruct',
}

export enum Screens {
  'TEXT_TO_TEXT' = 'text-to-text',
  'TEXT_TO_IMAGE' = 'text-to-image',
}
