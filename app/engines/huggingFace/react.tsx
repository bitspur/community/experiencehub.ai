/**
 * File: /openai/huggingFace/react.ts
 * Project: app
 * File Created: 26-07-2023 16:12:24
 * Author: Dharmendra
 * -----
 * Last Modified: 26-07-2023 16:44:20
 * Modified By: Dharmendra
 * -----
 * Your Company (c) Copyright 2023
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { PropsWithChildren, createContext, useContext } from 'react';
import { HuggingFace } from '../types';

const HuggingFaceContext = createContext<HuggingFace | undefined>(undefined);

export interface HuggingFaceProps extends PropsWithChildren {
  huggingFace: HuggingFace;
}

export const useHuggingFace = () => useContext(HuggingFaceContext);

export function HuggingFaceProvider({ children, huggingFace }: HuggingFaceProps) {
  return <HuggingFaceContext.Provider value={huggingFace}>{children}</HuggingFaceContext.Provider>;
}
