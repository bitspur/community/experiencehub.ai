/*
 *  File: /engines/huggingFace/huggingface.ts
 *  Project: app
 *  File Created: 09-08-2023 11:44:46
 *  Author: Hari Krishna
 *  -----
 *  Last Modified: 14-09-2023 16:52:02
 *  Modified By: Clay Risser
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {
  EnginesConfig,
  HuggingFace,
  ImageClassifierRequestBody,
  ImageGeneratorRequestBody,
  SummerizationInputRequest,
  TextClassifierResponse,
  audioRequest,
  chatRequest,
  fillMaskResponse,
  zeroShotClassifierResponse,
  questionAnswerRequest,
  audioRequestOutputBody,
  zeroShotImageClassifierResponse,
  TranslatedTextData,
  conversationResponse,
  SentenceSimilarityConfig,
  TableQuestionAnsweringResponse,
  DocumentQuestionAnsweringRequestBody,
  featureExtractionRequestBody,
  getTextGenerationResponseBody,
  getImageToTextRequest,
  VisualQuestionAnsweringRequestBody,
} from '../types';

import axios, { AxiosInstance } from 'axios';

const logger = console;

export class HuggingFaceApi {
  private api: AxiosInstance;

  static connect(config: EnginesConfig) {
    return new HuggingFaceApi(config);
  }

  constructor(config: EnginesConfig) {
    this.api = axios.create({
      baseURL: config.baseURL,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer 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`,
      },
    });
    logger.log('INFO: Connected to HuggingFace');
  }

  async getChatResponse(requestData: chatRequest) {
    // TODO: need to find the api and  add the logic for chat response
    return null;
  }

  async getGeneratedImages(requestBody: ImageGeneratorRequestBody) {
    const response = (
      await this.api.post(
        `models/stabilityai/${requestBody.model}`,
        { inputs: requestBody.prompt },
        {
          responseType: 'blob',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer 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`,
          },
        },
      )
    ).data;
    return URL.createObjectURL(response);
  }

  async getAnswerFromQuestion(requestBody: questionAnswerRequest) {
    return (
      await this.api.post(
        '/models/deepset/tinyroberta-squad2',
        { inputs: requestBody },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer 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',
          },
        },
      )
    ).data;
  }

  async getAudioResponse(requestBody: audioRequest) {
    const data = (
      await this.api.post(
        'models/speechbrain/tts-fastspeech2-ljspeech',
        {
          inputs: requestBody.prompt,
        },
        {
          headers: {
            'Content-Type': 'audio/webm',
            Authorization: 'Bearer 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',
          },
          responseType: 'arraybuffer',
        },
      )
    ).data;
    return new Blob([data], { type: 'audio/flac' });
  }

  async getImageResponse(blob: Blob) {
    return (
      await this.api.post(
        '/models/microsoft/swin-tiny-patch4-window7-224',

        blob,
        {
          responseType: 'json',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer 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`,
          },
        },
      )
    ).data;
  }

  async getImageSegmentationResponse(blob: Blob) {
    return (
      await this.api.post(
        '/models/facebook/mask2former-swin-base-coco-panoptic',

        blob,
        {
          responseType: 'json',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer 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`,
          },
        },
      )
    ).data;
  }

  async getObjectDetectionResponse(blob: Blob) {
    return (
      await this.api.post('/models/facebook/detr-resnet-50', blob, {
        responseType: 'json',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer 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`,
        },
      })
    ).data;
  }

  async getTextClassificationResponse(requestBody: TextClassifierResponse) {
    const data = (
      await this.api.post(
        '/models/SamLowe/roberta-base-go_emotions',
        {
          inputs: requestBody.prompt,
        },
        {
          responseType: 'json',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer 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`,
          },
        },
      )
    ).data;
    return data;
  }

  async getFillMaskResponse(requestBody: fillMaskResponse) {
    const data = (
      await this.api.post(
        '/models/bert-base-uncased',
        {
          inputs: requestBody.prompt,
        },
        {
          responseType: 'json',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer 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`,
          },
        },
      )
    ).data;
    return data;
  }

  async getSummarizationResponse(requestBody: SummerizationInputRequest) {
    const data = (
      await this.api.post(
        '/models/facebook/bart-large-cnn',
        {
          inputs: requestBody.prompt,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer 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',
          },
          responseType: 'text',
        },
      )
    ).data;
    return data;
  }

  async getZeroShotClassificationResponse(requestBody: zeroShotClassifierResponse) {
    const data = (
      await this.api.post('/models/facebook/bart-large-mnli', requestBody, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer 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',
        },
        responseType: 'json',
      })
    ).data;
    return data;
  }

  async getZeroShotImageClassificationResponse(requestData: zeroShotImageClassifierResponse) {
    return (
      await this.api.post('/models/openai/clip-vit-large-patch14', requestData, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer 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',
        },
        responseType: 'json',
      })
    ).data;
  }

  async getAudioClassificationResponse(blob: Blob) {
    const data = (
      await this.api.post('/models/speechbrain/lang-id-voxlingua107-ecapa', blob, {
        headers: {
          'Content-Type': 'audio/webm',
          Authorization: 'Bearer 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',
        },
        responseType: 'json',
      })
    ).data;
    return data;
  }

  async getTranslatedText(data: TranslatedTextData) {
    return (
      await axios.post(
        `https://api-inference.huggingface.co/models/Helsinki-NLP/opus-mt-${data.sourceLanguageCode}-${data.targetLanguageCode}`,
        {
          inputs: data.inputs,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer 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`,
          },
        },
      )
    ).data;
  }

  async getConversationResponse(requestData: conversationResponse) {
    return (
      await this.api.post('/models/facebook/blenderbot-400M-distill', requestData, {
        responseType: 'json',
      })
    ).data;
  }

  async getSentenceSimilarity(config: SentenceSimilarityConfig) {
    return (await this.api.post('models/sentence-transformers/all-MiniLM-L6-v2', config)).data;
  }

  async getTokenClassification(requestBody: fillMaskResponse) {
    const data = (
      await this.api.post(
        'models/dslim/bert-base-NER',
        {
          inputs: requestBody.prompt,
        },
        {
          responseType: 'json',
        },
      )
    ).data;
    return data;
  }

  async getTableQuestionAnsweringResponse(requestBody: TableQuestionAnsweringResponse) {
    const data = (
      await this.api.post(
        'models/google/tapas-small-finetuned-wikisql-supervised',
        {
          inputs: {
            query: requestBody.prompt,
            table: requestBody.table,
          },
        },
        {
          responseType: 'json',
        },
      )
    ).data;
    return data;
  }

  async getAutomaticSpeechRecognition(blob: Blob) {
    const data = (
      await this.api.post('/models/openai/whisper-tiny', blob, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer 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',
        },
        responseType: 'json',
      })
    ).data;
    return data.text;
  }

  async getDocumentQuestionAnsweringResponse(body: DocumentQuestionAnsweringRequestBody) {
    return (
      await this.api.post('/models/impira/layoutlm-document-qa', body, {
        responseType: 'json',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer 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`,
        },
      })
    ).data;
  }

  async getFeatureExtraction(requestBody: featureExtractionRequestBody) {
    return (
      await this.api.post(
        '/models/facebook/bart-base',

        requestBody,
        {
          responseType: 'json',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer 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`,
          },
        },
      )
    ).data;
  }

  async getTextGeneration(requestBody: getTextGenerationResponseBody) {
    return (
      await this.api.post('/models/gpt2', requestBody, {
        responseType: 'json',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer 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`,
        },
      })
    ).data;
  }

  async getImageToTextResponse(body: getImageToTextRequest) {
    const data = (
      await this.api.post('/models/nlpconnect/vit-gpt2-image-captioning', body, {
        responseType: 'json',
        headers: {
          Authorization: 'Bearer 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',
          'Content-Type': 'image/jpeg',
        },
      })
    ).data;
    return data;
  }

  async getImageToImageResponse(blob: Blob) {
    const data = (
      await this.api.post('/models/lllyasviel/sd-controlnet-canny', blob, {
        responseType: 'json',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer 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',
        },
      })
    ).data;
    return data;
  }

  async getVisualQuestionAnsweringResponse(body: VisualQuestionAnsweringRequestBody) {
    const response = await this.api.post('/models/dandelin/vilt-b32-finetuned-vqa', body, {
      responseType: 'json',
      headers: {
        Authorization: 'Bearer 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',
        'Content-Type': 'application/json',
      },
    });

    const data = response.data;
    return data;
  }
}
