import React from 'react';
import { UserAudioInput } from './index';

export default {
  title: 'components/UserAudioInput',
  component: UserAudioInput,
  parameters: { status: { type: 'beta' } },
};

export const main = (args) => <UserAudioInput {...args} />;
