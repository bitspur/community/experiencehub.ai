import React, { useState, useRef, useEffect } from 'react';
import { Mic, Dot, StopCircle, Play, Pause, XCircle } from '@tamagui/lucide-icons';
import { SimpleTooltip, Text, Button, XStack } from 'ui';

interface RecordAudioInputProps {
  recording: boolean;
  setPopoverSelection: React.Dispatch<React.SetStateAction<string>>;
}

export const UserAudioInput = ({ ...props }: RecordAudioInputProps) => {
  const [recording, setRecording] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const audioRef = useRef(new Audio());
  const [mediaRecorder, setMediaRecorder] = useState<MediaRecorder | null>(null);
  const [audioUrl, setAudioUrl] = useState<string | null>(null);
  const mediaStreamRef = useRef<MediaStream | null>(null);
  const isBlinkingRef = useRef(false);
  const dotRef = useRef<HTMLElement | null>(null);

  useEffect(() => {
    const animateBlink = () => {
      if (dotRef.current) {
        dotRef.current.style.opacity = isBlinkingRef.current ? '1' : '0.2';
      }
      requestAnimationFrame(animateBlink);
    };
    const blinkInterval = setInterval(() => {
      isBlinkingRef.current = !isBlinkingRef.current;
    }, 600);
    animateBlink();
    return () => {
      clearInterval(blinkInterval);
    };
  }, []);

  const handleStartRecording = () => {
    startRecording();
  };

  const handleStopRecording = () => {
    stopRecording();
  };

  useEffect(() => {
    if (!props.recording) return;
    startRecording();
  }, [props.recording]);

  const startRecording = async () => {
    console.log('Recording started');
    const audioElement = new Audio('app/assets/record-notification.mp3');
    audioElement.play();
    const { start, mediaRecorderInstance, stream } = (await recordAudio()) as {
      start: () => void;
      mediaRecorderInstance: MediaRecorder;
      stream: MediaStream;
    };
    mediaStreamRef.current = stream;
    setMediaRecorder(mediaRecorderInstance);
    start();
    setRecording(true);
  };

  const stopRecording = () => {
    console.log('Recording stopped');
    if (mediaRecorder) {
      mediaRecorder.stop();
      setRecording(false);
    }
    if (mediaStreamRef.current) {
      mediaStreamRef.current.getTracks().forEach((track) => track.stop());
      mediaStreamRef.current = null;
    }
  };

  const recordAudio = () =>
    new Promise(async (resolve) => {
      const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
      const mediaRecorder = new MediaRecorder(stream);
      const audioChunks: Blob[] = [];
      mediaRecorder.addEventListener('dataavailable', (event) => {
        audioChunks.push(event.data);
      });

      mediaRecorder.addEventListener('stop', () => {
        const audioBlob = new Blob(audioChunks, { type: 'audio/wav' });
        const audioUrl = URL.createObjectURL(audioBlob);
        console.log('Recorded Audio Chunks:', audioChunks);
        setAudioUrl(audioUrl);
        resolve({ start, mediaRecorderInstance: mediaRecorder, stream });
      });
      const start = () => mediaRecorder.start();
      resolve({ start, mediaRecorderInstance: mediaRecorder, stream });
    });

  const handleAudioPlayback = () => {
    if (audioUrl) {
      const audio = audioRef.current;
      if (!isPlaying) {
        audio.src = audioUrl;
        audio.play();
        setIsPlaying(true);
      } else {
        audio.pause();
        audio.currentTime = 0;
        setIsPlaying(false);
      }
    }
  };

  const handleRemoveAudio = () => {
    setAudioUrl(null);
    setIsPlaying(false);
    props.setPopoverSelection('Text Input');
  };

  return (
    <XStack>
      {recording ? (
        <Button jc="center" ai="center">
          <SimpleTooltip element={<Text color="white">recording...</Text>}>
            <Text ref={dotRef}>
              <Dot size={40} color="green" />
            </Text>
          </SimpleTooltip>
          <SimpleTooltip element={<Text color="white">Stop Recording</Text>}>
            <Button
              onPress={handleStopRecording}
              style={{
                cursor: 'pointer',
              }}
            >
              <StopCircle color="red" />
            </Button>
          </SimpleTooltip>
        </Button>
      ) : (
        <XStack>
          <SimpleTooltip element={<Text color="white">Record</Text>}>
            <Button onPress={handleStartRecording}>
              <Mic />
            </Button>
          </SimpleTooltip>
          {audioUrl && (
            <XStack>
              <Button onPress={handleAudioPlayback}>{isPlaying ? <Pause /> : <Play />}</Button>
              {isPlaying && (
                <Button onPress={handleRemoveAudio}>
                  <XCircle />
                </Button>
              )}
            </XStack>
          )}
        </XStack>
      )}
    </XStack>
  );
};
