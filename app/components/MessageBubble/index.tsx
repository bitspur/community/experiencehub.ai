import React, { useState, useEffect, useRef } from 'react';
import { YStack, Text, TextProps, XStack, Circle, XStackProps, Paragraph } from 'ui';

export interface CustomProps {
  text: string | undefined;
  speed?: number;
  onComplete?: (value: boolean) => void;
  isUser?: boolean;
  bg?: string;
  bottomRightRadius?: TextProps['borderBottomRightRadius'];
  bottomLeftRadius?: TextProps['borderBottomLeftRadius'];
  avatar?: string;
  avatarBg?: TextProps['bg'];
  avatarContent?: string;
  isTypeWriter?: boolean;
}

type MessageBubbleProps = CustomProps & XStackProps;

export const MessageBubble = ({
  text,
  speed,
  onComplete,
  isUser,
  bg,
  bottomLeftRadius,
  bottomRightRadius,
  ...props
}: MessageBubbleProps) => {
  const [displayText, setDisplayText] = useState('');
  const [completed, setCompleted] = useState(false);
  const [time, setTime] = useState('');
  const date = new Date().toDateString().substring(4);
  const displayRef = useRef('');

  useEffect(() => {
    const date = new Date();
    const isoDate = date.toISOString().toString();
    const timeStamp = date.toDateString().substring(4);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const time = `${hours}:${minutes}`;
    setTime(time);
  }, []);

  useEffect(() => {
    if (onComplete) onComplete(completed);
  }, [completed]);

  useEffect(() => {
    if (props.isTypeWriter) {
      if (isUser && text) {
        setDisplayText(text);
        return;
      }
      let currentIndex = 0;
      const interval = setInterval(() => {
        if (text && currentIndex < text.length) {
          displayRef.current = displayRef.current + text[currentIndex];
          setDisplayText(displayRef.current);
        } else {
          clearInterval(interval);
          setCompleted(true);
        }
        currentIndex++;
      }, speed);
      return () => clearInterval(interval);
    }
    if (text) {
      setDisplayText(text);
      setCompleted(true);
    }
  }, [text, speed, props.isTypeWriter]);

  return (
    <XStack {...props}>
      {!isUser && renderAvatar()}
      {text && (
        <YStack maxWidth="60%">
          <Paragraph
            borderTopRightRadius={10}
            borderTopLeftRadius={10}
            borderBottomRightRadius={bottomRightRadius}
            borderBottomLeftRadius={bottomLeftRadius}
            bg={bg}
            bw={1}
            borderColor="$backgroundFocus"
            p="$3"
          >
            {displayText}
            <Text color="$gray6Dark" fontSize={12}>
              {date} at {time}
            </Text>
          </Paragraph>
        </YStack>
      )}
      {isUser && renderAvatar()}
    </XStack>
  );

  function renderAvatar() {
    return (
      <Circle size="$4" bg={props.avatarBg || props.avatar}>
        <Text>{props.avatarContent}</Text>
      </Circle>
    );
  }
};
