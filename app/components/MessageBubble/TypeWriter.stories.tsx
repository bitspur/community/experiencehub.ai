import React from 'react';
import { MessageBubble } from './index';

export default {
  title: 'components/MessageBubble',
  component: MessageBubble,
  parameters: { status: { type: 'beta' } },
};

export const main = (args) => <MessageBubble {...args} />;
main.args = {
  text: 'May I know who is coming to visit the property?',
  speed: 100,
  isUser: true,
  avatarContent: 'AI',
  isTypeWriter: true,
};
main.argTypes = {
  bg: { control: 'color' },
  avatarBg: { control: 'color' },
};
