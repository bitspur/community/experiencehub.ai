import React from 'react';
import { GiftedChatNative } from './index';

export default {
  title: 'components/GiftedChatNative',
  component: GiftedChatNative,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <GiftedChatNative />;
