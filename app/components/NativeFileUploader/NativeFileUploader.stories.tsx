import React from 'react';
import { NativeFileUploader } from '.';

export default {
  title: 'Components/NativeFileUploader',
  component: NativeFileUploader,
  parameters: { status: { type: 'beta' } },
};

export const main = () => {
  return (
    <NativeFileUploader onDocumentPick={(document) => document !== null && console.log('document', document)}>
      PICK
    </NativeFileUploader>
  );
};
