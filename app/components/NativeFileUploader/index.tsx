import React, { ReactNode } from 'react';
import { YStack, Button } from 'ui';
import * as DocumentPicker from 'expo-document-picker';
import type { YStackProps } from 'ui';

export interface NativeFileUploaderProps {
  children: ReactNode;
  onDocumentPick: (document: DocumentPicker.DocumentPickerResult | null) => void;
}

export const NativeFileUploader = ({ children, onDocumentPick, ...props }: NativeFileUploaderProps & YStackProps) => {
  const [document, setDocument] = React.useState<DocumentPicker.DocumentPickerResult | null>(null);

  const pickDocument = async () => {
    const result = await DocumentPicker.getDocumentAsync({});
    if (result.type === 'success') {
      setDocument(result);
    } else {
      setDocument(null);
    }
  };

  React.useEffect(() => {
    onDocumentPick(document);
  }, [document]);

  return (
    <YStack {...props}>
      <Button onPress={pickDocument}>{children}</Button>
    </YStack>
  );
};
