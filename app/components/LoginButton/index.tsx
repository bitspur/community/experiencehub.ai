import React from 'react';
import { YStack, SimpleDialog, Button, SizableText } from 'ui';
import type { YStackProps } from 'ui';
import { ForgetPassword } from '../ForgetPassword';
import { SignUp } from '../SignUp';
import { Login } from '../Login';

export interface LoginButtonProps {
  setOpenLogin: React.Dispatch<React.SetStateAction<boolean>>;
  setOpenSignUp: React.Dispatch<React.SetStateAction<boolean>>;
  setOpenForgotPassword: React.Dispatch<React.SetStateAction<boolean>>;

  openLogin: boolean;
  openSignUp: boolean;
  openForgotPassword: boolean;
}

type LoginSectionProps = YStackProps & LoginButtonProps;

export const LoginButton = ({
  openForgotPassword,
  openLogin,
  openSignUp,
  setOpenForgotPassword,
  setOpenLogin,
  setOpenSignUp,
  ...props
}: LoginSectionProps) => {
  return (
    <YStack cursor="pointer" {...props}>
      <SimpleDialog
        trigger={
          <Button>
            <SizableText
              onPress={() => {
                setOpenLogin(true);
                setOpenForgotPassword(false);
                setOpenSignUp(false);
              }}
            >
              Login/SignUp
            </SizableText>
          </Button>
        }
        title=""
      >
        <YStack space>
          {openForgotPassword && (
            <ForgetPassword setOpenForgotPassword={setOpenForgotPassword} setOpenLogin={setOpenLogin} />
          )}
          {openSignUp && <SignUp setOpenLogin={setOpenLogin} setOpenSignUp={setOpenSignUp} />}
          {openLogin && (
            <Login
              setOpenLogin={setOpenLogin}
              setOpenSignUp={setOpenSignUp}
              setOpenForgotPassword={setOpenForgotPassword}
            />
          )}
        </YStack>
      </SimpleDialog>
    </YStack>
  );
};
