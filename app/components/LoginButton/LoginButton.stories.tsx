import React from 'react';
import { LoginButton } from './index';

export default {
  title: 'Components/LoginButton',
  component: LoginButton,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <LoginButton />;
