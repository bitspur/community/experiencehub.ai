import React from 'react';
import { NavSection } from './index';

export default {
  title: 'Components/NavSection',
  component: NavSection,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <NavSection />;
