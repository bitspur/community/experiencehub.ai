import React from 'react';
import { YStack, Text, SimplePopover, XStack } from 'ui';
import { useExperiences } from 'app/utils/useExperiences';
import { Link } from 'solito/link';
import { HelpCircle, Home, Settings } from '@tamagui/lucide-icons';
import type { XStackProps } from 'ui';

type NavSectionProps = XStackProps;

export const NavSection = (props: NavSectionProps) => {
  const { experiences } = useExperiences();
  return (
    <XStack {...props} space ai="center">
      {experiences.map((experience, i) => (
        <SimplePopover trigger={<Text cursor="pointer">{experience.heading}</Text>} key={i}>
          <YStack space>
            {experience.experiencesList.map((item, i) => (
              <Link href={item.link} key={i}>
                <Text cursor="pointer" hoverStyle={{ backgroundColor: '$backgroundHover' }}>
                  {item.value}
                </Text>
              </Link>
            ))}
          </YStack>
        </SimplePopover>
      ))}
      <Link href="/">
        <YStack cursor="pointer">
          <Home />
        </YStack>
      </Link>
      <Link href="/settings">
        <YStack cursor="pointer">
          <Settings />
        </YStack>
      </Link>
      <Link href="/help">
        <YStack cursor="pointer">
          <HelpCircle />
        </YStack>
      </Link>
    </XStack>
  );
};
