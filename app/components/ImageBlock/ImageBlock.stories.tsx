import React from 'react';
import { ImageBlock } from './index';

export default {
  title: 'components/ImageBlock',
  component: ImageBlock,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = (args) => <ImageBlock {...args} />;
