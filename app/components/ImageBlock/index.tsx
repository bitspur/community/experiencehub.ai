import { Circle, Square, Text, Unspaced, View, XStack, YStack } from 'ui';
import React from 'react';
import { IChat } from 'app/screens/chat';
import { SimpleImage } from '@multiplatform.one/components';

export interface ImageBlockProps {
  chat: IChat;
}

export function ImageBlock({ chat }: ImageBlockProps) {
  const [showMore, setShowMore] = React.useState(false);

  return (
    <XStack width="100%">
      <Circle size="$3" bg="$blue10" als="flex-end" marginRight="$1" mb={showMore ? '$3' : undefined}>
        <Text>B</Text>
      </Circle>
      <YStack space>
        {showMore ? (
          chat?.images?.map((image) => (
            <SimpleImage
              src={image}
              key={image}
              height={300}
              width={400}
              cursor="pointer"
              onPress={() => window.open(image, '_blank')}
              resizeMode="cover"
              $xs={{ width: 300, height: 300 }}
              backgroundColor="$backgroundHover"
            />
          ))
        ) : (
          <XStack
            flex={2}
            flexDirection="row"
            flexWrap="wrap"
            width={400}
            height={400}
            $xs={{ width: 300, height: 300 }}
          >
            {Array.from({ length: 4 }).map((_, index) => {
              if (!chat?.images) return;
              const image = chat.images[index];
              return (
                <View width="50%">
                  <SimpleImage
                    src={image}
                    key={index}
                    height={200}
                    width={200}
                    cursor="pointer"
                    onPress={() => window.open(image, '_blank')}
                    resizeMode="cover"
                    $xs={{ width: 150, height: 150 }}
                  />
                  {chat?.images?.length > 4 && (
                    <Square
                      size={200}
                      position="absolute"
                      right={0}
                      bottom={0}
                      backgroundColor="rgba(0,0,0,0.65)"
                      cursor="pointer"
                      onPress={() => setShowMore(true)}
                      $xs={{ size: 150 }}
                    >
                      <Text color="white" fontSize={20} fontWeight="bold">
                        +{chat?.images?.length - 4}
                      </Text>
                    </Square>
                  )}
                </View>
              );
            })}
          </XStack>
        )}
        {showMore && (
          <Unspaced>
            <Text als="flex-end" color="blue" cursor="pointer" onPress={() => setShowMore(false)}>
              show less
            </Text>
          </Unspaced>
        )}
      </YStack>
    </XStack>
  );
}
