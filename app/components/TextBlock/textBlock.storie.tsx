import React from 'react';
import { TextBlock } from './index';

export default {
  title: 'components/TextBlock',
  component: TextBlock,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = (args) => <TextBlock {...args} />;
