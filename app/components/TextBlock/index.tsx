import { Circle, Text, XStack, YStack, useWindowDimensions } from 'ui';
import { CodeBlock } from '@multiplatform.one/components';
import React from 'react';
import { IMessage } from 'app/screens/experiences/chat';
import { SimpleParagraph } from '../SimpleParagraph';
import { useMedia } from 'ui';

export interface TextBlockProps {
  chat: IMessage;
}

export function TextBlock({ chat }: TextBlockProps) {
  const parsedSegments = parseContent(chat.prompt);
  const { width } = useWindowDimensions();
  const media = useMedia();

  function parseContent(data) {
    const regex = /```(.*?)\n([\s\S]*?)```/g;
    const segments = data?.split(regex);
    const parsedSegments: { type: string; content?: string; language?: string; code?: string }[] = [];

    segments?.forEach((segment, index) => {
      if (index % 3 === 0) {
        parsedSegments.push({
          type: 'text',
          content: segment.trim(),
        });
      } else if (index % 3 === 1) {
        parsedSegments.push({
          type: 'code',
          language: segment,
          code: segments[index + 1].trim(),
        });
      }
    });
    return parsedSegments;
  }

  return (
    <XStack
      elevation={1}
      padding="$5"
      width={width}
      jc={media.sm ? 'flex-start' : 'center'}
      backgroundColor={chat.role === 'user' ? '$gray7' : '$gray8'}
    >
      <XStack space width={media.sm ? width : width * 0.5} jc="flex-start">
        {chat.role === 'assistant' && (
          <Circle size="$3" bg="$green9Light" als="flex-start" marginRight="$1">
            <Text>A</Text>
          </Circle>
        )}
        {chat.role === 'user' && (
          <Circle size="$3" bg="$blue10" als="flex-start" ml="$1">
            <Text>U</Text>
          </Circle>
        )}
        <YStack jc="center" width={media.sm ? width * 0.75 : width * 0.5}>
          {parsedSegments?.map((segment, index) => {
            if (segment.type === 'text' && segment.content) {
              return (
                <SimpleParagraph backgroundColor={chat.role === 'user' ? '$gray7' : '$gray8'} key={index}>
                  {segment.content}
                </SimpleParagraph>
              );
            }

            if (segment.type === 'code') {
              return (
                <CodeBlock key={index} language={segment.language} backgroundColor="$color.blue4Light">
                  {segment.code}
                </CodeBlock>
              );
            }
          })}
        </YStack>
      </XStack>
    </XStack>
  );
}
