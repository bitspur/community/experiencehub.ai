import React from 'react';
import { YStack, Text, Input, Button } from 'ui';

export interface SignUpProps {
  setOpenSignUp: (openSignUp: boolean) => void;
  setOpenLogin: (openLogin: boolean) => void;
}

export const SignUp = ({ setOpenLogin, setOpenSignUp }: SignUpProps) => {
  return (
    <YStack space mt ai="center">
      <Text fontSize="$6" fontWeight="600">
        SignUp
      </Text>
      <Input placeholder="First Name" />
      <Input placeholder="Last Name" />
      <Input placeholder="Username" />
      <Input placeholder="Password" />
      <Input placeholder="Confirm Password" />
      <Input placeholder="Email(optional)" />
      <Input placeholder="Phone(optional)" />
      <Button
        onPress={() => {
          setOpenSignUp(false);
          setOpenLogin(true);
        }}
      >
        SignUp
      </Button>
    </YStack>
  );
};
