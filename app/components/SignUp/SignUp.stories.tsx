import React from 'react';
import { SignUp } from './index';

export default {
  title: 'Components/SignUp',
  component: SignUp,
  parameters: { status: { type: 'beta' } },
};

export const main = (args) => <SignUp {...args} />;
