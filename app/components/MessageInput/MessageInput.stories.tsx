import React from 'react';
import { MessageInput } from './index';

export default {
  title: 'components/MessageInput',
  component: MessageInput,
  parameters: { status: { type: 'beta' } },
};

export const main = (args) => <MessageInput {...args} />;
main.args = {
  isEmoji: true,
  isMic: true,
  isAttach: true,
  mediaList: '[hwllo,abcd,efgh]',
};
