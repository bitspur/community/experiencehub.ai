import React, { useState } from 'react';
import { Send } from '@tamagui/lucide-icons';
import { XStack, TextArea, YStack, Circle, YStackProps, InputProps } from 'ui';
import { Platform, useWindowDimensions } from 'react-native';
import { useMedia } from 'ui';

export interface CustomProps {
  onSend?: (text: string) => void;
  disabled?: boolean;
  onPress?: () => void;
  isAttach?: boolean;
  isEmoji?: boolean;
  showMic?: boolean;
  mediaList?: string;
  waitingForResponse?: boolean;
}

export type MessageInputProps = CustomProps & YStackProps & InputProps;

export const MessageInput = (props: MessageInputProps) => {
  const media = useMedia();
  const [text, setText] = React.useState('');
  const { width } = useWindowDimensions();
  const [textAreaHeight, setTextAreaHeight] = useState(Platform.OS === 'web' ? 50 : 45);

  function handleKeyPress(e) {
    if (props.waitingForResponse) return;
    if (e.key === 'Enter' && !e.shiftKey) {
      e.preventDefault();
      handleSend();
    }
    if (e.shiftKey && e.key === 'Enter') {
      textAreaHeight <= 150 && setTextAreaHeight(textAreaHeight + 20);
    }
    if (e.key === 'Backspace') {
      textAreaHeight > 50 && setTextAreaHeight(textAreaHeight - 20);
    }
  }

  function handleSend() {
    if (props.waitingForResponse) return;
    if (props.onSend) props.onSend(text);
    setText('');
    setTextAreaHeight(Platform.OS === 'web' ? 50 : 45);
  }

  return (
    <YStack space {...props}>
      <XStack space jc="space-evenly">
        <XStack p="$2" jc="center" ai="center" space width="100%" bg={props.backgroundColor}>
          <TextArea
            bw={0}
            onKeyPress={handleKeyPress}
            onChangeText={setText}
            placeholder="send a message"
            value={text}
            width={media.sm ? width * 0.75 : width * 0.5}
            height={textAreaHeight}
            scrollEnabled={false}
            onFocus={props.onPress}
            onBlur={props.onBlur}
          />

          <Circle
            cursor="pointer"
            bg="$background"
            jc="center"
            ai="center"
            onPress={handleSend}
            disabled={props.disabled}
            padding={10}
            animation="bouncy"
            elevation={12}
          >
            <Send size={22} />
          </Circle>
        </XStack>
      </XStack>
    </YStack>
  );
};
