import React from 'react';
import { YStack, XStack, Paragraph } from 'ui';
import type { YStackProps } from 'ui';

export interface SimpleParagraphProps extends YStackProps {
  children: string;
}

export const SimpleParagraph = ({ children, ...props }: SimpleParagraphProps) => {
  if (typeof children !== 'string') return null;
  const parsedSegments = children.split('\n');

  return (
    <YStack {...props}>
      {parsedSegments.map((segment, index) => (
        <XStack key={index}>
          <Paragraph key={index}>{segment}</Paragraph>
        </XStack>
      ))}
    </YStack>
  );
};
