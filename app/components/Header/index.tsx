import React, { useState } from 'react';
import { SolitoImage } from 'solito/image';
import { Link } from 'solito/link';
import { XStack, useAssets } from 'ui';
import type { XStackProps } from 'ui';
import { SearchBar } from '../SearchBar';
import { NavSection } from '../NavSection';
import { MenuSection } from '../MenuSection';
import { useMedia } from 'ui';
import { LoginButton } from '../LoginButton';

type HeaderProps = XStackProps;

export const Header = (props: HeaderProps) => {
  const [logoPng] = useAssets(require('app/assets/logo.png'));

  const [openLogin, setOpenLogin] = useState(false);
  const [openSignUp, setOpenSignUp] = useState(false);
  const [openMenu, setOpenMenu] = useState(false);
  const [openForgotPassword, setOpenForgotPassword] = useState(false);
  const screenSize = useMedia();

  return (
    <XStack pr="$4" height={80} backgroundColor="$backgroundFocus" jc="space-between" {...props}>
      <XStack ai="center">
        <Link href="/">
          <SolitoImage src={logoPng} alt="Logo" height={80} width={80} style={{ borderRadius: 9999 }} />
        </Link>
        <SearchBar elevation={12} />
      </XStack>
      <XStack space ai="center">
        <NavSection display={screenSize.gtMd ? 'flex' : 'none'} />
        <LoginButton
          display={screenSize.gtXs ? 'flex' : 'none'}
          {...{ openForgotPassword, openLogin, openSignUp, setOpenForgotPassword, setOpenLogin, setOpenSignUp }}
        />
        <MenuSection
          {...{
            openMenu,
            setOpenMenu,
            openForgotPassword,
            openLogin,
            openSignUp,
            setOpenForgotPassword,
            setOpenLogin,
            setOpenSignUp,
          }}
          jc="center"
        />
      </XStack>
    </XStack>
  );
};
