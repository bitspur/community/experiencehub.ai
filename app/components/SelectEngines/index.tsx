import { Engines, HuggingFaceModel, ImageSize, LocalAiModel, NumberOfImages, Screens } from 'app/engines/types';
import { OpenAiChatCompletions } from 'app/utils';
import React, { useEffect, useState } from 'react';
import { YStack, SimplePopover, Text, Button, XGroup, SelectSimple, Select, Label, Slider } from 'ui';

export interface SelectEnginesProps {
  engine: Engines;
  numberOfImage?: NumberOfImages;
  size?: ImageSize;
  model?: OpenAiChatCompletions | LocalAiModel | HuggingFaceModel;
  temperature?: number[];
  screen?: Screens | undefined;
  onEngineChange: (engine: Engines) => void;
  onNumberOfImagesChange?: (numberOfImages: NumberOfImages) => void;
  onSizeChange?: (size: ImageSize) => void;
  onModelChange?: (model: OpenAiChatCompletions | LocalAiModel | HuggingFaceModel) => void;
  onTemperatureChange?: (temperature: number[]) => void;
}

export const SelectEngines = (props: SelectEnginesProps) => {
  const [engine, setEngine] = useState<Engines>(props.engine);
  const [numberOfImage, setNumberOfImage] = useState<NumberOfImages | undefined>(props.numberOfImage);
  const [size, setSize] = useState<ImageSize | undefined>(props.size);
  const [model, setModel] = useState<OpenAiChatCompletions | LocalAiModel | HuggingFaceModel | undefined>(props.model);
  const [temperature, setTemperature] = useState<number[]>(props.temperature || [0.5]);

  useEffect(() => {
    if (!engine) return;
    if (props.onEngineChange) props.onEngineChange(engine);
  }, [engine]);

  useEffect(() => {
    if (!numberOfImage) return;
    if (props.onNumberOfImagesChange) props.onNumberOfImagesChange(numberOfImage);
  }, [numberOfImage]);

  useEffect(() => {
    if (!size) return;
    if (props.onSizeChange) props.onSizeChange(size);
  }, [size]);

  useEffect(() => {
    if (!model) return;
    if (props.onModelChange) props.onModelChange(model);
  }, [model]);

  useEffect(() => {
    if (!temperature) return;
    if (props.onTemperatureChange) props.onTemperatureChange(temperature);
  }, [temperature]);

  function handleEngineChange(engine: Engines) {
    setEngine(engine);
    switch (engine) {
      case Engines.OPENAI:
        setModel(OpenAiChatCompletions['GPT_3_5_TURBO']);
        setTemperature([0.5]);
        setNumberOfImage(NumberOfImages.ONE);
        setSize(ImageSize.MEDIUM);
        break;
      case Engines.LOCALAI:
        setModel(LocalAiModel['ggml-gpt4all-j']);
        break;
      case Engines.HUGGINGFACE:
        setModel(HuggingFaceModel['stable-diffusion-2-1']);
        break;
    }
  }

  function renderSelectAndItems(items: string[], placeholder?: string, onValueChange?: (value: string) => void) {
    return (
      <SelectSimple jc="center" placeholder={placeholder} onValueChange={onValueChange}>
        {items.map((item, i) => (
          <Select.Item key={i} index={i} value={item}>
            <Select.ItemText>{item}</Select.ItemText>
          </Select.Item>
        ))}
      </SelectSimple>
    );
  }

  function renderOpenAiOptions() {
    return (
      <YStack padding="$1" width="$15" space>
        <Text>
          Empowering apps with advanced language models, delivering smarter, interactive, and dynamic user experiences.
        </Text>

        {(props.screen === Screens.TEXT_TO_TEXT || !props.screen) &&
          renderSelectAndItems(Object.values(OpenAiChatCompletions), 'select the model', (value) =>
            setModel(value as OpenAiChatCompletions),
          )}

        {(props.screen === Screens.TEXT_TO_IMAGE || !props.screen) && (
          <YStack>
            <Label>no of images</Label>
            {renderSelectAndItems(
              Object.values(NumberOfImages).filter((value) => !isNaN(Number(value))) as string[],
              'number of images',
              (value) => setNumberOfImage(Number(value) as NumberOfImages),
            )}
          </YStack>
        )}

        {(props.screen === Screens.TEXT_TO_IMAGE || !props.screen) && (
          <YStack>
            <Label>image size</Label>
            {renderSelectAndItems(Object.values(ImageSize), 'image size', (value) => setSize(value as ImageSize))}
          </YStack>
        )}

        {(props.screen === Screens.TEXT_TO_TEXT || !props.screen) && (
          <YStack>
            <Label>Temperature [{temperature[0]}]</Label>
            <Slider name="Temperature" min={0} max={1} step={0.1} onValueChange={setTemperature} value={temperature}>
              <Slider.Track size="$6">
                <Slider.TrackActive br={0} bg="limegreen" />
              </Slider.Track>
              <Slider.Thumb size="$2" theme="SliderThumb" circular index={0} />
            </Slider>
          </YStack>
        )}
      </YStack>
    );
  }

  function renderHuggingFaceOptions() {
    return (
      <YStack width="$16" space>
        <Text>
          Hugging Face offers numerous AI models via APIs, enabling developers to incorporate sophisticated natural
          language processing in their applications.
        </Text>

        {renderSelectAndItems(Object.values(HuggingFaceModel), 'select the model', (value) =>
          setModel(value as HuggingFaceModel),
        )}
      </YStack>
    );
  }

  function renderLocalAiOptions() {
    return (
      <YStack width="$15" space>
        <Text>
          LocalAi is open source which provides a models and api's to generate images, text, audio and video without
          internet.
        </Text>

        {renderSelectAndItems(Object.values(LocalAiModel), 'select the model', (value) =>
          setModel(value as LocalAiModel),
        )}
      </YStack>
    );
  }

  return (
    <YStack ai="center" jc="center">
      <XGroup space="$2" backgroundColor="$backgroundFocus" padding="$2">
        {Object.values(Engines).map((engine, i) => (
          <SimplePopover trigger={<Button onPress={() => handleEngineChange(engine)}>{engine}</Button>} key={i}>
            {engine === Engines.OPENAI && renderOpenAiOptions()}
            {engine === Engines.LOCALAI && renderLocalAiOptions()}
            {engine === Engines.HUGGINGFACE && renderHuggingFaceOptions()}
          </SimplePopover>
        ))}
      </XGroup>
    </YStack>
  );
};
