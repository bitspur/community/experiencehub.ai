import React from 'react';
import { SelectEngines } from './index';

export default {
  title: 'components/SelectEngines',
  component: SelectEngines,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <SelectEngines />;
