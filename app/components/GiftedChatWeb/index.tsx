import React, { useState } from 'react';
import { GiftedChat, IMessage } from 'react-web-gifted-chat';
import { useOpenAi } from 'app/engines/openai';
import { chatRequest } from 'app/engines/types';

export function GiftedChatWeb() {
  const openAi = useOpenAi();
  const [message, setMessage] = useState<IMessage[]>([]);

  const handleSend = async (text: string) => {
    if (!openAi) return;
    let response: any;
    if (text === '') return;
    const randomId = Math.floor(Math.random() * 1000000000);
    setMessage((prev) => [...prev, { id: randomId, text: text, user: { id: 1, name: 'user' }, createdAt: new Date() }]);
    const payload = {
      messages: [{ role: 'user', content: text }],
      model: openAi ? 'gpt-3.5-turbo' : 'ggml-gpt4all-j',
      temperature: 0.5,
    } as chatRequest;
    if (openAi) {
      response = await openAi.getChatResponse(payload);
    }
    setMessage((prev) => [
      ...prev,
      {
        id: randomId + 1,
        text: response.choices[0].message.content,
        user: { id: 2, name: 'bot' },
        createdAt: new Date(),
      },
    ]);
  };

  return (
    <GiftedChat
      isAnimated={true}
      inverted={false}
      messages={message}
      onSend={(message) => message && message[0] && handleSend(message[0].text)}
      placeholder="Type your message here..."
      user={{
        id: 1,
        name: 'user',
      }}
    />
  );
}
