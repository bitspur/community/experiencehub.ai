import React from 'react';
import { GiftedChatWeb } from './index';

export default {
  title: 'components/GiftedChatWeb',
  component: GiftedChatWeb,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <GiftedChatWeb />;
