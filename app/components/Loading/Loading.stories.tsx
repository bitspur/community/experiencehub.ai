import React from 'react';
import { Loading } from './index';

export default {
  title: 'components/Loading',
  component: Loading,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = (args) => <Loading {...args} />;
main.args = {
  speed: 100,
  p: 10,
};
