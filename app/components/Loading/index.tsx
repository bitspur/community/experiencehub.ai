import React, { useEffect, useState } from 'react';
import { Text, TextProps, XStack } from 'ui';

export interface LoadingProps {
  speed?: number;
  p?: TextProps['p'];
}

export const Loading = ({ speed, p }: LoadingProps) => {
  const [dots, setDots] = useState('.');

  useEffect(() => {
    const interval = setInterval(() => {
      setDots((prev) => (prev.length < 5 ? prev + '.' : '.'));
    }, speed);
    return () => clearInterval(interval);
  });

  return (
    <XStack>
      <Text ai="center" p={p} br={10} bg="#D4E2D4" jc="flex-start" fontWeight="900">
        {dots}
      </Text>
    </XStack>
  );
};
