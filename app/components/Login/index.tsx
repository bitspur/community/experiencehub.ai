import React from 'react';
import { YStack, Text, Input, XStack, Dialog, Button } from 'ui';

export interface LoginProps {
  setOpenLogin: (openLogin: boolean) => void;
  setOpenSignUp: (openSignUp: boolean) => void;
  setOpenForgotPassword: (openForgotPassword: boolean) => void;
}

export const Login = ({ setOpenLogin, setOpenSignUp, setOpenForgotPassword }: LoginProps) => {
  return (
    <YStack space ai="center">
      <Text fontWeight="600">Login/SignUp</Text>
      <Input placeholder="Username" />
      <Input placeholder="Password" />
      <Text
        onPress={() => {
          setOpenLogin(false);
          setOpenForgotPassword(true);
        }}
        mb="$4"
        ml="$2"
        color="blue"
        fontSize="$2"
        cursor="pointer"
      >
        Forget Password?
      </Text>
      <XStack jc="space-evenly" space>
        <Dialog.Close asChild>
          <Button>Login</Button>
        </Dialog.Close>
        <Button
          onPress={() => {
            setOpenLogin(false);
            setOpenSignUp(true);
          }}
        >
          SignUp
        </Button>
      </XStack>
    </YStack>
  );
};
