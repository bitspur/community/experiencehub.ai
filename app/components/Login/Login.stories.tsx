import React from 'react';
import { Login } from './index';

export default {
  title: 'Components/Login',
  component: Login,
  parameters: { status: { type: 'beta' } },
};

export const main = (args) => <Login {...args} />;
