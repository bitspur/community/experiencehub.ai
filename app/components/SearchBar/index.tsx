import React, { useState, useEffect } from 'react';
import { Search } from '@tamagui/lucide-icons';
import { XStack, Input, SimplePopover, Paragraph, ScrollView } from 'ui';
import type { XStackProps } from 'ui';
import { useExperiences } from 'app/utils/useExperiences';
import { Link } from 'solito/link';

type SearchBarProps = XStackProps;

export const SearchBar = (props: SearchBarProps) => {
  const { experiences } = useExperiences();
  const [inputVal, setInputVal] = useState('');
  const [searchedExperience, setSearchedExperiences] = useState<{ value: string; link: string }[]>([]);
  const [openDialog, setOpenDialog] = useState(false);

  useEffect(() => {
    if (!inputVal) setSearchedExperiences([]);
  }, [inputVal]);

  function handleChangeText(text) {
    setSearchedExperiences([]);
    setInputVal(text);
    experiences.forEach((items) => {
      const filterData = items.experiencesList.filter((item) => item.value.toLowerCase().includes(text.toLowerCase()));
      if (filterData.length > 0) {
        setOpenDialog(true);
        setSearchedExperiences((prev) => [...prev, ...filterData]);
      }
    });
  }

  return (
    <XStack pl="$0.75" br="$3" ai="center" backgroundColor="$background" height="$3" space {...props}>
      <Search size="$1" />

      <SimplePopover
        open={openDialog}
        onOpenChange={setOpenDialog}
        trigger={
          <Input placeholder="Search models,users..." unstyled value={inputVal} onChangeText={handleChangeText} />
        }
        contentStyle={{ height: 400 }}
      >
        <ScrollView space>
          {searchedExperience.map((item) => (
            <Link key={item.link} href={item.link}>
              <Paragraph cursor="pointer" hoverStyle={{ backgroundColor: '$backgroundHover' }}>
                {item.value}
              </Paragraph>
            </Link>
          ))}
        </ScrollView>
      </SimplePopover>
    </XStack>
  );
};
