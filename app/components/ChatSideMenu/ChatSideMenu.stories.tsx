import React from 'react';
import { ChatSideMenu } from './index';

export default {
  title: 'components/ChatSideMenu',
  Component: ChatSideMenu,
  parameters: {
    status: {
      type: 'beta',
    },
  },
};

export const Default = (args) => <ChatSideMenu flexWrap={undefined} {...args} />;

Default.args = {
  modelsList: [
    {
      name: 'GPT 3.5',
      value: 'gpt-3.5-turbo',
    },
    {
      name: 'GPT 3.5 16k',
      value: 'gpt-3.5-turbo-16k',
    },
    {
      name: 'GPT 4',
      value: 'gpt-4',
    },
    {
      name: 'GPT 4 32k',
      value: 'gpt-4-32k',
    },
  ],
};
