import React, { useState } from 'react';
import { Model } from 'app/engines/types';
import { SelectSimple, Select, Button, XStack, YStack, Text, StackProps, Slider } from 'ui';

export interface ChatSideMenuProps {
  flexWrap: StackProps['flexWrap'];
  onChatDownload?: () => void;
  onModelChange?: (model: string) => void;
  onTemperatureChange?: (temperature: number[]) => void;
  model?: string;
  temperature?: number;
  modelsList?: Model[];
}

export const ChatSideMenu = ({
  flexWrap,
  onChatDownload,
  onModelChange,
  onTemperatureChange,
  modelsList,
  ...props
}: ChatSideMenuProps) => {
  const [temperature, setTemperature] = useState(props.temperature || 0);
  const [model, setModel] = useState('');

  function handleTemperatureChange(value: number[]) {
    if (!value[0]) return;
    if (onTemperatureChange) onTemperatureChange(value);
    else setTemperature(value[0]);
  }

  function handleModelChange(model: string) {
    if (onModelChange) onModelChange(model);
    setModel(model);
  }

  function renderSlider() {
    return (
      <Slider
        name="Temperature"
        min={0}
        max={1}
        step={0.1}
        onValueChange={handleTemperatureChange}
        value={[props.temperature || temperature]}
      >
        <Slider.Track size="$6">
          <Slider.TrackActive br={0} bg="limegreen" />
        </Slider.Track>
        <Slider.Thumb size="$2" theme="SliderThumb" circular index={0} />
      </Slider>
    );
  }

  function renderModels() {
    return (
      <SelectSimple
        jc="center"
        placeholder="select one of these"
        onValueChange={handleModelChange}
        value={props.model || model}
      >
        {modelsList?.map((model, i) => (
          <Select.Item index={i} value={model.value} key={i}>
            <Select.ItemText>{model.name}</Select.ItemText>
          </Select.Item>
        ))}
      </SelectSimple>
    );
  }

  return (
    <XStack jc="center" flexWrap={flexWrap}>
      <YStack mt="$4.5" space="$12">
        <YStack space="$4.5">
          {renderModels()}
          <Text alignSelf="center" fontWeight="700">
            Temperature
          </Text>
          {renderSlider()}
        </YStack>
        <YStack>
          <Button alignSelf="center" w="$18" onPress={onChatDownload}>
            Download Chat
          </Button>
        </YStack>
      </YStack>
    </XStack>
  );
};

export default ChatSideMenu;
