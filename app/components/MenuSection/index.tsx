import { Menu } from '@tamagui/lucide-icons';
import React from 'react';
import { Button, Paragraph, ScrollView, SimplePopover, Text, YStack } from 'ui';
import type { YStackProps } from 'ui';
import { useExperiences } from 'app/utils/useExperiences';
import { Link } from 'solito/link';
import { LoginButton } from '../LoginButton';

export interface MenuSectionInterface {
  openForgotPassword: boolean;
  openLogin: boolean;
  openSignUp: boolean;
  openMenu: boolean;
  setOpenMenu: React.Dispatch<React.SetStateAction<boolean>>;
  setOpenForgotPassword: React.Dispatch<React.SetStateAction<boolean>>;
  setOpenLogin: React.Dispatch<React.SetStateAction<boolean>>;
  setOpenSignUp: React.Dispatch<React.SetStateAction<boolean>>;
}

type MenuSectionProps = YStackProps & MenuSectionInterface;

export const MenuSection = ({
  openLogin,
  openSignUp,
  openForgotPassword,
  openMenu,
  setOpenMenu,
  setOpenForgotPassword,
  setOpenLogin,
  setOpenSignUp,
  ...props
}: MenuSectionProps) => {
  const { experiences } = useExperiences();

  function triggerMenu() {
    return (
      <YStack onPress={() => setOpenMenu((prev) => !prev)} ai="flex-end">
        <Menu size="$2" />
      </YStack>
    );
  }

  return (
    <YStack {...props}>
      <SimplePopover trigger={triggerMenu()}>
        <YStack zi={999} space>
          <SimplePopover
            trigger={
              <Button>
                <Text>Experiences</Text>
              </Button>
            }
            contentStyle={{ height: 400 }}
          >
            <ScrollView>
              {experiences.map((experience, i) => (
                <YStack key={i} space>
                  {experience.experiencesList.map((item, index) => (
                    <Link href={item.link} key={index + i}>
                      <Paragraph cursor="pointer" hoverStyle={{ backgroundColor: '$backgroundHover' }}>
                        {item.value}
                      </Paragraph>
                    </Link>
                  ))}
                </YStack>
              ))}
            </ScrollView>
          </SimplePopover>
          <Button>
            <Link href="/experiences/chat">
              <Text> Chat</Text>
            </Link>
          </Button>
          <Button>
            <Link href="/experiences/image-generation">
              <Text>ImageGeneration</Text>
            </Link>
          </Button>
          <Button>
            <Link href="/settings/account">
              <Text> Account</Text>
            </Link>
          </Button>
          <Button>
            <Link href="/settings/api-keys">
              <Text> Api Keys</Text>
            </Link>
          </Button>
          <Button>
            <Link href="/settings">
              <Text> Settings</Text>
            </Link>
          </Button>
          <Button>
            <Link href="/help">
              <Text> Help</Text>
            </Link>
          </Button>
          <Button>
            <Link href="/">
              <Text> Home</Text>
            </Link>
          </Button>
          <LoginButton
            {...{ openForgotPassword, openLogin, openSignUp, setOpenForgotPassword, setOpenLogin, setOpenSignUp }}
          />
        </YStack>
      </SimplePopover>
    </YStack>
  );
};
