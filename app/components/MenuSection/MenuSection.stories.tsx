import React from 'react';
import { MenuSection } from './index';

export default {
  title: 'Components/MenuSection',
  component: MenuSection,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <MenuSection />;
