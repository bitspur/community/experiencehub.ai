import React from 'react';
import { ReactFileUploader } from './index';

export default {
  title: 'components/ReactFileUploader ',
  component: ReactFileUploader ,
  parameters: { status: { type: 'beta' } },
};

export const main = (args) => <ReactFileUploader {...args}/>;
