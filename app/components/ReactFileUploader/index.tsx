import React, { useState } from 'react';
import { Upload } from 'antd';
import { Button, YStack } from 'ui';

interface IProps {
  accept?: string;
  onChange?: (file: File) => void;
  onFileUpload: (file: File | null) => void;
  beforeUpload?: (file: File) => boolean | Promise<boolean>;
}

export const ReactFileUploader = ({ accept, onChange, beforeUpload, onFileUpload }: IProps) => {
  const [fileData, setFileData] = useState<File | null>(null);

  const customRequest = (options: any) => {
    const { file } = options;
    if (file instanceof File) {
      setFileData(file);
      onChange?.(file);
      if (onFileUpload) onFileUpload(file);
    }
  };

  const handleBeforeUpload = (file: File) => {
    return beforeUpload?.(file) ?? true;
  };

  return (
    <YStack ai="center" jc="center" space>
      <Upload accept={accept} showUploadList={false} customRequest={customRequest} beforeUpload={handleBeforeUpload}>
        <Button padding="$4">{fileData ? 'File Uploaded' : 'Upload a File'}</Button>
      </Upload>
      {/* <YStack>
        {fileData && (fileData.type.includes('audio/') || fileData.type.includes('video/')) ? (
          <ReactPlayer
            url={URL.createObjectURL(fileData)}
            controls
            width={fileData.type.includes('video/') ? '400px' : 'auto'}
            height={fileData.type.includes('video/') ? '300px' : 'auto'}
          />
        ) : null}
        {fileData && !fileData.type.includes('audio/') && !fileData.type.includes('video/') ? (
          <SimpleImage src={URL.createObjectURL(fileData)} width={800} height={400} />
        ) : null}
      </YStack> */}
    </YStack>
  );
};
