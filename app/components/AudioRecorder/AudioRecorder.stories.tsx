import React from 'react';
import { AudioRecorder } from './index';

export default {
  title: 'components/AudioRecorder',
  component: AudioRecorder,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <AudioRecorder/>;
