import React, { useState, useRef } from 'react';
import { Mic } from '@tamagui/lucide-icons';
import { YStack, Button } from 'ui';

interface AudioRecorderProps {
  onRecordingStart?: (blob: Blob) => void;
}

export const AudioRecorder = ({ onRecordingStart }: AudioRecorderProps) => {
  const [isRecording, setIsRecording] = useState(false);
  const [isRecorded, setIsRecorded] = useState(false);
  const [recordedAudioBlob, setRecordedAudioBlob] = useState<Blob | null>(null);
  const mediaRecorder = useRef<MediaRecorder | null>(null);
  const [stream, setStream] = useState<MediaStream | null>(null);

  const startRecording = async () => {
    try {
      const audioStream = await navigator.mediaDevices.getUserMedia({ audio: true });
      setStream(audioStream);
      mediaRecorder.current = new MediaRecorder(audioStream);
      mediaRecorder.current.ondataavailable = (event) => {
        if (event.data.size > 0) {
          setRecordedAudioBlob(event.data);
          if (onRecordingStart) onRecordingStart(event.data);
          setIsRecorded(true);
        }
      };
      mediaRecorder.current.start();
      setIsRecording(true);
    } catch (err) {
      console.error(err);
    }
  };

  const stopRecording = () => {
    if (mediaRecorder.current) {
      mediaRecorder.current.stop();
      setIsRecording(false);
    }
    if (stream) {
      let tracks = stream.getTracks();
      tracks.forEach((track) => track.stop());
      setStream(null);
    }
  };

  return (
    <YStack ai="center" jc="center" space>
      <Button icon={<Mic />} onPress={isRecording ? stopRecording : startRecording}>
        {isRecording ? 'Stop Recording' : 'Record Audio'}
      </Button>
    </YStack>
  );
};
