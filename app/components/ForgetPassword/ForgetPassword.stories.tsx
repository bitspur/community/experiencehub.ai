import React from 'react';
import { ForgetPassword } from './index';

export default {
  title: 'Components/ForgetPassword',
  component: ForgetPassword,
  parameters: { status: { type: 'beta' } },
};

export const main = (args) => <ForgetPassword {...args} />;
