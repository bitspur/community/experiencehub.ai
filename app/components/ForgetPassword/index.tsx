import React from 'react';
import { YStack, Text, H3, Input, Button } from 'ui';

export interface ForgetPasswordProps {
  setOpenForgotPassword: (openForgotPassword: boolean) => void;
  setOpenLogin: (openLogin: boolean) => void;
}

export const ForgetPassword = ({ setOpenForgotPassword, setOpenLogin }: ForgetPasswordProps) => {
  return (
    <YStack space>
      <H3 paddingBottom="$4" paddingLeft="$9">
        Reset Password
      </H3>
      <Text>Current Password</Text>
      <Input placeholder="Current Password" />
      <Text>Enter New Password</Text>
      <Input placeholder="Password" />
      <Text>Confirm Password</Text>
      <Input placeholder="Confirm Password" />
      <Button
        onPress={() => {
          setOpenForgotPassword(false);
          setOpenLogin(true);
        }}
      >
        Update Password
      </Button>
    </YStack>
  );
};
