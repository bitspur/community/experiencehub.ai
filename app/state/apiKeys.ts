/**
 * File: /state/apiKeys.ts
 * Project: app
 * File Created: 28-07-2023 16:49:55
 * Author: Lalit Rajak
 * -----
 * Last Modified: 28-07-2023 17:52:17
 * Modified By: Lalit Rajak
 * -----
 * Your Company (c) Copyright 2023
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { createStateStore } from 'multiplatform.one/zustand';
import { ApiKeysProps } from './types';

const { useStore } = createStateStore(
  'apiKeys',
  {
    keys: [] as ApiKeysProps[],
  },
  undefined,
  { persist: true },
);

export function useApiKeys() {
  return useStore();
}
