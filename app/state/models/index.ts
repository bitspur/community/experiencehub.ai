/*
 *  File: /state/models/index.ts
 *  Project: app
 *  File Created: 21-08-2023 12:23:20
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 22-08-2023 12:06:43
 *  Modified By: Lalit Rajak
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import { createStateStore } from 'multiplatform.one/zustand';

const { useStore } = createStateStore(
  'models',
  {
    localAiModels: [] as string[],
    openAiModels: [] as string[],
    models: [] as string[],
  },
  (set, get) => {
    return {
      setLocalAiModels: (models: string[]) => {
        set({ localAiModels: models });
        set({ models: [...get().openAiModels, ...models] });
      },
      setOpenAiModels: (models: string[]) => {
        set({ openAiModels: models });
        set({ models: [...get().localAiModels, ...models] });
      },
    };
  },
  undefined,
);

export function useModels() {
  return useStore();
}
