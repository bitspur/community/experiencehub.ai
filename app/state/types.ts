/**
 * File: /state/types.ts
 * Project: app
 * File Created: 28-07-2023 16:43:39
 * Author: Lalit Rajak
 * -----
 * Last Modified: 28-07-2023 17:34:57
 * Modified By: Lalit Rajak
 * -----
 * Your Company (c) Copyright 2023
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export enum KeyTypes {
  'OPENAI' = 'OPENAI',
  'HUGGINGFACE' = 'HUGGINGFACE',
  'LOCALAI' = 'LOCALAI',
}

export interface ApiKeysProps {
  name: KeyTypes;
  key: string;
}

export interface PayloadProps {
  model?: string;
  temperature?: number;
  n?: number;
  size?: string;
}
