/*
 *  File: /environments/proxy.ts
 *  Project: app
 *  File Created: 13-09-2023 17:35:46
 *  Author: dharmendra
 *  -----
 *  Last Modified: 13-09-2023 17:37:36
 *  Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { config } from 'app/config';
import { Environment } from 'app/environments';
import axios, { AxiosInstance } from 'axios';
import { useEffect, useState } from 'react';

export class ProxyEnvironment implements Environment {
  name = 'proxy';
  api: AxiosInstance;

  constructor(baseURL: string) {
    this.api = axios.create({
      baseURL,
    });
  }
}

export function useProxyEnvironment(): Environment | undefined {
  const [environment, setEnvironment] = useState<Environment | undefined>();
  const baseURL = config.get('PROXY_BASE_URL');
  useEffect(() => {
    if (!baseURL) return;
    setEnvironment(new ProxyEnvironment(baseURL));
  }, [baseURL]);
  return environment;
}
