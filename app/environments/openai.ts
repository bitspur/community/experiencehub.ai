/*
 *  File: /environments/openai.ts
 *  Project: app
 *  File Created: 29-08-2023 15:59:07
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 30-08-2023 14:48:14
 *  Modified By: Lalit Rajak
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { config } from 'app/config';
import { Environment } from 'app/environments';
import { KeyTypes, useApiKeys } from 'app/state';
import axios, { AxiosInstance } from 'axios';
import { useEffect, useState } from 'react';

export class OpenAiEnvironment implements Environment {
  name = 'openai';
  api: AxiosInstance;

  constructor(baseURL: string, apiKey: string) {
    this.api = axios.create({
      baseURL,
      headers: {
        Authorization: `Bearer ${apiKey}`,
        'Content-Type': 'application/json',
      },
    });
  }
}

export function useOpenAiEnvironment(): Environment | undefined {
  const [environment, setEnvironment] = useState<Environment | undefined>();
  const apiKeysState = useApiKeys();
  const openAiKey = apiKeysState.keys.find((key) => key.name === KeyTypes.OPENAI)?.key;
  const baseURL = config.get('OPENAI_BASE_URL');
  useEffect(() => {
    if (!openAiKey || !baseURL) return;
    setEnvironment(new OpenAiEnvironment(baseURL, openAiKey));
  }, [openAiKey, baseURL]);
  return environment;
}
