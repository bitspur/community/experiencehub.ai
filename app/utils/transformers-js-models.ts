/*
 *  File: /utils/transformers-js-models.ts
 *  Project: app
 *  File Created: 23-08-2023 15:59:33
 *  Author: dharmendra
 *  ----- *  Last Modified: 23-08-2023 15:59:33 *  Modified By: dharmendra *  ----- *  Your Company (c) Copyright 2023 * *  Licensed under the Apache License, Version 2.0 (the "License"); *  you may not use this file except in compliance with the License.  *  You may obtain a copy of the License at *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export enum TransformersJsNlpModels {
  BERT_SENTIMENT_ANALYSIS = 'nlptown/bert-base-multilingual-uncased-sentimen',
  BERT_FILL_MASK = 'bert-base-uncased',
  ROBERTA_FILL_MASK = 'roberta-base',
  ROBERTA_LARGE_FILL_MASK = 'roberta-large',
  ALPHA_QUESTION_ANSWERING = 'alphakavi22772023/bertQA',
  TIMPAL_QUESTION_ANSWERING = 'timpal0l/mdeberta-v3-base-squad2',
  LLUKAS_QUESTION_ANSWERING = 'LLukas22/all-MiniLM-L12-v2-qa-en',
  INTFLOAT_SENTANCE_SIMILARITY = 'intfloat/e5-large-v2',
  JINA_SENTANCE_SIMILARITY = 'jinaai/jina-embedding-s-en-v1',
  LLUKAS_SENTANCE_SIMILARITY = 'LLukas22/all-MiniLM-L12-v2-qa-en',
}
