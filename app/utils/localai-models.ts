/*
 *  File: /utils/localai-models.ts
 *  Project: app
 *  File Created: 22-08-2023 09:51:45
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 22-08-2023 10:03:04
 *  Modified By: Lalit Rajak
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
export enum LocalAiChatCompletions {
  BAICHUAN_VICUNA_7B_GGMLV3_Q4_0 = 'baichuan-vicuna-7b.ggmlv3.q4_0',
  GGML_GPT4ALL_J = 'ggml-gpt4all-j',
  GGML_MPT_7B_BASE = 'ggml-mpt-7b-base',
  GGML_MPT_7B_INSTRUCT = 'ggml-mpt-7b-instruct',
  OPEN_LLAMA_7B_OPEN_INSTRUCT_GGMLV3_Q4_0 = 'open-llama-7B-open-instruct.ggmlv3.q4_0',
  OPEN_LLAMA_3B_Q4_0 = 'open-llama-3b-q4_0',
}
