/*
 *  File: /utils/openai-models.ts
 *  Project: app
 *  File Created: 21-08-2023 15:22:42
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 31-08-2023 14:23:49
 *  Modified By: Lalit Rajak
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export enum OpenAiChatCompletions {
  GPT_4 = 'gpt-4',
  GPT_4_0613 = 'gpt-4-0613',
  GPT_4_32K = 'gpt-4.32k',
  GPT_4_32K_0613 = 'gpt-4.32k-0613',
  GPT_3_5_TURBO = 'gpt-3.5-turbo',
  GPT_3_5_TURBO_0613 = 'gpt-3.5-turbo-0613',
  GPT_3_5_TURBO_16K = 'gpt-3.5-turbo-16k',
  GPT_3_5_TURBO_16K_0613 = 'gpt-3.5-turbo-16k-0613',
}

export enum OpenAiCompletions {
  TEXT_DAVINCI_003 = 'text-davinci-003',
  TEXT_DAVINCI_002 = 'text-davinci-002',
  TEXT_DAVINCI_001 = 'text-davinci-001',
  TEXT_CURIE_001 = 'text-curie-001',
  TEXT_BABBAGE_001 = 'text-babbage-001',
  TEXT_ADA_001 = 'text-ada-001',
  DAVINCI = 'davinci',
  CURIE = 'curie',
  BABBAGE = 'babbage',
  ADA = 'ada',
}

export enum OpenAiAudioTranscriptions {
  WHISPER_1 = 'whisper-1',
}

export enum OpenAiAudioTranslations {
  WHISPER_1 = 'whisper-1',
}

export enum OpenAiFineTunes {
  DAVINCI = 'davinci',
  CURIE = 'curie',
  BABBAGE = 'babbage',
  ADA = 'ada',
}

export enum OpenAiEmbeddings {
  TEXT_EMBEDDING_ADA_002 = 'text-embedding-ada-002',
  TEXT_SIMILARITY_WILDCARD_001 = 'text-similarity-*-001',
  TEXT_SEARCH_WILDCARD_WILDCARD_001 = 'text-search-*-*-001',
  CODE_SEARCH_WILDCARD_WILDCARD_001 = 'code-search-*-*-001',
}

export enum OpenAiModerations {
  TEXT_MODERATION_STABLE = 'text-moderation-stable',
  TEXT_MODERATION_LATEST = 'text-moderation-latest',
}

export enum OpenAiImageGenerations {
  DAVINCI = 'davinci',
}
