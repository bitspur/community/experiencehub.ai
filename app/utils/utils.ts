/*
 *  File: /utils/utils.ts
 *  Project: app
 *  File Created: 19-08-2023 11:30:34
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 14-09-2023 15:44:30
 *  Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { LocalAiChatCompletions } from './localai-models';
import {
  OpenAiAudioTranscriptions,
  OpenAiAudioTranslations,
  OpenAiChatCompletions,
  OpenAiCompletions,
  OpenAiEmbeddings,
  OpenAiFineTunes,
  OpenAiImageGenerations,
  OpenAiModerations,
} from './openai-models';

export type Platform = 'ios' | 'android' | 'web' | 'api' | 'desktop';
export type Environment = 'localai' | 'openai' | 'proxy' | 'transformers' | 'transformersjs';
export type Experience =
  | 'text-generation'
  | 'image-generation'
  | 'audio-translation'
  | 'audio-transcription'
  | 'fine-tunes'
  | 'embeddings'
  | 'moderations';

export type Role = 'user' | 'assistant';

export const OpenAiModel = {
  ...OpenAiChatCompletions,
  ...OpenAiCompletions,
  ...OpenAiEmbeddings,
  ...OpenAiAudioTranscriptions,
  ...OpenAiAudioTranslations,
  ...OpenAiModerations,
  ...OpenAiFineTunes,
  ...OpenAiImageGenerations,
};

export const LocalAiModel = {
  ...LocalAiChatCompletions,
};

export const Model = {
  ...OpenAiModel,
  ...LocalAiModel,
};
