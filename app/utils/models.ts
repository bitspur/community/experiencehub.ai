/*
 *  File: /utils/models.ts
 *  Project: app
 *  File: /utils/models.ts
 *  Author: Lalit Rajak
 *  -----
 * Last Modified: Th-09-2023 12:39:50
 * Modified By: dharmendra
 * Last Modified: Th-09-2023 12:39:50
 * Modified By: dharmendra
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Environment, Experience, Platform } from './utils';
import ada from 'app/models/ada.json';
import babbage from 'app/models/babbage.json';
import baichuan_vicuna_7b_ggmlv3_q4_0 from 'app/models/baichuan-vicuna-7b.ggmlv3.q4_0.json';
import curie from 'app/models/curie.json';
import davinci from 'app/models/davinci.json';
import ggml_gpt4all_j from 'app/models/ggml-gpt4all-j.json';
import ggml_mpt_7b_base from 'app/models/ggml-mpt-7b-base.json';
import ggml_mpt_7b_instruct from 'app/models/ggml-mpt-7b-instruct.json';
import gpt_3_5_turbo_16k_0613 from 'app/models/gpt-3.5-turbo-16k-0613.json';
import gpt_3_5_turbo_16k from 'app/models/gpt-3.5-turbo-16k.json';
import gpt_3_5_turbo_0613 from 'app/models/gpt-3.5-turbo-0613.json';
import gpt_3_5_turbo from 'app/models/gpt-3.5-turbo.json';
import gpt_4_32k_0613 from 'app/models/gpt-4.32k-0613.json';
import gpt_4_32k from 'app/models/gpt-4.32k.json';
import gpt_4_0613 from 'app/models/gpt-4-0613.json';
import gpt_4 from 'app/models/gpt-4.json';
import open_llama_3b_q4_0 from 'app/models/open-llama-3b-q4_0.json';
import open_llama_7B_open_instruct_ggmlv3_q4_0 from 'app/models/open-llama-7B-open-instruct.ggmlv3.q4_0.json';
import text_ada_001 from 'app/models/text-ada-001.json';
import text_babbage_001 from 'app/models/text-babbage-001.json';
import text_curie_001 from 'app/models/text-curie-001.json';
import text_davinci_001 from 'app/models/text-davinci-001.json';
import text_davinci_002 from 'app/models/text-davinci-002.json';
import text_davinci_003 from 'app/models/text-davinci-003.json';
import whisper_1 from 'app/models/whisper-1.json';

const openai_image_generation: IModel = {
  name: 'openai-image-generation',
  description: 'OpenAI Image Generation',
  environments: ['openai', 'proxy'],
  experiences: ['image-generation'],
  platforms: ['web', 'api', 'ios', 'android', 'desktop'],
};

export const models: Map<string, IModel> = new Map();
models.set(ada.name, ada as IModel);
models.set(babbage.name, babbage as IModel);
models.set(baichuan_vicuna_7b_ggmlv3_q4_0.name, baichuan_vicuna_7b_ggmlv3_q4_0 as IModel);
models.set(curie.name, curie as IModel);
models.set(davinci.name, davinci as IModel);
models.set(ggml_gpt4all_j.name, ggml_gpt4all_j as IModel);
models.set(ggml_mpt_7b_base.name, ggml_mpt_7b_base as IModel);
models.set(ggml_mpt_7b_instruct.name, ggml_mpt_7b_instruct as IModel);
models.set(gpt_3_5_turbo_16k_0613.name, gpt_3_5_turbo_16k_0613 as IModel);
models.set(gpt_3_5_turbo_16k.name, gpt_3_5_turbo_16k as IModel);
models.set(gpt_3_5_turbo_0613.name, gpt_3_5_turbo_0613 as IModel);
models.set(gpt_3_5_turbo.name, gpt_3_5_turbo as IModel);
models.set(gpt_4_32k_0613.name, gpt_4_32k_0613 as IModel);
models.set(gpt_4_32k.name, gpt_4_32k as IModel);
models.set(gpt_4_0613.name, gpt_4_0613 as IModel);
models.set(gpt_4.name, gpt_4 as IModel);
models.set(open_llama_3b_q4_0.name, open_llama_3b_q4_0 as IModel);
models.set(open_llama_7B_open_instruct_ggmlv3_q4_0.name, open_llama_7B_open_instruct_ggmlv3_q4_0 as IModel);
models.set(text_ada_001.name, text_ada_001 as IModel);
models.set(text_babbage_001.name, text_babbage_001 as IModel);
models.set(text_curie_001.name, text_curie_001 as IModel);
models.set(text_davinci_001.name, text_davinci_001 as IModel);
models.set(text_davinci_002.name, text_davinci_002 as IModel);
models.set(text_davinci_003.name, text_davinci_003 as IModel);
models.set(whisper_1.name, whisper_1 as IModel);

export interface IModel {
  name: string;
  description: string;
  environments: Environment[];
  experiences: Experience[];
  platforms: Platform[];
}
