/*
 *  File: /utils/useExperiences.ts
 *  Project: app
 *  File Created: 24-08-2023 10:12:58
 *  Author: dharmendra
 *  -----
 *  Last Modified: 11-09-2023 11:35:22
 *  Modified By: Clay Risser
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export const useExperiences = () => {
  const experiences: ExperienceProps[] = [
    {
      heading: 'Multimodal',
      experiencesList: [
        { value: 'Image to Text', link: '/multi-modal/image-to-text' },
        { value: 'Text to Image', link: '/multi-modal/text-to-image' },
        { value: 'Document Question Answering', link: '/multi-modal/document-question-answering' },
        { value: 'Visual Question Answering', link: '/multi-modal/visual-question-answering' },
        { value: 'Feature Extraction', link: '/multi-modal/feature-extraction' },
      ],
    },
    {
      heading: 'ComputerVision',
      experiencesList: [
        { value: 'Image Classification', link: '/computer-vision/image-classification' },
        { value: 'Image Segmentation', link: '/computer-vision/image-segmentation' },
        { value: 'Object Detection', link: '/computer-vision/object-detection' },
        { value: 'Image-to-Image', link: '/computer-vision/image-to-image' },
        { value: 'Zero-Shot Image Classification', link: '/computer-vision/zero-shot-image-classification' },
      ],
    },
    {
      heading: 'NLP',
      experiencesList: [
        { value: 'Conversational', link: '/nlp/conversational' },
        { value: 'Fil-Mask', link: '/nlp/fill-mask' },
        { value: 'Question Answering', link: '/nlp/question-answering' },
        { value: 'Sentence Similarity', link: '/nlp/sentence-similarity' },
        { value: 'Summarization', link: '/nlp/summarization' },
        { value: 'Text Classification', link: '/nlp/text-classification' },
        { value: 'Table Question Answering', link: '/nlp/table-question-answering' },
        { value: 'TextToText Generation', link: '/nlp/text-to-text-generation' },
        { value: 'Text Generation', link: '/nlp/text-generation' },
        { value: 'Token Classification', link: '/nlp/token-classification' },
        { value: 'Translation', link: '/nlp/translation' },
        { value: 'Zero-Shot Classification', link: '/nlp/zero-shot-classification' },
      ],
    },
    {
      heading: 'Audio',
      experiencesList: [
        { value: 'Audio Classification', link: '/audio/audio-classification' },
        { value: 'Audio-to-Audio', link: '/audio/audio-to-audio' },
        { value: 'Automatic Speech Recognition', link: '/audio/automatic-speech-recognition' },
        { value: 'Text-to-Speech', link: '/audio/text-to-speech' },
      ],
    },
  ];
  return { experiences };
};

export interface ExperienceProps {
  heading: string;
  experiencesList: { value: string; link: string }[];
}
