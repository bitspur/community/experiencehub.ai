import React from 'react';
import type { ReactNode } from 'react';
import { createWithLayout } from 'multiplatform.one';
import { Header } from 'app/components/Header';
import { useWindowDimensions } from 'react-native';
import { Stack, YStack } from 'ui';

export interface DefaultLayoutProps {
  children: ReactNode;
}

function DefaultLayout({ children }: DefaultLayoutProps) {
  const { width } = useWindowDimensions();
  return (
    <YStack fullscreen>
      <Header position="absolute" zi={100} width={width} />
      <Stack pt={80} />
      {children}
    </YStack>
  );
}

export const withDefaultLayout = createWithLayout(DefaultLayout);
