/*
 *  File: /hooks/useAudio/native.ts
 *  Project: app
 *  File Created: 01-09-2023 20:42:51
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 01-09-2023 20:50:37
 *  Modified By: Lalit Rajak
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import React, { useState } from 'react';
import { Button, View } from 'react-native';
import { Audio } from 'expo-av';

export default function useAudioNative() {
  const [recordingInstance, setRecordingInstance] = useState(null);

  const startRecording = async () => {
    const { status } = await Audio.requestPermissionsAsync();
    if (status !== 'granted') return;

    const recording = new Audio.Recording();
    await recording.prepareToRecordAsync(Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY);

    recording.setOnRecordingStatusUpdate((status) => {
      if (status.canRecord) {
        // Do something while recording
      } else if (status.isDoneRecording) {
        // Do something after recording
      }
    });

    await recording.startAsync();
    setRecordingInstance(recording);
  };

  const stopRecording = async () => {
    await recordingInstance.stopAndUnloadAsync();
  };
}
const recordingSettings: any = {
  android: {
    extension: '.m4a',
    outputFormat: Audio.RECORDING_OPTION_ANDROID_OUTPUT_FORMAT_MPEG_4,
    audioEncoder: Audio.RECORDING_OPTION_ANDROID_AUDIO_ENCODER_AAC,
    sampleRate: 44100,
    numberOfChannels: 2,
    bitRate: 128000,
  },
  ios: {
    extension: '.caf',
    audioQuality: Audio.RECORDING_OPTION_IOS_AUDIO_QUALITY_MAX,
    sampleRate: 44100,
    numberOfChannels: 2,
    bitRate: 128000,
    linearPCMBitDepth: 16,
    linearPCMIsBigEndian: false,
    linearPCMIsFloat: false,
  },
};
