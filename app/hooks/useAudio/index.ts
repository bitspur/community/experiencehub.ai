/*
 *  File: /hooks/useAudio/index.ts
 *  Project: app
 *  File Created: 01-09-2023 10:25:32
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 01-09-2023 15:35:38
 *  Modified By: Lalit Rajak
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { useState } from 'react';

export interface IStartRecording {
  media: MediaRecorder;
  stream: MediaStream;
}

export interface IStopRecording {
  audioBlob: Blob;
  audio: HTMLAudioElement;
}

export function useAudio() {
  const [chunks, setChunks] = useState<Blob[]>([]);
  const startRecording = async (): Promise<IStartRecording> => {
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    const mediaRecorder = new MediaRecorder(stream);
    const audioChunks: Blob[] = [];
    mediaRecorder.addEventListener('dataavailable', (event) => {
      audioChunks.push(event.data);
      setChunks(audioChunks);
    });
    mediaRecorder.start();
    return { media: mediaRecorder, stream };
  };

  const stopRecording = (media: MediaRecorder | null, stream: MediaStream | null): IStopRecording => {
    if (media) {
      media.stop();
    }
    if (stream) {
      stream.getTracks().forEach((track) => track.stop());
    }
    const audioBlob = new Blob(chunks, { type: 'audio/wav' });
    const audio = new Audio(URL.createObjectURL(audioBlob));
    return { audioBlob, audio };
  };
  return { startRecording, stopRecording };
}
