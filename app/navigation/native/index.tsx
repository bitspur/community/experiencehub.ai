import React from 'react';
import ImageGeneration from 'app/screens/experiences/imageGeneration';
import Chat from 'app/screens/experiences/chat';
import TextGeneration from 'app/screens/nlp/textGeneration';
import VisualQuestionAnsweringScreen from 'app/screens/multiModal/visualQuestionAnswering';
import ImageToImageScreen from 'app/screens/computerVision/imageToImage';
import ApiKeysScreen from 'app/screens/settings/apiKeysScreen';
import AudioToAudioScreen from 'app/screens/audio/audioToAudio';
import HomeScreen from 'app/screens/home';
import ErrorScreen from 'app/screens/error';
import HelpScreen from 'app/screens/help';
import SettingsScreen from 'app/screens/settings';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import TextToTextGeneration from 'app/screens/nlp/textToTextGeneration';
import AccountScreen from 'app/screens/settings/accountScreen';
import TextClassificationScreen from 'app/screens/nlp/textClassification';
import FeatureExtraction from 'app/screens/multiModal/featureExtraction';

const Stack = createNativeStackNavigator<{
  imageGeneration: undefined;
  chat: undefined;
  textGeneration: undefined;
  visualQuestionAnswering: undefined;
  imageToImage: undefined;
  account: undefined;
  apiKeys: undefined;
  audioToAudio: undefined;
  home: undefined;
  error: undefined;
  help: undefined;
  settings: undefined;
  textToTextGeneration: undefined;
  textClassification: undefined;
  featureExtraction: undefined;
}>();

export function NativeNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="home"
        component={HomeScreen}
        options={{
          title: 'Home',
        }}
      />
      <Stack.Screen
        name="textToTextGeneration"
        component={TextToTextGeneration}
        options={{
          title: 'Text To Text',
        }}
      />
      <Stack.Screen
        name="error"
        component={ErrorScreen}
        options={{
          title: 'Error',
        }}
      />
      <Stack.Screen
        name="help"
        component={HelpScreen}
        options={{
          title: 'Help',
        }}
      />
      <Stack.Screen
        name="settings"
        component={SettingsScreen}
        options={{
          title: 'Settings',
        }}
      />

      <Stack.Screen
        name="textClassification"
        component={TextClassificationScreen}
        options={{
          title: 'Text Classification',
        }}
      />
      <Stack.Screen name="audioToAudio" component={AudioToAudioScreen} />
      <Stack.Screen name="apiKeys" component={ApiKeysScreen} />
      <Stack.Screen name="account" component={AccountScreen} />
      <Stack.Screen name="imageToImage" component={ImageToImageScreen} />
      <Stack.Screen name="visualQuestionAnswering" component={VisualQuestionAnsweringScreen} />
      <Stack.Screen name="textGeneration" component={TextGeneration} />
      <Stack.Screen name="featureExtraction" component={FeatureExtraction} />
      <Stack.Screen name="chat" component={Chat} />
      <Stack.Screen name="imageGeneration" component={ImageGeneration} />
    </Stack.Navigator>
  );
}

export const routeMaps = {
  imageGeneration: '/experiences/image-generation',
  chat: '/experiences/chat',
  textGeneration: '/nlp/text-generation',
  visualQuestionAnswering: '/multi-modal/visual-question-answering',
  featureExtraction: '/multi-modal/feature-extraction',
  imageToImage: '/computer-vision/image-to-image',
  account: '/settings/account',
  apiKeys: '/settings/api-keys',
  audioToAudio: '/audio/audio-to-audio',
  home: '',
  error: 'error',
  help: 'help',
  settings: 'settings',
  TextToTextGeneration: '/nlp/text-to-text-generation',
  textClassification: '/nlp/text-classification',
};
