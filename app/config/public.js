/*
 *  File: /config/public.js
 *  Project: app
 *  File Created: 07-09-2023 14:46:59
 *  Author: dharmendra
 *  -----
 *  Last Modified: 13-09-2023 17:39:53
 *  Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * File: /config/public.js
 * Project: app
 * File Created: 12-07-2023 12:36:17
 * Author: Lalit Rajak
 * -----
 * Last Modified: 28-07-2023 17:43:20
 * Modified By: Lalit Rajak
 * -----
 * Your Company (c) Copyright 2023
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

module.exports = {
  DEBUG: process.env.DEBUG,
  KEYCLOAK_BASE_URL: process.env.KEYCLOAK_BASE_URL,
  KEYCLOAK_CLIENT_ID: process.env.KEYCLOAK_CLIENT_ID,
  KEYCLOAK_ENABLED: process.env.KEYCLOAK_ENABLED,
  KEYCLOAK_REALM: process.env.KEYCLOAK_REALM,
  KEYCLOAK_SSR: process.env.KEYCLOAK_SSR,

  CROSS_STORAGE_HUB_URL: process.env.CROSS_STORAGE_HUB_URL,
  SENTRY_DSN: process.env.SENTRY_DSN,
  LOCALAI_BASE_URL: process.env.LOCALAI_BASE_URL,
  HUGGINGFACE_BASE_URL: process.env.HUGGINGFACE_BASE_URL,
  OPENAI_BASE_URL: process.env.OPENAI_BASE_URL,
  PROXY_BASE_URL: process.env.PROXY_BASE_URL,
};

delete process.env.DEBUG;
