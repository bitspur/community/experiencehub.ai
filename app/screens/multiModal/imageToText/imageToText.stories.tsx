import React from 'react';
import ImageToText from './index';

export default {
  title: 'screens/multimodal/imageToText',
  component: ImageToText,
  parameters: {
    status: {
      type: 'beta',
    },
  },
};

export const main = () => <ImageToText />;
