import React, { useState } from 'react';
import { YStack, Button, TextArea, SimpleImage, H1, H5, Separator, H4, Card } from 'ui';
import { Loading } from 'app/components/Loading';
import { ReactFileUploader } from 'app/components/ReactFileUploader';
import { withDefaultLayout } from 'app/layouts/Default';
import { useHuggingFace } from 'app/engines/huggingFace';

function ImageToText() {
  const [uploadedImage, setUploadedImage] = useState<File | null>(null);
  const [imageCaption, setImageCaption] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const huggingFace = useHuggingFace();

  const handleGenerateCaption = async () => {
    if (!uploadedImage) {
      console.error('Please upload an image');
      return;
    }
    setIsLoading(true);
    const reader = new FileReader();
    reader.onload = async function () {
      const result = reader.result as string;
      if (result) {
        const base64String = result.split(',')[1];
        const body = {
          inputs: {
            image: base64String,
          },
        };
        const generatedCaption = await huggingFace?.getImageToTextResponse(body);
        setImageCaption(generatedCaption[0].generated_text);
        setIsLoading(false);
      }
    };
    reader.readAsDataURL(uploadedImage);
  };

  const handleRemoveImage = () => {
    setUploadedImage(null);
  };

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1>Image To Text</H1>
      <H5>
        An image-to-text model, also known as an image captioning model, takes an image as input and generates
        <Separator borderWidth={'$0'} />
        descriptive text or captions that describe the contents of the image. These models combine computer vision
        <Separator borderWidth={'$0'} />
        techniques to understand the visual content of the image and natural language processing techniques to generate
        <Separator borderWidth={'$0'} />
        human-readable text.
      </H5>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H4>This model can be loaded on the Inference API on-demand.</H4>
        <ReactFileUploader onFileUpload={setUploadedImage} accept="image/*" />
        <YStack space ai="center" jc="center">
          {uploadedImage && (
            <YStack space ai="center" jc="center">
              <SimpleImage src={URL.createObjectURL(uploadedImage)} alt="Uploaded" width={400} height={350} />
              <Button onPress={handleRemoveImage}>Remove Image</Button>
            </YStack>
          )}
        </YStack>
      </YStack>
      <YStack space ai="center" jc="center">
        <Button onPress={handleGenerateCaption}>Press Compute to Generate Caption</Button>
      </YStack>
      {isLoading ? (
        <Loading speed={100} p={1} />
      ) : (
        <Card borderRadius="$10" backgroundColor="$background" padding="$4" ai="center" jc="center">
          <TextArea value={imageCaption} height={80} width={700} padding="$4" />
        </Card>
      )}
    </YStack>
  );
}

export default withDefaultLayout(ImageToText);
