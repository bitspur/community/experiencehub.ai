import React from 'react';
import VisualQuestionAnswering from './index';

export default {
  title: 'screens/multiModal/visualQuestionAnswering',
  component: VisualQuestionAnswering,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <VisualQuestionAnswering />;
