import React, { useState } from 'react';
import { H1, Input, YStack, H5, Separator, Button } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';

function FeatureExtraction() {
  const [question, setQuestion] = useState('');
  const [answer, setAnswer] = useState('');
  const huggingFace = useHuggingFace();

  interface featureExtractionRequestBody {
    inputs: string;
  }

  const onFinalCompute = async () => {
    const requestBody: featureExtractionRequestBody = {
      inputs: question,
    };

    const response = await huggingFace?.getFeatureExtraction(requestBody);
    setAnswer(response);
  };

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1>Feature Extraction</H1>
      <H5>
        Feature extraction refers to the process of transforming raw data into numerical
        <Separator borderWidth="$0" />
        features that can be processed while preserving the information in the original dataset.
      </H5>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H5>This model can be loaded on the Inference API on-demand.</H5>
        <Input value={question} onChangeText={setQuestion} placeholder="Enter raw data" width="100%" margin="10px" />
        <Button onPress={onFinalCompute}>Compute</Button>
      </YStack>
      <YStack padding="$15" space>
        {answer}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(FeatureExtraction);
