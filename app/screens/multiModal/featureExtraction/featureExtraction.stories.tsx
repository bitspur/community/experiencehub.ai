import React from 'react';
import  FeatureExtraction  from './index';

export default {
  title: 'screens/multiModal/FeatureExtraction',
  component:FeatureExtraction ,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <FeatureExtraction/>;


