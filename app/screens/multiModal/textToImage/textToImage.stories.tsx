import React from 'react';
import TextToImage from './index';

export default {
  title: 'screens/multimodal/textToImage',
  component: TextToImage,
  parameters: {
    status: {
      type: 'beta',
    },
  },
};

export const main = () => <TextToImage />;
