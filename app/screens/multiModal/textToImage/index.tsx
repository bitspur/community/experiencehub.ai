import React, { useEffect } from 'react';
import { SimpleImage, Stack, Text, XStack, YStack, useAssets } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { useOpenAi } from 'app/engines/openai';
import { Pencil } from '@tamagui/lucide-icons';
import {
  Engines,
  HuggingFaceModel,
  ImageGeneratorRequestBody,
  ImageSize,
  LocalAiModel,
  NumberOfImages,
  Role,
  Screens,
} from 'app/engines/types';
import { useLocalAi } from 'app/engines/localai';
import { SelectEngines } from 'app/components/SelectEngines';
import { MessageInput } from 'app/components/MessageInput';
import { ScrollView } from 'react-native';
import { Loading } from 'app/components/Loading';
import { Link } from 'solito/link';
import { SolitoImage } from 'solito/image';
import { OpenAiChatCompletions } from 'app/utils';
import { withDefaultLayout } from 'app/layouts/Default';

function TextToImage() {
  const [engine, setEngine] = React.useState<Engines>(Engines.HUGGINGFACE);
  const [size, setSize] = React.useState<ImageSize>(ImageSize.MEDIUM);
  const [numberOfImages, setNumberOfImages] = React.useState<NumberOfImages>(NumberOfImages.FIVE);
  const [textToImageChats, setTextToImageChats] = React.useState<TextToImageChatsData[]>([]);
  const [model, setModel] = React.useState<OpenAiChatCompletions | LocalAiModel | HuggingFaceModel | undefined>(
    HuggingFaceModel['stable-diffusion-2-1'],
  );
  const [loading, setLoading] = React.useState<boolean>(false);
  const [edit, setEdit] = React.useState<boolean>(false);
  const [editEngine, setEditEngine] = React.useState<boolean>(false);
  const huggingFace = useHuggingFace();
  const openAi = useOpenAi();
  const localAi = useLocalAi();
  const scrollViewRef = React.useRef<ScrollView>(null);
  const [logoPng] = useAssets(require('app/assets/logo.png'));

  async function handleSendRequest(prompt: string) {
    if (!(openAi || huggingFace)) return;
    if (!engine) return;
    setTextToImageChats((prev) => [...prev, { role: Role.USER, prompt }]);
    setLoading(true);
    setEditEngine(false);
    let generatedData: TextToImageChatsData;
    const body = {
      prompt,
      ...(engine === Engines.OPENAI ? { n: numberOfImages, size } : undefined),
      ...(engine === Engines.HUGGINGFACE ? { model } : undefined),
    } as ImageGeneratorRequestBody;
    switch (engine) {
      case Engines.OPENAI:
        const images = await openAi?.getGeneratedImages(body);
        generatedData = {
          role: Role.BOT,
          images,
        };
        break;
      case Engines.HUGGINGFACE:
        const huggingFaceResponse = await huggingFace?.getGeneratedImages(body);
        generatedData = {
          role: Role.BOT,
          images: [huggingFaceResponse as string],
        };
        break;
    }
    setTextToImageChats((prev) => [...prev, generatedData]);
    setLoading(false);
  }

  function handleEngineChange(engine: Engines) {
    setEngine(engine);
  }

  function handleSizeChange(size: ImageSize) {
    setSize(size);
  }

  function handleNumberOfImagesChange(numberOfImages: NumberOfImages) {
    setNumberOfImages(numberOfImages);
  }

  function handleModelChange(model: OpenAiChatCompletions | LocalAiModel | HuggingFaceModel) {
    setModel(model);
  }

  function handleImagePress(src: string) {
    window.open(src, '_blank');
  }

  function handlePencilPress() {
    setEditEngine(true);
    setEdit(false);
  }

  useEffect(() => {
    if (scrollViewRef.current) {
      scrollViewRef.current.scrollToEnd({ animated: true });
    }
  }, [textToImageChats.length]);
  function renderEngines() {
    return textToImageChats.length > 0 && !editEngine ? (
      <YStack jc="center" padding="$4">
        <XStack als="center" space="$2" onHoverIn={() => setEdit(true)} onHoverOut={() => setEdit(false)}>
          <Text als="center">{engine.toUpperCase()}</Text>
          {edit ? (
            <Stack cursor="pointer" onPress={handlePencilPress}>
              <Pencil size="$1" />
            </Stack>
          ) : null}
        </XStack>
      </YStack>
    ) : (
      <YStack padding="$4">
        <SelectEngines
          engine={engine}
          model={model}
          numberOfImage={numberOfImages}
          size={size}
          screen={Screens.TEXT_TO_IMAGE}
          onEngineChange={handleEngineChange}
          onModelChange={handleModelChange}
          onNumberOfImagesChange={handleNumberOfImagesChange}
          onSizeChange={handleSizeChange}
        />
      </YStack>
    );
  }

  return (
    <YStack fullscreen space>
      {renderEngines()}
      <YStack position="absolute" top={0}>
        <Link href="/">
          <SolitoImage src={logoPng} alt="Logo" height={80} width={80} style={{ borderRadius: 9999 }} />
        </Link>
      </YStack>
      <ScrollView ref={scrollViewRef} showsHorizontalScrollIndicator={false}>
        {textToImageChats.map((chat, index) => (
          <XStack p="$3" jc={chat.role === Role.USER ? 'flex-end' : 'flex-start'} key={index}>
            {chat.prompt && (
              <Text p="$3" bw={1} borderRadius={10}>
                {chat.prompt}
              </Text>
            )}

            <XStack display="flex" flexWrap="wrap" maxWidth="50%" gap={10}>
              {chat.images &&
                chat.images?.map((image, index) => (
                  <SimpleImage
                    mt={index > 0 ? 10 : 0}
                    mb={index < image.length - 1 ? 10 : 0}
                    src={image}
                    key={index}
                    height={300}
                    width={400}
                    onPress={() => handleImagePress(image)}
                    cursor="pointer"
                  />
                ))}
            </XStack>
          </XStack>
        ))}
        {loading && (
          <XStack jc="flex-start" padding="$4">
            <Loading speed={30} p={10} />
          </XStack>
        )}
        <YStack height={50} />
      </ScrollView>
      <YStack p="$3" bottom={0} pos="absolute" width="100%">
        <MessageInput onSend={handleSendRequest} />
      </YStack>
    </YStack>
  );
}

export interface TextToImageChatsData {
  role: Role;
  prompt?: string;
  images?: string[];
}

export default withDefaultLayout(TextToImage);
