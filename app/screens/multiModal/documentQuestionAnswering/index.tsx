import React, { useState } from 'react';
import { YStack, Input, Button, H1, H5, Separator, H4, SimpleImage, Progress } from 'ui';
import { ReactFileUploader } from 'app/components/ReactFileUploader';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';
import { DocumentQuestionAnsweringRequestBody } from 'app/engines/types';

function DocumentQuestionAnswering() {
  const [question, setQuestion] = useState('');
  const [answer, setAnswer] = useState<{ score: number; answer: string }[]>([]);
  const [uploadedImage, setUploadedImage] = useState<string>('');
  const [imageBlob, setImageBlob] = useState<Blob | null>(null);
  const huggingFace = useHuggingFace();

  const handleFileUpload = async (file: File) => {
    console.log('files', file);
    const url = URL.createObjectURL(file);
    setUploadedImage(url);
    const res = await fetch(url);
    const blob = await res.blob();
    setImageBlob(blob);
  };

  async function blobToBase64(blob: Blob): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        if (event.target?.result) {
          const dataUrl = event.target.result as string;
          const base64Match = dataUrl.match(/^data:(.*;base64,)?(.*)$/);
          if (base64Match && base64Match[2]) {
            resolve(base64Match[2]);
          } else {
            reject(new Error('Invalid data URL format.'));
          }
        } else {
          reject(new Error('Unable to convert blob to base64.'));
        }
      };
      reader.onerror = reject;
      reader.readAsDataURL(blob);
    });
  }

  const handleSubmit = async () => {
    const base64 = await blobToBase64(imageBlob as Blob);
    const body = {
      inputs: {
        image: base64,
        question,
      },
    } as DocumentQuestionAnsweringRequestBody;

    if (!imageBlob) return;
    const classificationData = await huggingFace?.getDocumentQuestionAnsweringResponse(body);
    setAnswer(classificationData);
  };

  return (
    <YStack>
      <YStack space ai="center" jc="center">
        <H1>Document Question Answering</H1>
        <H5>
          Document Question Answering (also known as Document Visual Question Answering) is the
          <Separator borderWidth={'$0'} />
          task of answering questions on document images. Document question answering models take a
          <Separator borderWidth={'$0'} />
          (document, question) pair as input and return an answer in natural language. Models usually
          <Separator borderWidth={'$0'} />
          rely on multi-modal features, combining text, position of words (bounding-boxes) and image.
        </H5>
        <YStack
          space
          ai="center"
          jc="center"
          marginLeft="$8"
          marginTop="$3"
          marginBottom="$3"
          borderRadius={'$10'}
          backgroundColor="$backgroundHover"
          padding="$6"
          width={800}
        >
          <H4>This model can be loaded on the Inference API on-demand.</H4>
          <ReactFileUploader onFileUpload={handleFileUpload} accept={'image/*'} />
          {uploadedImage && <SimpleImage src={uploadedImage} width={450} height={450} alt="uploaded image" />}
          <Input value={question} onChangeText={setQuestion} placeholder="Ask a question" width="100%" margin="10px" />
        </YStack>
        <Button onPress={handleSubmit} width="20%" margin="10px">
          Submit
        </Button>
        <YStack space m="$6">
          {answer?.map((item, index) => (
            <YStack key={index}>
              <H4>{item.answer}</H4>
              <Progress size="$4" value={item.score * 100}>
                <Progress.Indicator animation="bouncy" />
              </Progress>
            </YStack>
          ))}
        </YStack>
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(DocumentQuestionAnswering);
