import React from 'react';
import DocumentQuestionAnswering from './index';

export default {
  title: 'screens/multiModal/documentQuestionAnswering',
  component: DocumentQuestionAnswering,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <DocumentQuestionAnswering />;
