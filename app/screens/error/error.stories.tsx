import React from 'react';
import ErrorScreen from './index';

export default {
  title: 'screens/Error',
  component: ErrorScreen,
  parameters: {
    status: {
      type: 'beta',
    },
  },
};

export const main = () => <ErrorScreen />;
