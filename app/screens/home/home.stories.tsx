import React from 'react';
import HomeScreen from './index';

export default {
  title: 'screens/Home',
  component: HomeScreen,
  parameters: {
    status: {
      type: 'beta',
    },
  },
};

export const main = () => <HomeScreen />;
