import React from 'react';
import { YStack, ScrollView, H1, H3, useAssets, Square, Paragraph, Card, XStack, H2, useWindowDimensions } from 'ui';
import { SolitoImage } from 'solito/image';
import { ArrowRight } from '@tamagui/lucide-icons';
import { Platform } from 'react-native';
import { Link } from 'solito/link';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';

const HomeScreen = () => {
  const [ai1png] = useAssets(require('app/assets/ai1.png'));
  const { width } = useWindowDimensions();
  const huggingFace = useHuggingFace();

  return (
    <YStack fullscreen pt={80} space backgroundColor="$backgroundHover">
      <ScrollView>
        <YStack space>
          <YStack ai="center" jc="center" paddingTop="$4">
            <H1>The AI Community</H1>
            <H2>Building The Future.</H2>
          </YStack>
          <XStack alignItems="center" justifyContent="center" flexWrap="wrap">
            <Square animation={'bouncy'} hoverStyle={{ scale: 1.2 }}>
              <SolitoImage src={ai1png} width={500} height={330} alt="A cool bot's image." />
            </Square>
          </XStack>
        </YStack>
        <Paragraph fontSize="$6" textAlign="center">
          Build, train and deploy state of the art models powered by the reference open source in machine learning.
        </Paragraph>
        <ScrollView horizontal={Platform.OS !== 'web'}>
          <XStack jc="center" p="$8" flexWrap="wrap">
            <Link href="https://chat.openai.com/">
              <Card
                borderWidth="$1"
                height={340}
                width={300}
                p="$4"
                m="$5"
                space
                animation="quick"
                hoverStyle={{ scale: 1.2 }}
              >
                <XStack>
                  <H3>ChatGpt</H3>
                  <ArrowRight />
                </XStack>
                <Paragraph fontSize="$4" textAlign="center">
                  ChatGPT is an advanced conversational AI model that utilizes a deep learning architecture to generate
                  human-like responses to text-based inputs. It has been trained on a massive amount of diverse internet
                  text and can engage in natural and interactive conversations with users.
                </Paragraph>
              </Card>
            </Link>
            <Link href="https://bard.google.com/">
              <Card
                borderWidth="$1"
                height={340}
                width={300}
                p="$4"
                m="$5"
                space
                animation="quick"
                hoverStyle={{ scale: 1.2 }}
              >
                <XStack ai="center" jc="space-evenly">
                  <H3>Bard AI</H3>
                  <ArrowRight />
                </XStack>
                <Paragraph fontSize="$4" textAlign="center">
                  Google's newest, revolutionary AI-powered chatbot, like ChatGPT, Bard AI, is developed with their
                  existing LaMDA AI platform; it is an experimental conversational AI service that promises to have a
                  big impact on the world of AI.
                </Paragraph>
              </Card>
            </Link>
            <Link href="https://github.com/features/copilot">
              <Card
                borderWidth="$1"
                height={340}
                width={300}
                p="$4"
                m="$5"
                space
                animation="quick"
                hoverStyle={{ scale: 1.2 }}
              >
                <XStack ai="center" jc="space-evenly">
                  <H3>CoPilot</H3>
                  <ArrowRight />
                </XStack>
                <Paragraph fontSize="$4" textAlign="center">
                  GitHub CoPilot is GitHub's latest feature, utilizing OpenAI Codex's GPT-3 model for auto-completion
                  capabilities. This powerful tool is compatible with several popular coding platforms like VS Code,
                  Neovim, and JetBrains and also enables cloud workflows with GitHub Codespaces.
                </Paragraph>
              </Card>
            </Link>
          </XStack>
        </ScrollView>
      </ScrollView>
    </YStack>
  );
};

export default withDefaultLayout(HomeScreen);
