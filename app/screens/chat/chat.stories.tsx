import React from 'react';
import Chat from '.';

export default {
  title: 'Screens/Chat',
  component: Chat,
  parameters: {
    status: {
      type: 'beta',
    },
  },
};

export const Default = () => <Chat />;
