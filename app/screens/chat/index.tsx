import {
  useMedia,
  Select,
  Slider,
  Text,
  XStack,
  YStack,
  useAssets,
  isWeb,
  useWindowDimensions,
  Button,
  Stack,
} from 'ui';
import React, { useEffect, useState } from 'react';
import { SelectSimple } from '@multiplatform.one/components';
import { Environment, Experience, Role } from 'app/utils';
import { Platform, ScrollView } from 'react-native';
import { MessageInput } from 'app/components/MessageInput';
import { ImageBlock } from 'app/components/ImageBlock';
import { TextBlock } from 'app/components/TextBlock';
import { Menu, X } from '@tamagui/lucide-icons';
import { ChatConfig, useChatProtocol } from 'app/protocols/chat';
import {
  ImageGenerationConfig,
  NumberOfImages,
  useImageGenerationProtocol,
  ImageSize,
} from 'app/protocols/image-generation';
import { Protocol } from 'app/protocols/protocol';
import { Link } from 'solito/link';
import { SolitoImage } from 'solito/image';
import { useLocalAIEnvironment, useOpenAiEnvironment, useProxyEnvironment } from 'app/environments';
import { IModel, models } from 'app/utils/models';
import { useAudio } from 'app/hooks';
import { useAudioToTextProtocol } from 'app/protocols/audio-to-text';
import ReactPlayer from 'react-player';
import { useApiKeys } from 'app/state';
import { Loading } from 'app/components/Loading';

export interface IChat {
  id: string;
  role: Role;
  prompt: string;
  experience?: Experience;
  images?: string[];
  audioUrl?: string | undefined;
}

function Chat() {
  const [model, setModel] = useState<IModel | undefined>();
  const [environments, setEnvironments] = useState<Environment[]>([]);
  const [environment, setEnvironment] = useState<string | undefined>(undefined);
  const [experiences, setExperiences] = useState<Experience[]>([]);
  const [experience, setExperience] = useState<Experience | undefined>(undefined);
  const [chats, setChats] = useState<IChat[]>([]);
  const [temperature, setTemperature] = useState<number[]>([0.2]);
  const [numberOfImage, setNumberOfImage] = useState<string | undefined>(undefined);
  const [imageSize, setImageSize] = useState<string | undefined>(undefined);
  const [openSideBar, setOpenSideBar] = useState<boolean>(false);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState<boolean>(false);
  const [recordedMedia, setRecordedMedia] = useState<MediaRecorder | null>(null);
  const [audioUrl, setAudioUrl] = useState<string | undefined>(undefined);
  const [audioBlob, setAudioBlob] = useState<Blob | null>(null);
  const [stream, setStream] = useState<MediaStream | null>(null);
  const scrollViewRef = React.useRef<ScrollView>(null);
  const media = useMedia();
  const [logoPng] = useAssets(require('app/assets/logo.png'));
  const localAiEnvironment = useLocalAIEnvironment();
  const openAiEnvironment = useOpenAiEnvironment();
  const proxyEnvironment = useProxyEnvironment();
  const chatProtocol = useChatProtocol();
  const imageGenerationProtocol = useImageGenerationProtocol();
  const audioToTextProtocol = useAudioToTextProtocol();
  const { width, height } = useWindowDimensions();
  const { startRecording, stopRecording } = useAudio();
  const [url, setUrl] = useState<string | undefined>(undefined);
  const [waitingForResponse, setWaitingForResponse] = useState<boolean>(false);
  const apiKeysState = useApiKeys();

  useEffect(() => {
    if (scrollViewRef.current) {
      scrollViewRef.current.scrollToEnd({ animated: true });
    }
  }, [chats.length]);

  async function handleModelChange(model) {
    setEnvironments([]);
    setExperience(undefined);
    setExperiences([]);
    const modelObject = models.get(model);
    setModel(modelObject);
    const availableEnvironments: Environment[] = [];

    modelObject?.environments.map((environment) => {
      switch (environment) {
        case 'localai':
          if (localAiEnvironment) return availableEnvironments.push('localai');
          break;
        case 'openai':
          if (openAiEnvironment) return availableEnvironments.push('openai');
          break;
        case 'proxy':
          if (proxyEnvironment) return availableEnvironments.push('proxy');
      }
    });
    setEnvironments(availableEnvironments);
  }

  function handleEnvironmentChange(environment) {
    setEnvironment(environment);
    setExperiences([]);
    setExperiences(model?.experiences || []);
  }

  async function handleSendRequest(prompt: string) {
    if (!(prompt || audioBlob)) return;
    setChats((prev) => [
      ...prev,
      { id: Protocol.generateRandomString(16), prompt, role: 'user', experience: experience as Experience, audioUrl },
    ]);
    if (audioBlob) {
      setAudioBlob(null);
      setAudioUrl(undefined);
    }
    const environment = getEnvironment();
    if (!environment) return;
    switch (experience) {
      case 'text-generation': {
        if (!chatProtocol || !model?.name) return;
        const chatHistory = chats.map((chat) => {
          return { role: chat.role, content: chat.prompt };
        });
        const chatConfig: ChatConfig = {
          model: model.name,
          temperature: temperature[0]!,
          messages: [...chatHistory, { role: 'user', content: prompt }],
        };
        setWaitingForResponse(true);
        const chat = await chatProtocol.autoGenerate(environment, chatConfig);
        setWaitingForResponse(false);
        return setChats((prev) => (chat ? [...prev, chat] : prev));
      }
      case 'image-generation': {
        if (!imageGenerationProtocol) return;
        const imageGenerationConfig: ImageGenerationConfig = {
          prompt,
          n: Number(numberOfImage),
          size: imageSize as ImageSize,
        };
        setWaitingForResponse(true);
        const generatedImageChat = await imageGenerationProtocol.autoGenerate(environment, imageGenerationConfig);
        setWaitingForResponse(false);
        return setChats((prev) => (generatedImageChat ? [...prev, generatedImageChat] : prev));
      }
      case 'audio-translation': {
        if (!audioToTextProtocol || !audioBlob || !model?.name) return;
        const audioToTextConfig = new FormData();
        audioToTextConfig.append('file', audioBlob, `my-audio-${Math.random() * 100}.mp3`);
        audioToTextConfig.append('model', model?.name);
        const audioToTextChat = await audioToTextProtocol.autoGenerate(environment, audioToTextConfig);
        return setChats((prev) => (audioToTextChat ? [...prev, audioToTextChat] : prev));
      }
    }
  }

  function getEnvironment() {
    switch (environment) {
      case 'localai':
        if (localAiEnvironment) return localAiEnvironment;
        return proxyEnvironment;
      case 'openai':
        if (openAiEnvironment) return openAiEnvironment;
        return proxyEnvironment;
      default:
        return proxyEnvironment;
    }
  }

  async function handleSendAudio() {
    const { media, stream } = await startRecording();
    setRecordedMedia(media);
    setStream(stream);
  }

  function handleStop() {
    const { audioBlob } = stopRecording(recordedMedia, stream);
    setAudioBlob(audioBlob);
    setAudioUrl(URL.createObjectURL(audioBlob));
  }

  async function handleSendAudioRequest() {
    if (!audioBlob) return;
    const audioToTextConfig = new FormData();
    audioToTextConfig.append('file', audioBlob, 'my-audio.mp3');
    audioToTextConfig.append('model', 'whisper-1');
    const environment = getEnvironment();
    if (!environment) return;
    const res = await audioToTextProtocol?.autoGenerate(environment, audioToTextConfig);
    console.log('response', res);
  }

  return (
    <XStack fullscreen backgroundColor="$backgroundPress">
      <YStack width={width} height={height} {...(isWeb && { width: '70%', height: '100%' })} $xs={{ width: '100%' }}>
        <ScrollView ref={scrollViewRef} showsHorizontalScrollIndicator={false}>
          {chats.map((chat, index) => (
            <XStack p="$3" jc={chat.role === 'user' ? 'flex-end' : 'flex-start'} key={index}>
              {renderChat(chat)}
            </XStack>
          ))}
          <YStack height={100} bg="$backgroundTransparent" />
        </ScrollView>

        <MessageInput
          y={isKeyboardOpen && !isWeb ? -310 : 0}
          paddingBottom={Platform.OS === 'web' ? '$3' : '$10'}
          onSend={handleSendRequest}
          waitingForResponse={waitingForResponse}
          position="absolute"
          bottom={0}
          width="100%"
          onFocus={() => setIsKeyboardOpen(true)}
          onBlur={() => setIsKeyboardOpen(false)}
        />

        {(media.xs || !isWeb) && openSideBar && (
          <YStack
            width={width}
            height={height}
            position="absolute"
            backgroundColor="rgba(0,0,0,0.5)"
            onPress={() => setOpenSideBar(false)}
          />
        )}
      </YStack>
      {(openSideBar || media.gtXs) && (
        <YStack
          width="30%"
          height="100%"
          bg="$backgroundFocus"
          animation="lazy"
          enterStyle={{ x: 100 }}
          {...(!isWeb && { position: 'absolute', top: 0, right: 0, width: width * 0.58 })}
          $xs={{ zIndex: 15, position: 'absolute', top: 0, right: 0, width: '60%' }}
        >
          {renderSideBar()}
        </YStack>
      )}
      {(media.xs || !isWeb) && !openSideBar && (
        <YStack pos="absolute" top={10} right={10} cursor="pointer" onPress={() => setOpenSideBar(true)}>
          <Menu size={24} />
        </YStack>
      )}
      <YStack position="absolute">
        <Link href="/">
          <SolitoImage src={logoPng} alt="Logo" height={60} width={60} style={{ borderRadius: 9999 }} />
        </Link>
      </YStack>
    </XStack>
  );

  function renderChat(chat: IChat) {
    if (chat.prompt) {
      return <TextBlock chat={chat} />;
    }
    switch (chat.experience) {
      case 'text-generation':
        return <TextBlock chat={chat} />;
      case 'image-generation':
        return <ImageBlock chat={chat} />;
      case 'audio-translation':
        return <ReactPlayer url={chat.audioUrl} controls={true} width={300} height={30} />;
      default:
        return <>{}</>;
    }
  }

  function renderSideBar() {
    return (
      <ScrollView>
        <YStack space padding="$4">
          {(media.xs || !isWeb) && openSideBar && (
            <YStack position="absolute" right={10} top={10} cur="pointer" onPress={() => setOpenSideBar(false)}>
              <X />
            </YStack>
          )}
          <SelectSimple
            jc="center"
            placeholder="select one of these"
            onValueChange={handleModelChange}
            value={model?.name}
          >
            {Array.from(models.keys())?.map((model, i) => (
              <Select.Item index={i} value={model} key={i}>
                <Select.ItemText>{model}</Select.ItemText>
              </Select.Item>
            ))}
          </SelectSimple>
          {environments.length > 0 && (
            <SelectSimple
              jc="center"
              placeholder="select the environment"
              onValueChange={handleEnvironmentChange}
              value={environment}
            >
              {environments?.map((environment, i) => (
                <Select.Item index={i} value={environment} key={i}>
                  <Select.ItemText>{environment}</Select.ItemText>
                </Select.Item>
              ))}
            </SelectSimple>
          )}
          {experiences.length > 0 && (
            <SelectSimple
              jc="center"
              placeholder="select one of these"
              onValueChange={(text) => setExperience(text as Experience)}
              value={experience}
            >
              {experiences?.map((experience, i) => (
                <Select.Item index={i} value={experience} key={i}>
                  <Select.ItemText>{experience}</Select.ItemText>
                </Select.Item>
              ))}
            </SelectSimple>
          )}
          {experience === 'text-generation' && (
            <YStack padding="$2" space>
              <Text>Temperature [{temperature[0]}]</Text>
              <Slider
                name="Temperature"
                min={0}
                max={1}
                step={0.1}
                onValueChange={setTemperature}
                value={temperature || [0]}
              >
                <Slider.Track size="$6">
                  <Slider.TrackActive br={0} bg="limegreen" />
                </Slider.Track>
                <Slider.Thumb size="$2" theme="SliderThumb" circular index={0} />
              </Slider>
            </YStack>
          )}
          {experience === 'image-generation' && (
            <YStack>
              <SelectSimple
                jc="center"
                placeholder="number of images"
                onValueChange={setNumberOfImage}
                value={numberOfImage}
              >
                {Object.values(NumberOfImages)
                  .filter((value) => !isNaN(Number(value)))
                  .map((value, i) => (
                    <Select.Item index={i} value={value as string} key={i}>
                      <Select.ItemText>{value}</Select.ItemText>
                    </Select.Item>
                  ))}
              </SelectSimple>
              <SelectSimple jc="center" placeholder="image size" value={imageSize} onValueChange={setImageSize}>
                {Object.values(ImageSize).map((value, i) => (
                  <Select.Item index={i} value={value as string} key={i}>
                    <Select.ItemText>{value}</Select.ItemText>
                  </Select.Item>
                ))}
              </SelectSimple>
            </YStack>
          )}
          {experience === 'audio-translation' && (
            <YStack space>
              <Button onPress={handleSendAudio}>Record</Button>
              <Button onPress={handleStop}>stop</Button>
              <Button onPress={handleSendAudioRequest}>send</Button>
            </YStack>
          )}
        </YStack>
      </ScrollView>
    );
  }
}

export default Chat;
