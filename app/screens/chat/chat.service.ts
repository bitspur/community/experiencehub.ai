/*
 *  File: /screens/chat/chat.service.ts
 *  Project: app
 *  File Created: 25-08-2023 16:11:08
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 28-08-2023 10:43:50
 *  Modified By: Lalit Rajak
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { config } from 'app/config';
import { ConnectionConfig } from 'app/protocols/types';
import { KeyTypes, useApiKeys } from 'app/state';
import {
  Environment,
  Experience,
  LocalAiChatCompletions,
  LocalAiModel,
  Model,
  OpenAiAudioTranscriptions,
  OpenAiAudioTranslations,
  OpenAiChatCompletions,
  OpenAiImageGenerations,
  OpenAiModel,
} from 'app/utils';

export interface IChatService {
  getSupportedEnvironments(model: string): Environment[];
  getSupportedExperiences(environment: Environment, model: string): Experience[];
  getOpenAiExperiences(model: string): Experience[];
  getLocalAiExperiences(model: string): Experience[];
  getConnectionConfig(environment: Environment, apiKeyState: ReturnType<typeof useApiKeys>): ConnectionConfig;
  replaceModel(model: string): string;
}

export class ChatService implements IChatService {
  constructor() {}

  getSupportedEnvironments(model: string): Environment[] {
    const environments: Environment[] = [];
    if (OpenAiModel[this.replaceModel(model)]) {
      environments.push(Environment.OpenAI);
    }
    if (LocalAiModel[this.replaceModel(model)]) {
      environments.push(Environment.LocalAI);
    }
    return environments;
  }

  getSupportedExperiences(environment: Environment, model: string): Experience[] {
    const experiences: Experience[] = [];
    if (environment === Environment.OpenAI) {
      experiences.push(...this.getOpenAiExperiences(model));
    }
    if (environment === Environment.LocalAI) {
      experiences.push(...this.getLocalAiExperiences(model));
    }
    return experiences;
  }

  getOpenAiExperiences(model: string): Experience[] {
    const experiences: Experience[] = [];
    if (OpenAiChatCompletions[this.replaceModel(model)]) {
      experiences.push(Experience.TEXT_GENERATION);
    }
    if (OpenAiAudioTranscriptions[this.replaceModel(model)]) {
      experiences.push(Experience.AUDIO_GENERATION);
    }
    if (OpenAiAudioTranslations[this.replaceModel(model)]) {
      experiences.push(Experience.AUDIO_TRANSLATION);
    }
    if (OpenAiImageGenerations[this.replaceModel(model)]) {
      experiences.push(Experience.IMAGE_GENERATION);
    }
    return experiences;
  }

  getLocalAiExperiences(model: string): Experience[] {
    const experiences: Experience[] = [];
    if (LocalAiChatCompletions[this.replaceModel(model)]) {
      experiences.push(Experience.TEXT_GENERATION);
    }
    return experiences;
  }

  getConnectionConfig(environment: Environment, apiKeyState: ReturnType<typeof useApiKeys>): ConnectionConfig {
    switch (environment) {
      case Environment.OpenAI:
        return {
          baseUrl: config.get('OPENAI_BASE_URL') || '',
          apiKey: apiKeyState.keys.find((key) => key.name === KeyTypes.OPENAI)?.key,
        };
      case Environment.LocalAI:
        return {
          baseUrl: config.get('LOCALAI_BASE_URL') || '',
          apiKey: apiKeyState.keys.find((key) => key.name === KeyTypes.LOCALAI)?.key,
        };
      default:
        return {} as ConnectionConfig;
    }
  }

  replaceModel(model: string): string {
    return model.replace(/-/g, '_').replace(/[.]/g, '_').toUpperCase();
  }
}
