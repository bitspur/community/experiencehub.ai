import React from 'react';
import ImageClassification from './index';

export default {
  title: 'screens/computerVision/imageClassification',
  component: ImageClassification,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <ImageClassification />;
