import React, { useState } from 'react';
import { Button, H1, H4, H5, YStack, Progress, SimpleImage, Separator } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';
import { Platform } from 'react-native';
import { ReactFileUploader } from 'app/components/ReactFileUploader';

function ImageClassification() {
  const huggingFace = useHuggingFace();
  const [uploadedImage, setUploadedImage] = useState('');
  const [imageBlob, setImageBlob] = useState<Blob | null>(null);
  const [classification, setClassification] = useState<{ label: string; score: number }[]>([]);

  const handleFileUpload = async (file: File) => {
    console.log('files', file);
    const url = URL.createObjectURL(file);
    setUploadedImage(url);
    const res = await fetch(url);
    const blob = await res.blob();
    setImageBlob(blob);
  };

  const handleImageClassification = async () => {
    if (!imageBlob) return;
    const classificationData = await huggingFace?.getImageResponse(imageBlob);
    setClassification(classificationData);
  };

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1>Image Classification</H1>
      <H5>
        Image classification is the process of categorizing and labeling groups of pixels or{' '}
        <Separator borderWidth={'$0'} />
        vectors within an image based on specific rules. The categorization devised using
        <Separator borderWidth={'$0'} />
        one or more spectral or textural characteristics. Two general methods of classification
        <Separator borderWidth={'$0'} />
        are 'supervised' and 'unsupervised'.
      </H5>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H4>This model can be loaded on the Inference API on-demand.</H4>
        {Platform.OS === 'web' ? <ReactFileUploader accept="image/*" onFileUpload={handleFileUpload} /> : null}
        {uploadedImage && <SimpleImage src={uploadedImage} width={300} height={300} alt="uploaded image" />}
      </YStack>
      <Button onPress={handleImageClassification}>Press Compute After Uploading Image</Button>
      <YStack space>
        {classification?.map((item, index) => (
          <YStack key={index}>
            <H4>{item.label}</H4>
            <H5>{item.score.toFixed(2)}</H5>
            <Progress size="$4" value={item.score * 100}>
              <Progress.Indicator animation="bouncy" />
            </Progress>
          </YStack>
        ))}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(ImageClassification);
