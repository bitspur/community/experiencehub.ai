import React, { useState } from 'react';
import { Button, H1, H4, H5, YStack, SimpleImage, Separator } from 'ui';
import { withDefaultLayout } from 'app/layouts/Default';
import { useHuggingFace } from 'app/engines/huggingFace';
import { Platform } from 'react-native';
import { ReactFileUploader } from 'app/components/ReactFileUploader';

function ImageToImage() {
  const huggingFace = useHuggingFace();
  const [status, setStatus] = useState(false);
  const [resultImage, setResultImage] = useState<File | null>(null);
  const [uploadedImage, setUploadedImage] = useState('');
  const [imageBlob, setImageBlob] = useState<Blob | null>(null);

  const handleFileUpload = async (file: File) => {
    console.log('files', file);
    const url = URL.createObjectURL(file);
    setUploadedImage(url);
    const res = await fetch(url);
    const blob = await res.blob();
    setImageBlob(blob);
  };

  const ImageToImagehandler = async () => {
    console.log('Button pressed, setting status to true');
    setStatus(true);
  };

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1>Image To Image</H1>
      <H5>
        Image-to-image is the task of transforming a source image to match the characteristics{' '}
        <Separator borderWidth={'$0'} />
        of a target image or a target image domain. Any image manipulation and enhancement is{' '}
        <Separator borderWidth={'$0'} />
        possible with image to image models.
      </H5>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H4>This model can be loaded on the Inference API on-demand.</H4>
        {Platform.OS === 'web' ? <ReactFileUploader accept="image/*" onFileUpload={handleFileUpload} /> : null}
        {uploadedImage && <SimpleImage src={uploadedImage} width={300} height={300} alt="uploaded image" />}
      </YStack>
      <Button onPress={ImageToImagehandler}>Press Compute After Uploading Image</Button>
      <YStack space>{status && <H5>Request failed with status code 500</H5>}</YStack>
    </YStack>
  );
}

export default withDefaultLayout(ImageToImage);
