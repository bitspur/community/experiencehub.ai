import React from 'react';
import ImageToImage from './index';

export default {
  title: 'screens/computerVision/ImageToImage',
  component: ImageToImage,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <ImageToImage />;
