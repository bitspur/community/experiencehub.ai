import React, { useState } from 'react';
import { Button, H1, H4, H5, YStack, Progress, SimpleImage, Separator } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';
import { Platform } from 'react-native';
import { ReactFileUploader } from 'app/components/ReactFileUploader';

function ImageSegmentation() {
  const huggingFace = useHuggingFace();
  const [uploadedImage, setUploadedImage] = useState('');
  const [imageBlob, setImageBlob] = useState<Blob | null>(null);
  const [segmentation, setSegmentation] = useState<{ label: string; score: number }[]>([]);

  const handleFileUpload = async (file: File) => {
    console.log('files', file);
    const url = URL.createObjectURL(file);
    setUploadedImage(url);
    const res = await fetch(url);
    const blob = await res.blob();
    setImageBlob(blob);
  };

  const handleImageSegmentation = async () => {
    if (!imageBlob) return;
    const segmentationData = await huggingFace?.getImageSegmentationResponse(imageBlob);
    setSegmentation(segmentationData);
  };

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1>Image Segmentation</H1>
      <H5>
        The primary goal of image segmentation is to divide an image into multiple distinct regions or segments,
        <Separator borderWidth={'$0'} />
        where each segment corresponds to a specific object or region of interest. It provides pixel-level
        <Separator borderWidth={'$0'} />
        information by labeling each pixel with its corresponding segment or class. The output is a detailed mask
        <Separator borderWidth={'$0'} />
        or labeling of the entire image.
      </H5>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H4>This model can be loaded on the Inference API on-demand.</H4>
        {Platform.OS === 'web' ? <ReactFileUploader accept="image/*" onFileUpload={handleFileUpload} /> : null}
        {uploadedImage && <SimpleImage src={uploadedImage} width={300} height={300} alt="uploaded image" />}
      </YStack>
      <Button onPress={handleImageSegmentation}>Press Compute After Uploading Image</Button>
      <YStack space>
        {segmentation?.map((item, index) => (
          <YStack key={index}>
            <H4>{item.label}</H4>
            <H5>{item.score.toFixed(2)}</H5>
            <Progress size="$4" value={item.score * 100}>
              <Progress.Indicator animation="bouncy" />
            </Progress>
          </YStack>
        ))}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(ImageSegmentation);
