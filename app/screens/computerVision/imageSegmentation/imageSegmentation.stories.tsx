import React from 'react';
import ImageSegmentation from './index';

export default {
  title: 'screens/computerVision/imageSegmentation',
  component: ImageSegmentation,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <ImageSegmentation />;
