import React from 'react';
import ObjectDetection from './index';

export default {
  title: 'screens/computerVision/objectDetection',
  component: ObjectDetection,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <ObjectDetection />;
