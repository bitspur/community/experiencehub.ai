import React from 'react';
import { YStack, Button, H5, H1, H4, SimpleImage, Progress, Separator } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';
import { Platform } from 'react-native';
import { ReactFileUploader } from 'app/components/ReactFileUploader';

function ObjectDetection() {
  const huggingFace = useHuggingFace();
  const [uploadedImage, setUploadedImage] = React.useState('');
  const [imageBlob, setImageBlob] = React.useState<Blob | null>(null);
  const [result, setResult] = React.useState<{ label: string; score: number }[] | null>(null);

  const handleFileUpload = async (file: File) => {
    console.log('files', file);
    const url = URL.createObjectURL(file);
    setUploadedImage(url);
    const res = await fetch(url);
    const blob = await res.blob();
    setImageBlob(blob);
  };

  const handleObjectDetection = async () => {
    if (!imageBlob) return;
    const resultData = await huggingFace?.getObjectDetectionResponse(imageBlob);
    setResult(resultData);
  };

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1>Object Detection</H1>
      <H5>
        Object detection is a computer vision task that involves identifying and locating objects within an image or
        <Separator borderWidth={'$0'} />
        video stream. This task is widely used in various applications, including autonomous vehicles, surveillance,
        <Separator borderWidth={'$0'} />
        image analysis, and more.
      </H5>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H4>This model can be loaded on the Inference API on-demand.</H4>
        {Platform.OS === 'web' ? <ReactFileUploader accept="image/*" onFileUpload={handleFileUpload} /> : null}
        {uploadedImage && <SimpleImage src={uploadedImage} width={300} height={300} alt="uploaded image" />}
      </YStack>
      <Button onPress={handleObjectDetection}>Press Compute After Uploading Image</Button>
      <YStack space>
        {result?.map((item, index) => (
          <YStack key={index}>
            <H4>{item.label}</H4>
            <H5>{item.score.toFixed(2)}</H5>
            <Progress size="$4" value={item.score * 100}>
              <Progress.Indicator animation="bouncy" />
            </Progress>
          </YStack>
        ))}
        {result && result.length === 0 && <H5>No Object Was Detected! </H5>}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(ObjectDetection);
