import React, { useState } from 'react';
import { YStack, Button, H1, H5, H4, Input, Progress, SimpleImage, Separator, XStack } from 'ui';
import { zeroShotImageClassifierResponse } from 'app/engines/types';
import { useHuggingFace } from 'app/engines/huggingFace';
import { Platform } from 'react-native';
import { ReactFileUploader } from 'app/components/ReactFileUploader';
import { withDefaultLayout } from 'app/layouts/Default';

function ZeroShotImageClassification() {
  const huggingFace = useHuggingFace();
  const [uploadedImage, setUploadedImage] = useState<string>('');
  const [candidate_labels, setCandidateLabels] = useState<string>('');
  const [imageBlob, setImageBlob] = useState<Blob | null>(null);
  const [classification, setClassification] = useState<{ label: string; score: number }[]>([]);

  const handleFileUpload = async (file: File) => {
    console.log('files', file);
    const url = URL.createObjectURL(file);
    setUploadedImage(url);
    const res = await fetch(url);
    const blob = await res.blob();
    setImageBlob(blob);
  };

  async function blobToBase64(blob: Blob): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        if (event.target?.result) {
          const dataUrl = event.target.result as string;
          const base64Match = dataUrl.match(/^data:(.*;base64,)?(.*)$/);
          if (base64Match && base64Match[2]) {
            resolve(base64Match[2]);
          } else {
            reject(new Error('Invalid data URL format.'));
          }
        } else {
          reject(new Error('Unable to convert blob to base64.'));
        }
      };
      reader.onerror = reject;
      reader.readAsDataURL(blob);
    });
  }

  const handleClassifiedData = async () => {
    const base64 = await blobToBase64(imageBlob as Blob);
    const body = {
      image: base64,
      parameters: {
        candidate_labels,
      },
    } as zeroShotImageClassifierResponse;

    const classificationData = await huggingFace?.getZeroShotImageClassificationResponse(body);
    setClassification(classificationData);
  };

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1>Zero-Shot Image Classification</H1>
      <H5>
        Zero-shot image classification refers to the task of classifying images into categories
        <Separator borderWidth={'$0'} />
        without requiring any specific training examples of those categories during the model
        <Separator borderWidth={'$0'} />
        training phase. Instead, zero-shot classification relies on the model's ability to
        <Separator borderWidth={'$0'} />
        generalize and understand the relationships between different classes based on the
        <Separator borderWidth={'$0'} />
        textual descriptions or attributes associated with those classes.
      </H5>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H4>This model can be loaded on the Inference API on-demand.</H4>
        {Platform.OS === 'web' ? <ReactFileUploader accept="image/*" onFileUpload={handleFileUpload} /> : null}
        <XStack space ai="center" jc="center">
          <H5>Labels :</H5>
          <Input
            placeholder="Enter your possible labels here"
            value={candidate_labels}
            onChangeText={setCandidateLabels}
            width={300}
          />
        </XStack>
        {uploadedImage && <SimpleImage src={uploadedImage} width={450} height={450} alt="uploaded image" />}
      </YStack>
      <Button onPress={handleClassifiedData}>Press Compute After Uploading Image</Button>
      <YStack space>
        {classification?.map((item, index) => (
          <YStack key={index}>
            <H4>{item.label}</H4>
            <H5>{item.score.toFixed(2)}</H5>
            <Progress size="$4" value={item.score * 100}>
              <Progress.Indicator animation="bouncy" />
            </Progress>
          </YStack>
        ))}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(ZeroShotImageClassification);
