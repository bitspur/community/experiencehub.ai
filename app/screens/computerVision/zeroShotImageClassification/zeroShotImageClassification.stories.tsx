import React from 'react';
import ZeroShotImageClassification from './index';

export default {
  title: 'screens/computerVision/ZeroShotImageClassification',
  component: ZeroShotImageClassification,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <ZeroShotImageClassification />;
