import React, { useState, useEffect, useRef } from 'react';
import { YStack, XStack, Stack, Text, useAssets } from 'ui';
import { useOpenAi } from 'app/engines/openai';
import { Engines, HuggingFaceModel, LocalAiModel, Role, Screens, chatRequest } from 'app/engines/types';
import { MessageBubble } from '../../../components/MessageBubble';
import { Loading } from '../../../components/Loading';
import { MessageInput } from '../../../components/MessageInput';
import { useWindowDimensions } from 'ui';
import { useLocalAi } from 'app/engines/localai';
import { ScrollView } from 'react-native';
import { useHuggingFace } from 'app/engines/huggingFace';
import { SelectEngines } from 'app/components/SelectEngines';
import { Link } from 'solito/link';
import { SolitoImage } from 'solito/image';
import { useModels } from 'app/state/models';
import { OpenAiChatCompletions } from 'app/utils';
import { Pencil } from '@tamagui/lucide-icons';
import { Protocol } from 'app/protocols/protocol';
import { withDefaultLayout } from 'app/layouts/Default';

interface MessageProps {
  id: string;
  message: string;
  isUser: boolean;
}

function TextToTextGeneration() {
  const openAi = useOpenAi();
  const localAi = useLocalAi();
  const huggingFace = useHuggingFace();
  const [data, setData] = useState<MessageProps[]>([]);
  const [disabled, setDisabled] = useState(false);
  const [loading, setLoading] = useState(false);
  const [completed, setCompleted] = useState(false);
  const { height } = useWindowDimensions();
  const [engine, setEngine] = useState<Engines>(Engines.HUGGINGFACE);
  const [model, setModel] = useState<OpenAiChatCompletions | LocalAiModel | HuggingFaceModel | undefined>(undefined);
  const [temperature, setTemperature] = useState<number[]>([0.5]);
  const [logoPng] = useAssets(require('app/assets/logo.png'));
  const containerRef = useRef<ScrollView | null>(null);
  const modelsState = useModels();
  const [edit, setEdit] = React.useState<boolean>(false);
  const [editEngine, setEditEngine] = React.useState<boolean>(false);

  useEffect(() => {
    if (!modelsState) return;
    console.log(modelsState);
  }, [modelsState]);

  useEffect(() => {
    if (containerRef.current) {
      containerRef.current.scrollToEnd({ animated: true });
    }
  }, [data.length]);

  function getEngineByName(engine) {
    switch (engine) {
      case Engines.OPENAI:
        return openAi;
      case Engines.HUGGINGFACE:
        return huggingFace;
      case Engines.LOCALAI:
        return localAi;
    }
  }

  async function handleSendRequest(content: string) {
    if (!engine || !model || !content) return;
    if (!getEngineByName(engine)) return;
    setDisabled(true);
    setData((prev) => [...prev, { id: Protocol.generateRandomString(12), message: content, isUser: true }]);
    const payload = {
      messages: [{ role: Role.USER, content }],
      temperature: temperature[0],
      model,
    } as chatRequest;
    const response = await getEngineByName(engine)?.getChatResponse(payload);
    setLoading(false);
    setData((prev) => [...prev, { id: response.id, message: response.choices[0].message.content, isUser: false }]);
  }

  function handleEngineChange(engine: Engines) {
    setEngine(engine);
  }

  function handleTemperatureChange(temperature: number[]) {
    setTemperature(temperature);
  }

  function handleModelChange(model: OpenAiChatCompletions | HuggingFaceModel | LocalAiModel) {
    setModel(model);
  }

  function handlePencilPress() {
    setEditEngine(true);
    setEdit(false);
  }

  useEffect(() => {
    setDisabled(false);
  }, [completed]);

  function renderEngines() {
    return data.length > 0 && !editEngine ? (
      <YStack jc="center" padding="$4">
        <XStack als="center" space="$2" onHoverIn={() => setEdit(true)} onHoverOut={() => setEdit(false)}>
          <Text als="center">{engine.toUpperCase()}</Text>
          {edit ? (
            <Stack cursor="pointer" onPress={handlePencilPress}>
              <Pencil size="$1" />
            </Stack>
          ) : null}
        </XStack>
      </YStack>
    ) : (
      <YStack padding="$4">
        <SelectEngines
          engine={engine}
          model={model}
          temperature={temperature}
          onTemperatureChange={handleTemperatureChange}
          onModelChange={handleModelChange}
          onEngineChange={handleEngineChange}
          screen={Screens.TEXT_TO_TEXT}
        />
      </YStack>
    );
  }

  return (
    <YStack jc="space-between" height={height} bc="#FFFBEB" fullscreen>
      {renderEngines()}
      <YStack position="absolute" top={0}>
        <Link href="/">
          <SolitoImage src={logoPng} alt="Logo" height={80} width={80} style={{ borderRadius: 9999 }} />
        </Link>
      </YStack>
      <ScrollView ref={containerRef} showsHorizontalScrollIndicator={false}>
        {data.map((item, index) => (
          <YStack p="$2" key={index}>
            <MessageBubble
              isTypeWriter={true}
              key={index}
              p="$2"
              ai="center"
              jc={item.isUser ? 'flex-end' : 'flex-start'}
              bg={item.isUser === true ? '#DFD7BF' : '#E1ECC8'}
              bottomRightRadius={item.isUser ? 0 : 10}
              bottomLeftRadius={item.isUser ? 10 : 0}
              text={item.message}
              speed={30}
              onComplete={setCompleted}
              isUser={item.isUser}
              avatarContent={item.isUser ? 'UR' : 'AI'}
              avatarBg={!item.isUser ? '#E1ECC8' : '#DFD7BF'}
            />
          </YStack>
        ))}
        {loading && <Loading p="$3" speed={100} />}
        <YStack height={20} />
      </ScrollView>
      <MessageInput p="$3" onSend={handleSendRequest} disabled={disabled} />
    </YStack>
  );
}

export default withDefaultLayout(TextToTextGeneration);
