import React from 'react';
import TextToTextGeneration from './index';

export default {
  title: 'screens/nlp/textToTextGeneration',
  component: TextToTextGeneration,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <TextToTextGeneration />;
