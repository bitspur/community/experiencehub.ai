import React from 'react';
import TableQuestionAnswering from './index';

export default {
  title: 'screens/nlp/tableQuestionAnswering',
  component: TableQuestionAnswering,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <TableQuestionAnswering />;
