import React, { useState } from 'react';
import { YStack, Input, Button, H1, Table, TextArea } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { TableQuestionAnsweringResponse } from 'app/engines/types';
import { withDefaultLayout } from 'app/layouts/Default';

function TableQuestionAnswering() {
  const [repository, setRepository] = useState('');
  const [stars, setStars] = useState('');
  const [contributors, setContributors] = useState('');
  const [programmingLanguage, setProgrammingLanguage] = useState('');
  const [transformers, setTransformers] = useState('');
  const [datasets, setDatasets] = useState('');
  const [tokenizers, setTokenizers] = useState('');
  const [answer, setAnswer] = useState('');
  const [question, setQuestion] = useState<string>('');
  const huggingFace = useHuggingFace();
  const table = {
    table: {
      Repository: ['Transformers', 'Datasets', 'Tokenizers'],
      Stars: ['36555', '7484', '9443'],
      Contributors: ['100', '100', '100'],
      ProgrammingLanguage: ['Python', 'Python', 'Python,Node and Java'],
    },
  };

  const handleAnswerQuestion = async () => {
    if (!question) return;
    const body: TableQuestionAnsweringResponse = {
      prompt: question,
      table: table.table,
    };

    const response = await huggingFace?.getTableQuestionAnsweringResponse(body);
    setAnswer(response.data.cells[0]);
  };

  return (
    <YStack>
      <YStack w="100%" ai="center">
        <YStack space="$10">
          <YStack ai="center">
            <H1>Table Question Answering</H1>
          </YStack>
          <Table heading="Table">
            <Table.Col>
              <Table.Cell fontWeight="700">Repository</Table.Cell>
              {table.table.Repository.map((item, i) => (
                <Table.Cell key={i}>{item}</Table.Cell>
              ))}
            </Table.Col>
            <Table.Col>
              <Table.Cell fontWeight="700">Stars</Table.Cell>
              {table.table.Stars.map((item, i) => (
                <Table.Cell key={i}>{item}</Table.Cell>
              ))}
            </Table.Col>
            <Table.Col>
              <Table.Cell fontWeight="700">Contributors</Table.Cell>
              {table.table.Contributors.map((item, i) => (
                <Table.Cell key={i}>{item}</Table.Cell>
              ))}
            </Table.Col>
            <Table.Col>
              <Table.Cell fontWeight="700">Programming Language</Table.Cell>
              {table.table.ProgrammingLanguage.map((item, i) => (
                <Table.Cell key={i}>{item}</Table.Cell>
              ))}
            </Table.Col>
          </Table>
        </YStack>
        <YStack ai="center" space>
          <Input
            h="$10"
            w={400}
            placeholder="Ask a question about the table..."
            value={question}
            onChangeText={setQuestion}
          />
          <Button w="$16" onPress={handleAnswerQuestion}>
            Get Answers
          </Button>
          <TextArea placeholder="Answer" value={answer} onChangeText={setAnswer} />
        </YStack>
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(TableQuestionAnswering);
