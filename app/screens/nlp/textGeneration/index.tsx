import React, { useState } from 'react';
import { YStack, XStack, Input, TextArea, Button, H1, Text, H5 } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';
import { getTextGenerationResponseBody } from 'app/engines/types';
import { useWindowDimensions } from 'react-native';

export function TextGeneration() {
  const [inputQuestion, setInputQuestion] = useState<string>('');
  const [generatedAnswer, setGeneratedAnswer] = useState<string>('');
  const huggingFace = useHuggingFace();
  const { width } = useWindowDimensions();

  async function handleComputePrompt() {
    const body = {
      inputs: inputQuestion,
    } as getTextGenerationResponseBody;

    const response = await huggingFace?.getTextGeneration(body);
    setGeneratedAnswer(response[0].generated_text);
  }

  return (
    <YStack fullscreen mt={80} ai="center" pt={100} space>
      <H1>Text Generation</H1>
      <H5>Hosted inference API</H5>
      <XStack space>
        <Text fontSize="$3">gpt-2</Text>
      </XStack>
      <YStack space>
        <XStack space>
          <Input size="$4" placeholder="Enter your question" value={inputQuestion} onChangeText={setInputQuestion} />
          <Button onPress={handleComputePrompt}>Compute</Button>
        </XStack>
      </YStack>
      <YStack width={width * 0.8}>
        {generatedAnswer.length > 0 && <TextArea size="$6" value={generatedAnswer} />}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(TextGeneration);
