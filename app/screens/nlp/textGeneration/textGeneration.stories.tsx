import React from 'react';
import TextGeneration from './index';

export default {
  title: 'Screens/nlp/TextGeneration',
  component: TextGeneration,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <TextGeneration />;
