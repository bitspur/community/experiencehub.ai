import React from 'react';
import Summarization from './index';

export default {
  title: 'screens/nlp/Summarization',
  component: Summarization,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <Summarization />;
