import React, { useState } from 'react';
import { ScrollView, Button, H1, Separator, YStack, TextArea, H5 } from 'ui';
import { SummerizationInputRequest } from 'app/engines/types';
import { useHuggingFace } from 'app/engines/huggingFace';
import { Loading } from 'app/components/Loading';
import { withDefaultLayout } from 'app/layouts/Default';

function Summarization() {
  const [inputText, setInputText] = useState<string>('');
  const [summary, setSummary] = useState<string>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const huggingFace = useHuggingFace();

  const onHandleClick = async () => {
    setIsLoading(true);
    const body = {
      prompt: inputText,
    } as SummerizationInputRequest;

    const result = await huggingFace?.getSummarizationResponse(body);
    setSummary(result[0].summary_text);
    setIsLoading(false);
  };

  return (
    <ScrollView backgroundColor="$backgroundHover">
      <YStack space>
        <YStack ai="center" jc="center" space>
          <H1>Summarization</H1>
          <Separator />
          <H5 paddingBottom="$2">
            Summarization is the task of producing a shorter version of a document while
            <Separator borderWidth="$0" />
            preserving its important information.Some models can extract text from the original
            <Separator borderWidth="$0" />
            input, while other models can generate entirely new text.
          </H5>
        </YStack>
        <YStack space ai="center" jc="center">
          <TextArea
            width="60%"
            height={200}
            value={inputText}
            onChangeText={(text) => setInputText(text)}
            borderRadius={'$4'}
            borderWidth={'$2'}
            placeholder="Enter a paragraph for summary"
          />
          <Button onPress={onHandleClick}>Compute</Button>
          {isLoading ? (
            <Loading />
          ) : (
            summary && (
              <YStack
                padding="$4"
                width="75%"
                ai="center"
                jc="center"
                borderRadius={'$4'}
                borderWidth={'$0.5'}
                backgroundColor={'$backgroundHover'}
              >
                <H5>{summary}</H5>
              </YStack>
            )
          )}
        </YStack>
      </YStack>
    </ScrollView>
  );
}

export default withDefaultLayout(Summarization);
