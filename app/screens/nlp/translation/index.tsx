import React, { useState } from 'react';
import { YStack, Button, H1, TextArea, SelectSimple, Select, H4, H6, SimpleTooltip, ScrollView } from 'ui';
import { Loading } from 'app/components/Loading';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';

function Translation() {
  const [inputText, setInputText] = useState('');
  const [translatedText, setTranslatedText] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [sourceLanguageCode, setSourceLanguageCode] = useState<string | undefined>(undefined);
  const [targetLanguageCode, setTargetLanguageCode] = useState<string | undefined>(undefined);
  const huggingface = useHuggingFace();
  const languageOptions = [
    { value: 'en', label: 'English' },
    { value: 'de', label: 'German' },
    { value: 'hi', label: 'Hindi' },
    { value: 'es', label: 'Spanish' },
    { value: 'fr', label: 'French' },
    { value: 'zh', label: 'Chinese' },
    { value: 'ru', label: 'Russian' },
    { value: 'el', label: 'Greek' },
  ];

  const translateText = async () => {
    if (!sourceLanguageCode || !targetLanguageCode) {
      console.error('Please select source and target languages');
      return setIsLoading(false);
    }
    setIsLoading(true);
    try {
      const response = await huggingface?.getTranslatedText({
        inputs: inputText,
        sourceLanguageCode,
        targetLanguageCode,
      });
      setTranslatedText(response[0].translation_text);
    } catch (error) {
      console.error('Error translating text:', error);
    } finally {
      setIsLoading(false);
    }
  };

  function handleKeyPress(event) {
    if (event.key === 'Enter') {
      event.preventDefault();
      setInputText('');
    }
  }

  return (
    <YStack>
      <YStack ai="center" mr="$6" ml="$6">
        <ScrollView>
          <H1 mt="$2.5" space alignSelf="center">
            LANGUAGE TRANSLATOR
          </H1>
          <H4 mt="$2.5" space alignSelf="center">
            Empowering Connection Beyond Words
          </H4>
          <H6 mt="$2.5" space alignSelf="center">
            Experience seamless communication across languages with our Translation App. Connect with the world, break
            barriers, and share your message effortlessly.
          </H6>
          <YStack ai="center" space="$6" mt="$10">
            <YStack w="16%">
              <SelectSimple
                value={sourceLanguageCode}
                onValueChange={setSourceLanguageCode}
                jc="center"
                placeholder="Select Source Language"
              >
                {languageOptions.map((option, index) => (
                  <Select.Item jc="center" value={option.value} index={index} key={index}>
                    <Select.ItemText fontWeight="700">{option.label}</Select.ItemText>
                  </Select.Item>
                ))}
              </SelectSimple>
            </YStack>
            <TextArea
              h="$10"
              w="80%"
              value={inputText}
              onChangeText={setInputText}
              placeholder="Enter text to translate..."
              fontSize="$8"
              onKeyPress={handleKeyPress}
            />
            <YStack w="16%">
              <SelectSimple
                value={targetLanguageCode}
                onValueChange={setTargetLanguageCode}
                jc="center"
                placeholder="Select Target Language"
              >
                {languageOptions.map((option, index) => (
                  <Select.Item value={option.value} index={index} jc="center" key={index}>
                    <Select.ItemText fontWeight="700">{option.label}</Select.ItemText>
                  </Select.Item>
                ))}
              </SelectSimple>
            </YStack>
            <YStack jc="center" ai="center">
              <SimpleTooltip
                children={
                  <Button
                    mt="$8"
                    animation={'bouncy'}
                    hoverStyle={{ scale: 1.2 }}
                    width="$20"
                    fontWeight="700"
                    fontSize="$8"
                    onPress={translateText}
                  >
                    Translate
                  </Button>
                }
                element={!sourceLanguageCode && !targetLanguageCode && 'Please Select Source and Target Language'}
                paragraphStyle={{ color: 'red' }}
              />
            </YStack>
            {isLoading ? (
              <Loading speed={100} p={1} />
            ) : (
              <YStack ai="center" space="$4">
                <H4>Translated Text:</H4>
                <TextArea w="300%" h="$10" value={translatedText} onChangeText={setTranslatedText} fontSize="$8" />
              </YStack>
            )}
          </YStack>
        </ScrollView>
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(Translation);
