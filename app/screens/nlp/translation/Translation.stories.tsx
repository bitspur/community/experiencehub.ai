import React from 'react';
import Translation from './index';

export default {
  title: 'screens/nlp/translation',
  component: Translation,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <Translation />;
