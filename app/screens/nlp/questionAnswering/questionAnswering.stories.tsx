import React from 'react';
import QuestionAnswering from './index';

export default {
  title: 'screens/nlp/questionAnswering',
  component: QuestionAnswering,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <QuestionAnswering />;
