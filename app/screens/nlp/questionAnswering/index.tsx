import { questionAnswerRequest } from 'app/engines/types';
import React from 'react';
import { YStack, XStack, Input, TextArea, Button, H1, Text } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';

function QuestionAnswering() {
  const [inputText, setInputText] = React.useState<string>('');
  const [dataContext, setDataContext] = React.useState<string>('');
  const [answer, setAnswer] = React.useState<any>([]);
  const huggingFace = useHuggingFace();

  async function handleComputePrompt() {
    const body = {
      question: inputText,
      context: dataContext,
    } as questionAnswerRequest;

    const finalAnswer = await huggingFace?.getAnswerFromQuestion(body);
    setAnswer([finalAnswer.answer, finalAnswer.score]);
  }

  return (
    <YStack space>
      <YStack space="$8" ai="center" jc="center">
        <H1>Question Answering</H1>
        <XStack space>
          <Text fontSize="$3">deepset/tinyroberta-squad2</Text>
        </XStack>
        <YStack space>
          <Text>Context:</Text>
          <TextArea placeholder="Enter Context" value={dataContext} onChangeText={setDataContext} height={250} />
          <Text>Enter Question:</Text>
          <XStack space>
            <Input size="$4" placeholder="Enter your question" value={inputText} onChangeText={setInputText} />
            <Button onPress={handleComputePrompt}>Compute</Button>
          </XStack>
          {answer.length > 0 && (
            <YStack>
              <TextArea size="$4" placeholder="Output" value={`Answer:${answer[0]} \nScore:${answer[1]}`} />
            </YStack>
          )}
        </YStack>
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(QuestionAnswering);
