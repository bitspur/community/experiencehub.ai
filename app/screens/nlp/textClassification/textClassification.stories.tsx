import React from 'react';
import TextClassification from './index';

export default {
  title: 'screens/nlp/textClassification',
  component: TextClassification,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <TextClassification />;
