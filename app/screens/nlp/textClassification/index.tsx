import { TextClassifierResponse } from 'app/engines/types';
import React from 'react';
import { YStack, XStack, Button, Input, H1, Text, Progress, Label, Paragraph } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';

function TextClassification() {
  const [inputText, setInputText] = React.useState<string>('');
  const [result, setResult] = React.useState<any>([]);
  const huggingFace = useHuggingFace();

  async function handleCompute() {
    const body = {
      prompt: inputText,
    } as TextClassifierResponse;

    const response = await huggingFace?.getTextClassificationResponse(body);
    setResult(response);
  }

  return (
    <YStack space>
      <YStack space="$6" ai="center" jc="center">
        <H1>Text Classification</H1>
        <Text>
          Text classification is the process of assigning tags or categories to text according to its content.
        </Text>
        <XStack space>
          <Text fontSize="$3">SamLowe/roberta-base-go_emotions</Text>
        </XStack>
        <XStack space>
          <Input onChangeText={(text) => setInputText(text)} size="$4" placeholder="Enter your text" />
          <Button onPress={handleCompute}>Compute</Button>
        </XStack>
        {result && result.length > 0
          ? result[0].map((item, index) => (
              <YStack key={index}>
                <Label fontSize={20}>{item.label}</Label>
                <Paragraph>{item.score.toFixed(2)}</Paragraph>
                <Progress size="$4" value={item.score * 100}>
                  <Progress.Indicator animation="bouncy" />
                </Progress>
              </YStack>
            ))
          : null}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(TextClassification);
