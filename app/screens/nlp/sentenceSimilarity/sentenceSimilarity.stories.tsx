import React from 'react';
import SentenceSimilarity from './index';

export default {
  title: 'screens/nlp/sentenceSimilarity',
  component: SentenceSimilarity,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <SentenceSimilarity />;
