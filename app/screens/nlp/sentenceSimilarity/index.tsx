import React, { useEffect, useState } from 'react';
import { YStack, Text, Input, Button, H2, H4, Paragraph } from 'ui';
import { Loading } from 'app/components/Loading';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';

function SentenceSimilarity() {
  const [similarityScores, setSimilarityScores] = useState<number[]>([]);
  const [sentence, setSentence] = useState<string>('');
  const [sentences, setSentences] = useState<string[]>([]);
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);
  const [source_sentence, setSourceSentence] = useState('');
  const [scores, setScores] = useState<number[]>([]);
  const huggingFace = useHuggingFace();

  useEffect(() => {
    setSentences(sentence.split(','));
  }, [sentence.length]);

  const calculateSimilarity = async () => {
    setLoading(true);
    const body = {
      inputs: {
        sentences,
        source_sentence,
      },
    };
    const response = await huggingFace?.getSentenceSimilarity(body);
    setScores(response);
    setError(null);
    setLoading(false);
  };

  return (
    <YStack space>
      <YStack jc="center" ai="center" space="$8">
        <H2>Sentence Similarity</H2>
        {loading ? (
          <Loading />
        ) : (
          <>
            <YStack ai="center" space>
              <H4>Source Sentence</H4>
              <Input
                w={1400}
                h="$8"
                placeholder="Source Sentence"
                value={source_sentence}
                onChangeText={setSourceSentence}
              />
              <H4>Sentences to Compare to</H4>
              <Input w={1400} h="$8" placeholder="Source Sentence" value={sentence} onChangeText={setSentence} />
              <Button onClick={() => calculateSimilarity()}>Compare</Button>
            </YStack>
            {loading && <Loading />}
            {error && <Text>{error}</Text>}
            {similarityScores.length > 0 && error === null && (
              <Text>
                Similarity Scores:{' '}
                {similarityScores.map((score, index) => (
                  <YStack key={index}>{`Sentence ${index + 1}: ${score}`}</YStack>
                ))}
              </Text>
            )}
            {scores.map((score, index) => {
              return (
                <Paragraph>
                  {sentences[index]}
                  <Text key={index}>{score}</Text>
                </Paragraph>
              );
            })}
          </>
        )}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(SentenceSimilarity);
