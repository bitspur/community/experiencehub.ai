import React from 'react';
import ZeroShotClassification from './index';

export default {
  title: 'screens/nlp/zeroShotClassification',
  component: ZeroShotClassification,
  parameters: { status: { type: 'beta' } },
};

export const Default = () => <ZeroShotClassification />;
