import React from 'react';
import { YStack, XStack, Button, Paragraph, Text, H1, TextArea, Input, Progress } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { zeroShotClassifierResponse } from 'app/engines/types';
import { withDefaultLayout } from 'app/layouts/Default';

function ZeroShotClassification() {
  const huggingFace = useHuggingFace();
  const [inputs, setInputs] = React.useState('');
  const [candidate_labels, setCandidateLabels] = React.useState<string>('');
  const [result, setResult] = React.useState<any>('');

  async function handleClassifiedData() {
    const body = {
      inputs,
      parameters: {
        candidate_labels,
        multi_class: false,
      },
    } as zeroShotClassifierResponse;

    console.log('body', {
      inputs,
      parameters: {
        candidate_labels,
        multi_class: false,
      },
    });

    const response = await huggingFace?.getZeroShotClassificationResponse(body);
    setResult(response);
  }

  return (
    <YStack space>
      <YStack space="$6" ai="center" jc="center">
        <H1>Zero Shot Classification</H1>
        <XStack space>
          <Text fontSize="$3">facebook/bart-large-mnli</Text>
        </XStack>
        <Paragraph>
          Zero-shot classification is a method of solving a classification task without any labeled training data. The
          model is able to classify the text based on the description of the classes without seeing any example of the
          class.
        </Paragraph>
        <YStack space jc="flex-start">
          <TextArea
            size="$6"
            placeholder="Enter your text here"
            height={200}
            width={500}
            onChangeText={setInputs}
            value={inputs}
          />
          <Input
            placeholder="Enter your candidate_labels here"
            value={candidate_labels}
            onChangeText={setCandidateLabels}
          />
          <Button onPress={handleClassifiedData}>Classify</Button>
        </YStack>
        <YStack space>
          {result && (
            <YStack space="$6">
              {result.labels.map((label, index) => (
                <YStack space>
                  <Text fontSize={20}>
                    {label}: {result.scores[index].toFixed(2)} {'\n'}
                  </Text>
                  <Progress size="$4" value={result.scores[index] * 100}>
                    <Progress.Indicator animation="bouncy" />
                  </Progress>
                </YStack>
              ))}
            </YStack>
          )}
        </YStack>
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(ZeroShotClassification);
