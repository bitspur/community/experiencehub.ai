import React from 'react';
import FillMask from './index';

export default {
  title: 'screens/nlp/fillMask',
  component: FillMask,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <FillMask />;
