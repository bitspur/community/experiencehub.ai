import React from 'react';
import { YStack, XStack, Button, Input, H1, Text, Progress, Label, Paragraph, Separator } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { fillMaskResponse } from 'app/engines/types';
import { withDefaultLayout } from 'app/layouts/Default';

function FillMask() {
  const [inputText, setInputText] = React.useState<string>('');
  const [result, setResult] = React.useState<any>([]);
  const huggingFace = useHuggingFace();

  async function handleCompute() {
    const body = {
      prompt: inputText,
    } as fillMaskResponse;

    const response = await huggingFace?.getFillMaskResponse(body);
    setResult(response);
  }

  return (
    <YStack space>
      <YStack space="$6" ai="center" jc="center">
        <H1>Fill Mask</H1>
        <XStack space>
          <Text fontSize="$3">bert-base-uncased</Text>
        </XStack>
        <Text>Fill-Mask</Text>
        <Text>Mask token: [MASK]</Text>
        <Text>Example : Delhi is the [MASK] of India.</Text>
        <XStack space>
          <Input onChangeText={(text) => setInputText(text)} size="$4" placeholder="Enter your text" />
          <Button onPress={handleCompute}>Compute</Button>
        </XStack>
        {result && (
          <YStack padding="$10" borderWidth={1} backgroundColor="$background" space>
            {result.map((item, index) => (
              <YStack space="$2" key={index}>
                <Label fontSize={20}>{item.token_str}</Label>
                <Paragraph>{item.sequence}</Paragraph>
                <Progress size="$4" value={item.score * 100}>
                  <Progress.Indicator animation="bouncy" />
                </Progress>
                {item.score}
                <Separator />
              </YStack>
            ))}
          </YStack>
        )}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(FillMask);
