import React from 'react';
import TokenClassification from './index';

export default {
  title: 'screens/nlp/tokenClassification',
  component: TokenClassification,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <TokenClassification />;
