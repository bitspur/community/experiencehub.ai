import React, { useState, useEffect } from 'react';
import { YStack, Button, Text, H3, H1, TextArea } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { fillMaskResponse } from 'app/engines/types';
import { withDefaultLayout } from 'app/layouts/Default';

function TokenClassification() {
  const [inputText, setInputText] = useState('');
  const [tokenClassifications, setTokenClassifications] = useState<any[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const huggingFace = useHuggingFace();

  const handleInputChange = (event) => {
    setInputText(event.target.value);
  };

  const performTokenClassification = async () => {
    if (!huggingFace) {
      console.error('Hugging Face engine is not available.');
      return;
    }

    const tokens = inputText.split(' ');

    const body = {
      prompt: inputText,
    } as fillMaskResponse;

    const response = await huggingFace?.getTokenClassification(body);
    setTokenClassifications(response);
  };

  useEffect(() => {
    console.log(tokenClassifications);
  }, [tokenClassifications]);

  return (
    <YStack>
      <YStack space ai="center">
        <H1>Token Classification</H1>
        <H3>
          Token classification is a natural language understanding task in which a label is assigned to some tokens in a
          text.
        </H3>
        <TextArea
          h="$20"
          w={800}
          value={inputText}
          onChange={handleInputChange}
          placeholder="Enter text for token classification"
        />
        <Button onClick={performTokenClassification}>Classify Tokens</Button>
        {isLoading ? (
          <Text>Loading...</Text>
        ) : huggingFace ? (
          <>
            {tokenClassifications.map((classification, index) => (
              <Text key={index}>
                "{classification.word}" - Entity: {classification.entity_group || 'O (No Entity)'}, Score:{' '}
                {classification.score}
              </Text>
            ))}
          </>
        ) : (
          <Text>Loading Hugging Face Engine...</Text>
        )}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(TokenClassification);
