import React, { useState } from 'react';
import { YStack, Text } from 'ui';
import { Header } from 'app/components/Header';
import { MessageInput } from 'app/components/MessageInput';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';
import { ChatProtocol } from 'core/protocols/chat';
import { KeyTypes } from 'app/state/types';
import { useApiKeys } from 'app/state';

type Message = {
  role: string;
  content: string;
};

function Conversational() {
  const [messages, setMessages] = useState<Message[]>([]);
  const huggingFace = useHuggingFace();

  const apiKeysState = useApiKeys();
  const sendMessage = async (inputText) => {
    if (inputText.trim() === '') {
      return;
    }

    const protocol = await ChatProtocol.getProtocolEnvironmentByModel('baichuan-vicuna-7b.ggmlv3.q4_0');

    const apiKey = apiKeysState.keys.find((key) => key.name === KeyTypes.OPENAI)?.key;

    setMessages((prev) => [...prev, { role: 'user', content: inputText }]);

    const data = await protocol?.chatCompletion(
      {
        model: 'gpt-4',
        temperature: 0.9,
        messages: [...messages, { role: 'user', content: inputText }],
      },
      apiKey as string,
    );

    setMessages((prev) => [...prev, { role: 'assistant', content: data.choices[0].message.content }]);
  };

  return (
    <YStack fullscreen>
      <Header />
      <YStack
        alignSelf="center"
        flexDirection="column-reverse"
        top={80}
        position="absolute"
        width="60%"
        padding={30}
        paddingBottom={80}
        paddingTop={100}
        bottom={0}
      >
        {messages.map((message, index) => (
          <YStack ai={message.role === 'user' ? 'flex-end' : 'flex-start'} key={index}>
            <Text
              key={index}
              backgroundColor={message.role === 'user' ? 'lightblue' : 'lightgreen'}
              padding={10}
              margin={5}
              borderRadius={10}
            >
              {message.content}
            </Text>
          </YStack>
        ))}
      </YStack>
      <MessageInput onSend={sendMessage} width="60%" position="absolute" bottom={0} als="center" />
    </YStack>
  );
}

export default withDefaultLayout(Conversational);
