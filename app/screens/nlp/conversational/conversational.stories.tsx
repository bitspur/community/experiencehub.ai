import React from 'react';
import Conversational from './index';

export default {
  title: 'screens/nlp/conversational',
  component: Conversational,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => <Conversational />;
