import React from 'react';
import AccountScreen from './index';

export default {
  title: 'screens/settings/accountScreen',
  component: AccountScreen,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <AccountScreen />;
