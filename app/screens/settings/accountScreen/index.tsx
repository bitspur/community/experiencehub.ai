import { withDefaultLayout } from 'app/layouts/Default';
import React from 'react';
import { YStack, H1, H2, Input, Paragraph } from 'ui';

function AccountScreen() {
  return (
    <YStack ai="center" space mt="$6">
      <H1>Organization Settings</H1>
      <YStack ai="center" jc="center" space mt="$12">
        <H2>Organization name</H2>
        <Paragraph>Human friendly label for your Organization shown in user interface</Paragraph>
        <Input minWidth={200} maxWidth={500} />
        <H2>Organization Id</H2>
        <Paragraph>Identifier for this Organization sometimes used in API request</Paragraph>
        <Input minWidth={200} maxWidth={500} />
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(AccountScreen);
