import React from 'react';
import SettingsScreen from './index';

export default {
  title: 'screens/Settings',
  component: SettingsScreen,
  parameters: {
    status: {
      type: 'beta',
    },
  },
};

export const main = () => <SettingsScreen />;
