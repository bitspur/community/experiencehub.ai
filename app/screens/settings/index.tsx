import React from 'react';
import { YStack, Card, useWindowDimensions, ScrollView, XStack, H3 } from 'ui';
import { Link } from 'solito/link';
import { ArrowRight } from '@tamagui/lucide-icons';
import { withDefaultLayout } from 'app/layouts/Default';

const SettingsScreen = () => {
  const { width } = useWindowDimensions();
  return (
    <YStack fullscreen pt={80} space backgroundColor="$backgroundHover">
      <ScrollView>
        <ScrollView>
          <XStack jc="center" p="$8" flexWrap="wrap">
            <Link href="/settings/account">
              <Card
                borderWidth="$1"
                height={240}
                width={300}
                p="$4"
                m="$5"
                space
                animation="quick"
                hoverStyle={{ scale: 1.2 }}
                ai="center"
                jc="center"
              >
                <H3>Account</H3>
                <ArrowRight />
              </Card>
            </Link>
            <Link href="/settings/api-keys">
              <Card
                borderWidth="$1"
                height={240}
                width={300}
                p="$4"
                m="$5"
                space
                animation="quick"
                hoverStyle={{ scale: 1.2 }}
                ai="center"
                jc="center"
              >
                <H3>API-KEYS</H3>
                <ArrowRight />
              </Card>
            </Link>
            <Link href="/help">
              <Card
                borderWidth="$1"
                height={240}
                width={300}
                p="$4"
                m="$5"
                space
                animation="quick"
                hoverStyle={{ scale: 1.2 }}
                ai="center"
                jc="center"
              >
                <H3>HELP</H3>
                <ArrowRight />
              </Card>
            </Link>
          </XStack>
        </ScrollView>
      </ScrollView>
    </YStack>
  );
};

export default withDefaultLayout(SettingsScreen);
