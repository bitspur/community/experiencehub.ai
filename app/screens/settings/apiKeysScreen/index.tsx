import React from 'react';
import {
  YStack,
  XStack,
  Input,
  TableCol,
  TableCell,
  Button,
  SimpleDialog,
  Dialog,
  Paragraph,
  SelectSimple,
  Select,
  H1,
} from 'ui';
import { Plus, Trash2 } from '@tamagui/lucide-icons';
import { useApiKeys } from 'app/state/apiKeys';
import { ApiKeysProps, KeyTypes } from 'app/state/types';
import { withDefaultLayout } from 'app/layouts/Default';

function ApiKeysScreen() {
  const [text, setText] = React.useState<string>('');
  const apiKeysState = useApiKeys();
  const [selectedModels, setSelectedModels] = React.useState<string[]>([]);
  const availableModels = Object.values(KeyTypes).filter((model) => !selectedModels.includes(model));
  const [keyName, setKeyName] = React.useState<string>('');

  React.useEffect(() => {
    const modelsWithKey = apiKeysState.keys.map((item) => item.name);
    setSelectedModels(modelsWithKey);
  }, [apiKeysState.keys]);

  function handleAddKey() {
    if (!text || !keyName) return;
    if (!selectedModels.includes(keyName)) {
      apiKeysState.setKeys([...apiKeysState.keys, { name: keyName, key: text }] as ApiKeysProps[]);
      setSelectedModels([...selectedModels, keyName]);
      setText('');
      setKeyName('');
    }
  }

  function handleDeleteKey(index: number) {
    const deletedModelName = apiKeysState.keys[index]?.name;
    apiKeysState.setKeys(apiKeysState.keys.filter((item, i) => i !== index));
    setSelectedModels(selectedModels.filter((model) => model !== deletedModelName));
  }

  function renderApiKeysTable() {
    return (
      <YStack>
        <XStack>
          <TableCol bw={0} minWidth={265}>
            <TableCell>Name</TableCell>
          </TableCol>
          <TableCol bw={0} minWidth={265}>
            <TableCell>API Key</TableCell>
          </TableCol>
          <TableCol bw={0} minWidth={265}>
            <TableCell>Created</TableCell>
          </TableCol>
        </XStack>
        {apiKeysState?.keys?.map((item, index) => {
          const key = item.key;
          const firstThreeChar = key.substring(0, 3);
          const lastThreeChar = key.substring(key.length - 3);
          const maskedKey = firstThreeChar + '****' + lastThreeChar;

          return (
            <XStack key={index}>
              <TableCol bw={0} minWidth={250}>
                <TableCell>{item.name}</TableCell>
              </TableCol>
              <TableCol bw={0} minWidth={250}>
                <TableCell>{maskedKey}</TableCell>
              </TableCol>
              <TableCol bw={0}>
                <TableCell>
                  <YStack cursor="pointer" onPress={() => handleDeleteKey(index)}>
                    <Trash2 size={20} />
                  </YStack>
                </TableCell>
              </TableCol>
            </XStack>
          );
        })}
      </YStack>
    );
  }

  function renderDialog() {
    return (
      <SimpleDialog
        description="Add your new API key"
        trigger={
          <Button onPress={() => setKeyName('')}>
            <Plus /> Add API Keys
          </Button>
        }
      >
        <YStack space>
          <SelectSimple
            jc="center"
            placeholder="select one of these"
            value={keyName}
            onValueChange={(value) => setKeyName(value)}
          >
            {availableModels.map((name, i) => (
              <Select.Item key={i} index={i} value={name}>
                <Select.ItemText>{name}</Select.ItemText>
              </Select.Item>
            ))}
          </SelectSimple>
          <Input placeholder="Api key" value={text} onChangeText={setText} />
          <XStack space ai="center" jc="center">
            <Dialog.Close asChild>
              <Button onPress={handleAddKey}>Add</Button>
            </Dialog.Close>
          </XStack>
        </YStack>
      </SimpleDialog>
    );
  }

  return (
    <YStack space ai="center">
      <YStack ai="center" p="$6" space>
        <H1 mb="$8">API Keys</H1>
        <Paragraph>
          Your secret keys are listed below. please note that we do not display your secret API keys again after you
          generate them
        </Paragraph>
        <Paragraph>
          Do not share your API keys others or expose it in the browser or other client-side code in order to protect
          the security of your account OpenAI may also automatically rotate any API key that we've found has leaked
          publicly
        </Paragraph>
      </YStack>
      {apiKeysState.keys.length > 0 && renderApiKeysTable()}
      {renderDialog()}
    </YStack>
  );
}

export default withDefaultLayout(ApiKeysScreen);
