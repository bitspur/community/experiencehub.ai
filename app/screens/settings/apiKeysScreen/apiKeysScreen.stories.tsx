import React from 'react';
import ApiKeysScreen from './index';

export default {
  title: 'screens/settings/apiKeysScreen',
  component: ApiKeysScreen,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <ApiKeysScreen />;
