import React from 'react';
import AudioToAudio from './index';

export default {
  title: 'Screens/audio/audioToAudio',
  component: AudioToAudio,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <AudioToAudio />;
