import React, { useState, useRef } from 'react';
import { YStack, Button, H1, H4, XStack, Separator, Paragraph } from 'ui';
import { Mic, Play, Volume2 } from '@tamagui/lucide-icons';
import { withDefaultLayout } from 'app/layouts/Default';
import { Platform } from 'react-native';
import { ReactFileUploader } from 'app/components/ReactFileUploader';

function AudioToAudio() {
  const [isRecording, setIsRecording] = useState(false);
  const [result, setResult] = useState<{ label: string; score: number }[]>([]);
  const [mediaRecorder, setMediaRecorder] = useState<MediaRecorder | null>(null);
  const mediaStreamRef = useRef<MediaStream | null>(null);
  const [isBlob, setIsBlob] = useState<Blob | null>(null);
  const [audioUrl, setAudioUrl] = useState<string | null>(null);
  const [audioPlaying, setAudioPlaying] = useState(false);
  const audioRef = useRef(new Audio());
  const [uploadedFile, setUploadedFile] = useState<Blob | null>(null);

  let finalAudio = isBlob || uploadedFile;

  const startRecording = async () => {
    console.log('Recording started');
    const audioElement = new Audio('app/assets/record-notification.mp3');
    audioElement.play();
    const { start, mediaRecorderInstance, stream } = (await recordAudio()) as {
      start: () => void;
      mediaRecorderInstance: MediaRecorder;
      stream: MediaStream;
    };
    mediaStreamRef.current = stream;
    setMediaRecorder(mediaRecorderInstance);
    start();
    setIsRecording(true);
  };

  const recordAudio = () =>
    new Promise(async (resolve) => {
      const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
      const mediaRecorder = new MediaRecorder(stream);
      const audioChunks: Blob[] = [];

      mediaRecorder.addEventListener('dataavailable', (event) => {
        audioChunks.push(event.data);
      });

      mediaRecorder.addEventListener('stop', () => {
        const isBlob = new Blob(audioChunks, { type: 'audio/wav' });
        const audioUrl = URL.createObjectURL(isBlob);
        console.log('Recorded Audio Chunks:', audioChunks);
        setIsBlob(isBlob);
        setAudioUrl(audioUrl);
        resolve({ start, mediaRecorderInstance: mediaRecorder, stream });
      });

      const start = () => mediaRecorder.start();
      resolve({ start, mediaRecorderInstance: mediaRecorder, stream });
    });

  const stopRecording = () => {
    console.log('Recording stopped');
    if (mediaRecorder) {
      mediaRecorder.stop();
      setIsRecording(false);
    }
    if (mediaStreamRef.current) {
      mediaStreamRef.current.getTracks().forEach((track) => track.stop());
      mediaStreamRef.current = null;
    }
  };

  const handleAudioPlayback = () => {
    if (audioUrl) {
      const audio = audioRef.current;

      if (!audioPlaying) {
        audio.src = audioUrl;
        audio.play();
        setAudioPlaying(true);
      } else {
        audio.pause();
        audio.currentTime = 0;
        setAudioPlaying(false);
      }
    }
  };

  function handleUploadAudio(file) {
    console.log('file');
    setAudioUrl(URL.createObjectURL(file));
    setUploadedFile(file);
  }

  function handleStartRecording(blob: Blob) {
    setIsBlob(blob);
  }

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1>AudioToAudio</H1>
      {Platform.OS === 'web' ? <ReactFileUploader onFileUpload={handleUploadAudio} /> : null}
      <Paragraph fos={'$5'}>
        In this AudioToAudio screen when ever we give the audio input converted into speech
        <Separator />
        recognition The generated audio output is typically based on the analysis and interpretation
        <Separator />
        Of the provided Audio input, and it can serve a wide range of purposes, from answering voice
        <Separator />
        commands to producing synthesized speech or music.
      </Paragraph>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H4>This model can be loaded on the Inference API on-demand.</H4>
        <Button onPress={isRecording ? stopRecording : startRecording} icon={Mic}>
          {isRecording ? 'Stop Recording' : 'Start Recording'}
        </Button>
        <XStack space ai="center">
          <Button onPress={handleAudioPlayback} br={6}>
            Compute
          </Button>
          {audioPlaying ? (
            <Button icon={<Volume2 />} space>
              Press Compute After Recording Audio
            </Button>
          ) : (
            <Play onPress={handleAudioPlayback} />
          )}
        </XStack>
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(AudioToAudio);
