import React, { useState } from 'react';
import { H1, XStack, YStack, Separator, Button, H5, H4, Card } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import ReactPlayer from 'react-player';
import { ReactFileUploader } from 'app/components/ReactFileUploader';
import { withDefaultLayout } from 'app/layouts/Default';
import { AudioRecorder } from 'app/components/AudioRecorder';
import { Platform } from 'react-native';

function AutomaticSpeechRecognition() {
  const [audioBlob, setAudioBlob] = useState<Blob | null>(null);
  const [uploadedAudioBlob, setUploadedAudioBlob] = useState<Blob | null>(null);
  const [result, setResult] = useState();
  const huggingFace = useHuggingFace();

  let finalAudio = audioBlob || uploadedAudioBlob;

  function handleStartRecording(blob: Blob) {
    setAudioBlob(blob);
  }

  function handleUploadAudio(file) {
    setUploadedAudioBlob(file);
  }

  const onFinalCompute = async () => {
    if (finalAudio === null) return;
    const response = await huggingFace?.getAutomaticSpeechRecognition(finalAudio);
    setResult(response);
  };

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1 paddingTop="$4">Automatic Speech Recognition</H1>
      <H5>
        Automatic Speech Recognition or ASR, as it's known in short, is the technology
        <Separator borderWidth={'$0'} />
        that allows human beings to use their voices to speak with a computer interface in
        <Separator borderWidth={'$0'} />a way that, in its most sophisticated variations, resembles normal human
        conversation.
      </H5>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H4>This model can be loaded on the Inference API on-demand.</H4>
        {Platform.OS === 'web' ? <ReactFileUploader accept="audio/*" onFileUpload={handleUploadAudio} /> : null}
        <H5>OR</H5>
        <AudioRecorder onRecordingStart={handleStartRecording} />
        <XStack ai="center" jc="center" space>
          {finalAudio && (
            <Card elevate bordered borderRadius="$10" backgroundColor={'$backgroundStrong'}>
              <YStack space padding="$3">
                <H5>Recorded Audio</H5>
                <ReactPlayer url={URL.createObjectURL(finalAudio)} controls width="400px" height="50px" />
              </YStack>
            </Card>
          )}
        </XStack>
      </YStack>
      <Button onPress={onFinalCompute}>
        <H5>Press Compute After Uploading or Recording Audio</H5>
      </Button>
      <YStack space>
        {result && (
          <Card borderRadius="$10" backgroundColor="$background" padding="$4" ai="center" jc="center">
            <H5>{result}</H5>
          </Card>
        )}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(AutomaticSpeechRecognition);
