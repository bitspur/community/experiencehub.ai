import React from 'react';
import AutomaticSpeechRecognition from './index';

export default {
  title: 'screens/audio/AutomaticSpeechRecognition',
  component: AutomaticSpeechRecognition,
  parameters: { status: { type: 'beta' } },
};

export const main = () => < AutomaticSpeechRecognition/>;
