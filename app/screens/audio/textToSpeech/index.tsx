import React, { useState } from 'react';
import { Button, Card, H1, H4, Separator, XStack, YStack, Input, H5 } from 'ui';
import { audioRequest } from 'app/engines/types';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';
import { Mic } from '@tamagui/lucide-icons';
import ReactPlayer from 'react-player';

function TextToSpeech() {
  const [inputText, setInputText] = useState<string>('');
  const [audioBlob, setAudioBlob] = useState<Blob | null>(null);
  const huggingFace = useHuggingFace();

  const handleComputePrompt = async () => {
    const body = {
      prompt: inputText,
    } as audioRequest;

    const finalAudio = await huggingFace?.getAudioResponse(body);
    setAudioBlob(finalAudio);
  };

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1 paddingTop="$3">Text-to-Speech</H1>
      <H5>
        Text-to-Speech (TTS) is the task of generating natural sounding <Separator borderWidth="$0" />
        speech given text input. TTS models can be extended to have a single <Separator borderWidth="$0" />
        model that generates speech for multiple speakers and multiple languages.
      </H5>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H4>This model can be loaded on the Inference API on-demand.</H4>
        <XStack ai="center" jc="center" space>
          <Mic />
          <Input
            onChangeText={(text) => setInputText(text)}
            size="$6"
            borderWidth={2}
            placeholder="Please enter a text here."
          />
        </XStack>
      </YStack>
      <Button onPress={handleComputePrompt}>
        <H5>Press Compute After Entering Text</H5>
      </Button>
      <YStack mt="$6">
        <XStack ai="center" jc="center" space>
          {audioBlob && (
            <Card elevate bordered borderRadius="$10" backgroundColor={'$backgroundStrong'}>
              <YStack space padding="$3">
                <H5>Recorded Audio</H5>
                <ReactPlayer url={URL.createObjectURL(audioBlob)} controls width="400px" height="50px" />
              </YStack>
            </Card>
          )}
        </XStack>
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(TextToSpeech);
