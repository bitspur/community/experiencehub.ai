import React from 'react';
import TextToSpeech from './index';

export default {
  title: 'screens/audio/textToSpeech',
  component: TextToSpeech,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <TextToSpeech />;
