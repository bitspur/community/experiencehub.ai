import React, { useState } from 'react';
import { H1, XStack, YStack, Separator, Button, H5, H4, Progress, Card } from 'ui';
import { useHuggingFace } from 'app/engines/huggingFace';
import { withDefaultLayout } from 'app/layouts/Default';
import { AudioRecorder } from 'app/components/AudioRecorder';
import { ReactFileUploader } from 'app/components/ReactFileUploader';
import { Platform } from 'react-native';
import ReactPlayer from 'react-player';

function AudioClassification() {
  const [result, setResult] = useState<{ label: string; score: number }[]>([]);
  const [audioBlob, setAudioBlob] = useState<Blob | undefined>(undefined);
  const [uploadedAudioBlob, setUploadedAudioBlob] = useState<Blob | null>(null);
  const huggingFace = useHuggingFace();

  let finalAudio = audioBlob || uploadedAudioBlob;

  function handleStartRecording(blob: Blob) {
    setAudioBlob(blob);
  }

  function handleUploadAudio(file) {
    setUploadedAudioBlob(file);
  }

  const handleComputePrompt = async () => {
    if (finalAudio === null) return;
    const response = await huggingFace?.getAudioClassificationResponse(finalAudio);
    setResult(response);
  };

  return (
    <YStack ai="center" jc="center" space backgroundColor={'$background'} paddingTop="$4">
      <H1>Audio Classification</H1>
      <H5>
        Audio classification is a specialized form of machine learning focused on <Separator borderWidth="$0" />
        categorizing audio clips into predefined classes or labels. It can be used for <Separator borderWidth="$0" />
        recognizing which command a user is giving or the emotion of a statement, as well <Separator borderWidth="$0" />
        as identifying a speaker.
      </H5>
      <YStack
        space
        ai="center"
        jc="center"
        marginLeft="$8"
        marginTop="$3"
        marginBottom="$3"
        borderRadius={'$10'}
        backgroundColor="$backgroundHover"
        padding="$6"
        width={800}
      >
        <H4>This model can be loaded on the Inference API on-demand.</H4>
        {Platform.OS === 'web' ? <ReactFileUploader accept="audio/*" onFileUpload={handleUploadAudio} /> : null}
        <H5>OR</H5>
        <AudioRecorder onRecordingStart={handleStartRecording} />
        <XStack ai="center" jc="center" space>
          {finalAudio && (
            <Card elevate bordered borderRadius="$10" backgroundColor={'$backgroundStrong'}>
              <YStack space padding="$3">
                <H5>Recorded Audio</H5>
                <ReactPlayer url={URL.createObjectURL(finalAudio)} controls width="400px" height="50px" />
              </YStack>
            </Card>
          )}
        </XStack>
      </YStack>
      <Button onPress={handleComputePrompt} marginLeft="$5">
        <H5>Press Compute After Uploading or Recording Audio</H5>
      </Button>
      <YStack space>
        {result?.map((item, index) => (
          <YStack key={index}>
            <H4>{item.label}</H4>
            <H5>{item.score.toFixed(2)}</H5>
            <Progress size="$4" value={item.score * 100}>
              <Progress.Indicator animation="bouncy" />
            </Progress>
            {item.score}
          </YStack>
        ))}
      </YStack>
    </YStack>
  );
}

export default withDefaultLayout(AudioClassification);
