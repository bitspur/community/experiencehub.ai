import React from 'react';
import AudioClassification from './index';

export default {
  title: 'screens/audio/AudioClassification',
  component: AudioClassification,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <AudioClassification />;
