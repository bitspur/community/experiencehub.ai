import { Select, XStack, YStack, useAssets, isWeb, useWindowDimensions, Button, Paragraph } from 'ui';
import React, { useEffect, useState } from 'react';
import { SelectSimple } from '@multiplatform.one/components';
import { Platform, ScrollView } from 'react-native';
import { MessageInput } from 'app/components/MessageInput';
import { ArrowRightSquare, SlidersHorizontal, X } from '@tamagui/lucide-icons';
import { Link } from 'solito/link';
import { SolitoImage } from 'solito/image';
import { KeyTypes, useApiKeys } from 'app/state';
import {
  ImageGenerationPayload,
  ImageGenerationProtocol,
  ImageSize,
  NumberOfImages,
} from 'core/protocols/image-generation';
import { ImageBlock } from 'app/components/ImageBlock';

export interface IImageGenerationMessage {
  id: string;
  role: 'user' | 'assistant';
  prompt?: string;
  images?: string[];
}

function ImageGeneration() {
  const [chats, setChats] = useState<IImageGenerationMessage[]>([]);
  const [numberOfImages, setNumberOfImages] = useState<string | undefined>(undefined);
  const [imageSize, setImageSize] = useState<string | undefined>(undefined);
  const [openSideBar, setOpenSideBar] = useState<boolean>(false);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState<boolean>(false);
  const scrollViewRef = React.useRef<ScrollView>(null);
  const [logoPng] = useAssets(require('app/assets/logo.png'));
  const { width, height } = useWindowDimensions();
  const [waitingForResponse, setWaitingForResponse] = useState<boolean>(false);
  const apiKeysState = useApiKeys();
  const openaiApiKey = apiKeysState.keys.find((key) => key.name === KeyTypes.OPENAI)?.key;

  useEffect(() => {
    if (scrollViewRef.current) {
      scrollViewRef.current.scrollToEnd({ animated: true });
    }
  }, [chats.length]);

  useEffect(() => {
    setNumberOfImages('1');
    setImageSize('MEDIUM');
  }, []);

  async function handleSendRequest(prompt: string) {
    if (!prompt) return;
    setChats((prev) => [...prev, { id: new Date().getTime().toString(), prompt, role: 'user' }]);
    if (!openaiApiKey) return;
    if (!numberOfImages) return;
    if (!imageSize) return;
    setWaitingForResponse(true);
    const ImageGenerationPayload: ImageGenerationPayload = {
      prompt,
      n: Number(numberOfImages),
      size: ImageSize[imageSize],
    };
    const protocol = await ImageGenerationProtocol.getProtocolEnvironmentByModel();
    const images = await protocol?.imageGenerator(ImageGenerationPayload, openaiApiKey);
    const assistantMessage: IImageGenerationMessage = {
      id: new Date().getTime().toString(),
      role: 'assistant',
      images: images,
    };
    setWaitingForResponse(false);
    setChats((prev) => (assistantMessage ? [...prev, assistantMessage] : prev));
  }

  return (
    <XStack backgroundColor="$gray7" fullscreen>
      <YStack paddingBottom={100} width={width} height={height}>
        <ScrollView ref={scrollViewRef} showsHorizontalScrollIndicator={false}>
          {chats.map((chat, index) => (
            <XStack jc="center" ai="flex-start" key={index}>
              <ImageBlock chat={chat} />
            </XStack>
          ))}
        </ScrollView>
        <YStack
          display={waitingForResponse ? 'flex' : 'none'}
          p={10}
          br={10}
          backgroundColor={'$orange10Dark'}
          als="center"
          elevation={12}
        >
          <Paragraph color="$background">waiting for response</Paragraph>
        </YStack>

        <MessageInput
          backgroundColor="$gray7"
          y={isKeyboardOpen && !isWeb ? -310 : 0}
          paddingBottom={Platform.OS === 'web' ? '$3' : '$10'}
          onSend={handleSendRequest}
          waitingForResponse={waitingForResponse}
          position="absolute"
          bottom={0}
          width={width}
        />

        {openSideBar && (
          <YStack
            width={width}
            height={height}
            position="absolute"
            backgroundColor="rgba(0,0,0,0.2)"
            onPress={() => setOpenSideBar(false)}
          />
        )}
      </YStack>
      {openSideBar && (
        <YStack
          width={width * 0.3}
          height={height}
          animation="lazy"
          backgroundColor="$blue12Dark"
          enterStyle={{ x: 300 }}
          $xs={{ zIndex: 15, position: 'absolute', top: 0, right: 0, width: width * 0.6 }}
          {...{ position: 'absolute', top: 0, right: 0, zIndex: 15 }}
        >
          {renderSideBar()}
        </YStack>
      )}
      {!openSideBar && (
        <YStack pos="absolute" top={20} right={15} cursor="pointer" onPress={() => setOpenSideBar(true)}>
          <SlidersHorizontal />
        </YStack>
      )}
      <YStack position="absolute">
        <Link href="/">
          <SolitoImage src={logoPng} alt="Logo" height={60} width={60} style={{ borderRadius: 9999 }} />
        </Link>
      </YStack>
    </XStack>
  );

  function renderSideBar() {
    return (
      <ScrollView>
        <YStack space="$8" padding="$4">
          {openSideBar && (
            <YStack cur="pointer" onPress={() => setOpenSideBar(false)}>
              <ArrowRightSquare size="$2" />
            </YStack>
          )}
          {openSideBar && (
            <YStack space>
              <SelectSimple
                jc="center"
                placeholder="number of images"
                onValueChange={setNumberOfImages}
                value={numberOfImages}
              >
                {Object.keys(NumberOfImages)
                  .filter((value) => !isNaN(Number(value)))
                  .map((value, i) => (
                    <Select.Item index={i} value={value as string} key={i}>
                      <Select.ItemText>{value}</Select.ItemText>
                    </Select.Item>
                  ))}
              </SelectSimple>
              <SelectSimple jc="center" placeholder="image size" value={imageSize} onValueChange={setImageSize}>
                {Object.keys(ImageSize).map((value, i) => (
                  <Select.Item index={i + 1} value={value as string} key={i + 1}>
                    <Select.ItemText>{value}</Select.ItemText>
                  </Select.Item>
                ))}
              </SelectSimple>
            </YStack>
          )}
        </YStack>
      </ScrollView>
    );
  }
}

export default ImageGeneration;
