import React from 'react';
import ImageGeneration from '.';

export default{
    title:'Screens/experiences/ImageGeneration',
    component: ImageGeneration,
    parameters: {status: {type: 'beta'} }
}

export const main = ()=> <ImageGeneration />
