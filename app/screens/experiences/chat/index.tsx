import { useMedia, Select, Slider, Text, XStack, YStack, useAssets, isWeb, useWindowDimensions, Paragraph } from 'ui';
import React, { useEffect, useState } from 'react';
import { SelectSimple } from '@multiplatform.one/components';
import { Platform, ScrollView } from 'react-native';
import { MessageInput } from 'app/components/MessageInput';
import { TextBlock } from 'app/components/TextBlock';
import { ArrowRightSquare, SlidersHorizontal } from '@tamagui/lucide-icons';
import { ChatPayload } from 'core/protocols/chat';
import { Link } from 'solito/link';
import { SolitoImage } from 'solito/image';
import { IModel, models } from 'core/models';
import { KeyTypes, useApiKeys } from 'app/state';
import { ChatProtocol } from 'core/protocols/chat';

export type IMessage = {
  id: string;
  role: 'user' | 'assistant';
  prompt: string;
};

function Chat() {
  const [model, setModel] = useState<IModel | undefined>();
  const [chats, setChats] = useState<IMessage[]>([]);
  const [temperature, setTemperature] = useState<number[]>([0.2]);
  const [openSideBar, setOpenSideBar] = useState<boolean>(false);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState<boolean>(false);
  const scrollViewRef = React.useRef<ScrollView>(null);
  const media = useMedia();
  const [logoPng] = useAssets(require('app/assets/logo.png'));
  const { width, height } = useWindowDimensions();
  const [waitingForResponse, setWaitingForResponse] = useState<boolean>(false);
  const apiKeysState = useApiKeys();

  useEffect(() => {
    if (scrollViewRef.current) {
      scrollViewRef.current.scrollToEnd({ animated: true });
    }
  }, [chats.length]);

  useEffect(() => {
    setModel(models.get('gpt-3.5-turbo'));
  }, []);

  async function handleModelChange(model) {
    const modelObject = models.get(model);
    setModel(modelObject);
  }

  async function handleSendRequest(prompt: string) {
    if (!prompt) return;
    setChats((prev) => [...prev, { id: new Date().getTime().toString(), prompt, role: 'user' }]);

    if (!model?.name) return;
    const chatHistory = chats.map((chat) => {
      return { role: chat.role, content: chat.prompt };
    });
    const ChatPayload: ChatPayload = {
      model: model.name,
      temperature: temperature[0]!,
      messages: [...chatHistory, { role: 'user', content: prompt }],
    };
    const openaiApiKey = apiKeysState.keys.find((key) => key.name === KeyTypes.OPENAI)?.key;
    setWaitingForResponse(true);
    const protocol = await ChatProtocol.getProtocolEnvironmentByModel(model.name);

    const chat = await protocol?.chatCompletion(ChatPayload, openaiApiKey as string);
    const assistantMessage: IMessage = {
      id: new Date().getTime().toString(),
      role: 'assistant',
      prompt: chat?.choices[0].message.content,
    };
    setWaitingForResponse(false);
    return setChats((prev) => (assistantMessage ? [...prev, assistantMessage] : prev));
  }

  return (
    <XStack backgroundColor="$gray7" fullscreen>
      <YStack paddingBottom={100} width={width} height={height}>
        <ScrollView ref={scrollViewRef} showsHorizontalScrollIndicator={false}>
          {chats.map((chat, index) => (
            <XStack jc="center" ai="flex-start" key={index}>
              <TextBlock chat={chat} />
            </XStack>
          ))}
        </ScrollView>
        <YStack
          display={waitingForResponse ? 'flex' : 'none'}
          p={10}
          br={10}
          backgroundColor={'$orange10Dark'}
          als="center"
          elevation={12}
        >
          <Paragraph color="$background">waiting for response</Paragraph>
        </YStack>

        <MessageInput
          backgroundColor="$gray7"
          y={isKeyboardOpen && !isWeb ? -310 : 0}
          paddingBottom={Platform.OS === 'web' ? '$3' : '$10'}
          onSend={handleSendRequest}
          waitingForResponse={waitingForResponse}
          position="absolute"
          bottom={0}
          width={width}
        />

        {openSideBar && (
          <YStack
            width={width}
            height={height}
            position="absolute"
            backgroundColor="rgba(0,0,0,0.2)"
            onPress={() => setOpenSideBar(false)}
          />
        )}
      </YStack>
      {openSideBar && (
        <YStack
          width={width * 0.3}
          height={height}
          animation="lazy"
          backgroundColor="$blue12Dark"
          enterStyle={{ x: 300 }}
          $xs={{ zIndex: 15, position: 'absolute', top: 0, right: 0, width: width * 0.6 }}
          {...{ position: 'absolute', top: 0, right: 0, zIndex: 15 }}
        >
          {renderSideBar()}
        </YStack>
      )}
      {!openSideBar && (
        <YStack pos="absolute" top={20} right={15} cursor="pointer" onPress={() => setOpenSideBar(true)}>
          <SlidersHorizontal />
        </YStack>
      )}
      <YStack position="absolute">
        <Link href="/">
          <SolitoImage src={logoPng} alt="Logo" height={60} width={60} style={{ borderRadius: 9999 }} />
        </Link>
      </YStack>
    </XStack>
  );

  function renderSideBar() {
    return (
      <ScrollView>
        <YStack space="$8" padding="$4">
          {openSideBar && (
            <YStack cur="pointer" onPress={() => setOpenSideBar(false)}>
              <ArrowRightSquare size="$2" />
            </YStack>
          )}
          {openSideBar && (
            <YStack>
              <SelectSimple
                jc="center"
                placeholder="select one of these"
                onValueChange={handleModelChange}
                value={model?.name}
              >
                {Array.from(models.keys()).map((modelName, index) => (
                  <Select.Item index={index + 1} value={modelName} key={index}>
                    <Select.ItemText>{modelName}</Select.ItemText>
                  </Select.Item>
                ))}
              </SelectSimple>
              <YStack padding="$2" space>
                <Text>Temperature [{temperature[0]}]</Text>
                <Slider
                  name="Temperature"
                  min={0}
                  max={1}
                  step={0.1}
                  onValueChange={setTemperature}
                  value={temperature || [0]}
                >
                  <Slider.Track size="$6">
                    <Slider.TrackActive br={0} bg="$blue10Dark" />
                  </Slider.Track>
                  <Slider.Thumb bg="$gray2Light" size="$2" theme="SliderThumb" circular index={0} />
                </Slider>
              </YStack>
            </YStack>
          )}
        </YStack>
      </ScrollView>
    );
  }
}

export default Chat;
