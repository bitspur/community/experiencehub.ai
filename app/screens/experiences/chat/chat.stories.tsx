import React from 'react';
import Chat from '.';

export default{
    title:'Screens/experiences/Chat',
    component: Chat,
    parameters: {status: {type: 'beta'} }
}

export const main = ()=> <Chat />
