import React, { useRef, useState, useEffect } from 'react';
import ReactPlayer from 'react-player';
import { AudioRecorder } from 'app/components/AudioRecorder';
import { Circle, Paragraph, Select, SelectSimple, Text, XStack, YStack, useAssets, useWindowDimensions } from 'ui';
import { ScrollView } from 'react-native';
import { Link } from 'solito/link';
import { SolitoImage } from 'solito/image';
import { AudioPayload } from 'core/protocols/audio-to-text';
import { KeyTypes, useApiKeys } from 'app/state';
import { AudioProtocol } from 'core/protocols/audio-to-text';
import { ArrowRightSquare, SlidersHorizontal } from '@tamagui/lucide-icons';

function AudioScreen() {
  const [model, setModel] = useState<string>();
  const [experiences, setExperiences] = useState<string[]>([]);
  const [experience, setExperience] = useState<string | undefined>();
  const [audios, setAudios] = useState<AudioPayload[]>([]);
  const scrollViewRef = useRef<ScrollView>(null);
  const { width, height } = useWindowDimensions();
  const [waitingForResponse, setWaitingForResponse] = useState<boolean>(false);
  const [logoPng] = useAssets(require('app/assets/logo.png'));
  const [openSideBar, setOpenSideBar] = useState<boolean>(false);
  const apiKeysState = useApiKeys();

  useEffect(() => {
    if (scrollViewRef.current) {
      scrollViewRef.current.scrollToEnd({ animated: true });
    }
  }, [audios.length]);

  async function handleModelChange(model) {
    setModel(model);
    setExperiences(['translations', 'transcriptions']);
  }

  async function handleSendAudioRequest(audioBlob: Blob) {
    if (!model) return;
    const userAudio: AudioPayload = {
      id: Date.now().toString(),
      role: 'user',
      prompt: "User's Audio :  ",
      audioBlob,
    };

    if (!audioBlob) return;

    setAudios((prev) => (userAudio ? [...prev, userAudio] : prev));

    const formData = new FormData();
    formData.append('file', audioBlob, 'audio.wav');
    formData.append('model', model);

    const openaiApiKey = apiKeysState.keys.find((key) => key.name === KeyTypes.OPENAI)?.key;
    setWaitingForResponse(true);

    const protocol = AudioProtocol.getProtocolEnvironmentByModel(model);
    console.log('protocol', protocol);

    const textGenerated = await protocol?.audioTranslations(formData, openaiApiKey as string, experience);

    const assistantMessage: AudioPayload = {
      id: new Date().getTime().toString(),
      role: 'assistant',
      prompt: ' Translated Into Text  : ',
      responseText: textGenerated.text,
      audioBlob: null,
    };

    setWaitingForResponse(false);
    return setAudios((prev) => (assistantMessage ? [...prev, assistantMessage] : prev));
  }

  return (
    <XStack space>
      <YStack paddingBottom={100} width={width} height={height} space>
        <ScrollView ref={scrollViewRef} showsHorizontalScrollIndicator={false}>
          {audios.map((audio) => (
            <YStack space>
              {audio.role === 'user' ? (
                <YStack space alignSelf="stretch" p="$10" bg="$gray6Light" ai="flex-start">
                  <XStack space ai="center">
                    <Circle size="$3" bg="$blue10" als="flex-start" ml="$1">
                      <Text>U</Text>
                    </Circle>
                    <Text>{audio.prompt}</Text>
                  </XStack>
                  <ReactPlayer
                    url={audio.audioBlob ? URL.createObjectURL(audio.audioBlob) : undefined}
                    controls={true}
                    width="400px"
                    height="50px"
                  />
                </YStack>
              ) : (
                <YStack space alignSelf="stretch" p="$14" bg="$gray8Light" ai="flex-end">
                  <XStack space ai="center">
                    <Circle size="$3" bg="$green9Light" als="flex-start" marginRight="$1">
                      <Text>A</Text>
                    </Circle>
                    <Text>{audio.prompt}</Text>
                  </XStack>
                  <Text>{audio.responseText}</Text>
                </YStack>
              )}
            </YStack>
          ))}
        </ScrollView>
        <YStack
          display={waitingForResponse ? 'flex' : 'none'}
          p={10}
          br={10}
          backgroundColor={'$orange10Dark'}
          als="center"
          elevation={12}
        >
          <Paragraph color="$background">waiting for response</Paragraph>
        </YStack>
        <YStack bg="$gray9Dark" position="absolute" bottom={0} width={'100%'} padding={'$5'} space>
          <AudioRecorder onRecordingStart={handleSendAudioRequest} />
        </YStack>
        {openSideBar && (
          <YStack
            width={width}
            height={height}
            position="absolute"
            backgroundColor="rgba(0,0,0,0.2)"
            onPress={() => setOpenSideBar(false)}
          />
        )}
        {openSideBar && (
          <YStack
            width={width * 0.3}
            height={height}
            animation="lazy"
            backgroundColor="$blue12Dark"
            enterStyle={{ x: 300 }}
            $xs={{ zIndex: 15, position: 'absolute', top: 0, right: 0, width: width * 0.6 }}
            {...{ position: 'absolute', top: 0, right: 0, zIndex: 15 }}
          >
            {renderSideBar()}
          </YStack>
        )}
        {!openSideBar && (
          <YStack pos="absolute" top={20} right={15} cursor="pointer" onPress={() => setOpenSideBar(true)}>
            <SlidersHorizontal />
          </YStack>
        )}
      </YStack>
      <YStack position="absolute">
        <Link href="/">
          <SolitoImage src={logoPng} alt="Logo" height={60} width={60} style={{ borderRadius: 9999 }} />
        </Link>
      </YStack>
    </XStack>
  );
  function renderSideBar() {
    return (
      <ScrollView>
        <YStack space="$8" padding="$4">
          {openSideBar && (
            <YStack cur="pointer" onPress={() => setOpenSideBar(false)}>
              <ArrowRightSquare size="$2" />
            </YStack>
          )}
          {openSideBar && (
            <YStack>
              <SelectSimple jc="center" placeholder="Select a model" onValueChange={handleModelChange}>
                <Select.Item index={1} value="whisper-1">
                  <Select.ItemText>whisper_1</Select.ItemText>
                </Select.Item>
              </SelectSimple>
              {experiences.length > 0 && (
                <SelectSimple jc="center" placeholder="Select an operation" onValueChange={setExperience}>
                  {experiences.map((experience, index) => (
                    <Select.Item index={index} value={experience}>
                      <Select.ItemText>{experience}</Select.ItemText>
                    </Select.Item>
                  ))}
                </SelectSimple>
              )}
            </YStack>
          )}
        </YStack>
      </ScrollView>
    );
  }
}

export default AudioScreen;
