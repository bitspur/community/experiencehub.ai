import React from 'react';
import AudioScreen from '.'; // Ensure you're importing from the correct location

export default {
  title: 'screens/experiences/AudioScreen',
  component: AudioScreen,
  parameters: { status: { type: 'beta' } },
};

export const main = () => <AudioScreen />;
