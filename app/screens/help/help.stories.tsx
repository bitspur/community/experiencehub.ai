import React from 'react';
import HelpScreen from './index';

export default {
  title: 'screens/Help',
  component: HelpScreen,
  parameters: {
    status: {
      type: 'beta',
    },
  },
};

export const main = () => <HelpScreen />;
