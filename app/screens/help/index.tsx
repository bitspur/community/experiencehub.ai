import React, { useState } from 'react';
import { YStack, Input, Button, SimplePopover, Separator, Paragraph } from 'ui';
import { Header } from 'app/components/Header';

const HelpScreen = () => {
  const [input, setInput] = useState('');
  const handleSubmit = () => {
    setInput('');
  };

  return (
    <YStack space>
      <Header />
      <YStack p="$2.5" ai="center">
        <YStack space>
          <Paragraph mt="$16" fontSize="$8">
            Having Any Issues Please Write Us To <Separator />
            risserlabs@gmail.com
          </Paragraph>
          <Paragraph>
            We Are Constantly Trying To Improve The User Experience <Separator />
            Your FeedBack Is Really Important To Us...
          </Paragraph>
          <YStack space>
            <Input value={input} onChangeText={setInput} />
            <YStack>
              <SimplePopover trigger={<Button onPress={handleSubmit}>Submit</Button>}>
                Your feedback is submitted <Separator />
                successfully
              </SimplePopover>
            </YStack>
          </YStack>
        </YStack>
      </YStack>
    </YStack>
  );
};

export default HelpScreen;
