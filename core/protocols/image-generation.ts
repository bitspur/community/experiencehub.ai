/*
 *  File: /protocols/image-generation.ts
 *  Project: app
 *  File Created: 25-08-2023 12:01:37
 *  Author: Lalit Rajak
 *  -----
 * Last Modified: Tu-09-2023 18:02:52
 * Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { OpenAiImageGenerationProtocol } from './openai/image-generation';
import { ProxyImageGenerationProtocol } from './proxy/image-generation';
import { IChat } from 'app/screens/chat';
import { Protocol } from './protocol';
import { useEffect, useState } from 'react';
import { Environment } from 'app/environments';
import { models } from 'core/models';

export class ImageGenerationProtocol {
  static async getProtocolEnvironmentByModel(model?: string) {
    if (model) {
      const modelData = models.get(model);
      return modelData && this.getSupportedEnvironment(modelData.environment);
    }
    return this.getSupportedEnvironment('openai');
  }

  private static getSupportedEnvironment(environment: string) {
    switch (environment.toLowerCase()) {
      case 'openai':
        return new OpenAiImageGenerationProtocol();
      default:
        return new OpenAiImageGenerationProtocol();
    }
  }
}

export interface ImageGenerationProtocol {
  imageGenerator?(payload: ImageGenerationPayload, apiKey: string): Promise<string[]>;
}

export interface ImageGenerationPayload {
  prompt: string;
  n?: NumberOfImages;
  size?: ImageSize;
}

export enum ImageSize {
  'SMALL' = '256x256',
  'MEDIUM' = '512x512',
  'LARGE' = '1024x1024',
}

export enum NumberOfImages {
  'ONE' = 1,
  'TWO' = 2,
  'THREE' = 3,
  'FOUR' = 4,
  'FIVE' = 5,
  'SIX' = 6,
  'SEVEN' = 7,
  'EIGHT' = 8,
  'NINE' = 9,
  'TEN' = 10,
}
