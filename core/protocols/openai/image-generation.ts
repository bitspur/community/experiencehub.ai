/*
 *  File: /protocols/openai/image-generation.ts
 *  Project: app
 *  File Created: 25-08-2023 12:02:24
 *  Author: Lalit Rajak
 *  -----
 * Last Modified: Tu-09-2023 17:47:41
 * Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ImageGenerationProtocol, ImageGenerationPayload } from '../image-generation';
import axios from 'axios';

export class OpenAiImageGenerationProtocol implements ImageGenerationProtocol {
  async imageGenerator(payload: ImageGenerationPayload, apiKey: string): Promise<string[]> {
    return (
      await axios.post('https://api.openai.com/v1/images/generations', payload, {
        headers: {
          Authorization: `Bearer ${apiKey}`,
        },
      })
    ).data.data.map((url) => url.url);
  }
}
