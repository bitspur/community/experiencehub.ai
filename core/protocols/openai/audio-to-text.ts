/*
 *  File: /protocols/openai/audio-to-text.ts
 *  Project: app
 *  File Created: 01-09-2023 16:54:30
 *  File: /protocols/openai/audio-to-text.ts
 *  -----
 * Last Modified: Tu-10-2023 15:30:16
 * Modified By: Clay Risser
 *  -----
 * Last Modified: Tu-10-2023 15:30:16
 * Modified By: Clay Risser
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

// import { IAudioProtocol, AudioPayload } from 'core/protocols/audio-to-text';
// import axios from 'axios';

// export class OpenAIAudioToTextProtocol implements IAudioProtocol {
//   async audioTranslations(payload: AudioPayload, apiKey: string, modelName: string) {
//     const formData = new FormData();
//     if (payload.audioBlob) {
//       formData.append('file', payload.audioBlob, 'audio.wav');
//     }
//     formData.append('model', modelName);
//     return (
//       await axios.post('https://api.openai.com/v1/audio/translations', formData, {
//         headers: {
//           Authorization: `Bearer ${apiKey}`,
//           'Content-Type': 'multipart/form-data',
//         },
//       })
//     ).data;
//   }
// }

import { IAudioProtocol } from 'core/protocols/audio-to-text';
import axios from 'axios';

export class OpenAIAudioToTextProtocol implements IAudioProtocol {
  async audioTranslations(formData: FormData, apiKey: string, experience?: string): Promise<any> {
    console.log('formData', formData, 'apiKey', apiKey, 'experience', experience);
    return (
      await axios.post(`https://api.openai.com/v1/audio/${experience || 'translation'}`, formData, {
        headers: {
          Authorization: `Bearer ${apiKey}`,
          'Content-Type': 'multipart/form-data',
        },
      })
    ).data;
  }
}
