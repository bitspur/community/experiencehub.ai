/*
 *  File: /protocols/chat.ts
 *  Project: app
 *  File Created: 19-08-2023 11:59:52
 *  Author: Lalit Rajak
 *  -----
 * Last Modified: We-09-2023 10:18:15
 * Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { models } from 'core/models';
import { OpenAIChatProtocol } from './openai/chat';
import { LocalAIChatProtocol } from './localai/chat';

export class ChatProtocol {
  constructor() {}

  static async getProtocolEnvironmentByModel(model: string) {
    const modelData = models.get(model);
    return modelData && this.getSupportedEnvironment(modelData.environment);
  }

  private static getSupportedEnvironment(environment: string) {
    switch (environment.toLowerCase()) {
      case 'openai':
        return new OpenAIChatProtocol();
      case 'localai':
        console.log('calling localai');
        return new LocalAIChatProtocol();
      default:
        // return new OpenAIChatProtocol();
        console.log('calling proxy');
    }
  }
}

export interface IChatProtocol {
  chatCompletion: (payload: ChatPayload, apiKey?: string) => Promise<any>;
}

export interface ChatPayload {
  model?: string;
  temperature?: number;
  messages?: { role: string; content: string }[];
}
