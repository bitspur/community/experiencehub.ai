/*
 *  File: /protocols/proxy/audio-to-text.ts
 *  Project: app
 *  File: /protocols/proxy/audio-to-text.ts
 *  Author: Lalit Rajak
 *  -----
 *  Last Modified: 01-09-2023 17:17:59
 *  Modified By: Lalit Rajak
 *  Last Modified: 01-09-2023 17:17:59
 *  Modified By: Lalit Rajak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { IAudioToTextProtocol } from '../audio-to-text';
export class ProxyAudioToTextProtocol implements IAudioToTextProtocol {}
