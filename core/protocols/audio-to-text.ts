/*
 *  File: /protocols/audio-to-text.ts
 *  Project: core
 *  File Created: 03-10-2023 09:32:56
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

// /*
//  *  File: /protocols/audio-to-text.ts
//  *  Project: app
//  *  File Created: 01-09-2023 16:05:50
//  *  Author: Lalit Rajak
//  *  -----
//  *  Last Modified: 01-09-2023 20:24:27
//  *  Modified By: Lalit Rajak
//  *  -----
//  *  Your Company (c) Copyright 2023
//  *
//  *  Licensed under the Apache License, Version 2.0 (the "License");
//  *  you may not use this file except in compliance with the License.
//  *  You may obtain a copy of the License at
//  *
//  *      http://www.apache.org/licenses/LICENSE-2.0
//  *
//  *  Unless required by applicable law or agreed to in writing, software
//  *  distributed under the License is distributed on an "AS IS" BASIS,
//  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  *  See the License for the specific language governing permissions and
//  *  limitations under the License.
//  */

// import { useEffect, useState } from 'react';
// import { Environment } from 'app/environments';
// import { OpenAiAudioToTextProtocol } from './openai/audio-to-text';
// import { LocalAIAudioToTextProtocol } from './localai/audio-to-text';
// import { ProxyAudioToTextProtocol } from './proxy/audio-to-text';
// import { IChat } from 'app/screens/chat';

// export class AudioToTextProtocol implements IAudioToTextHandler {
//   constructor() {}
//   async autoGenerate(environment: Environment, config: FormData): Promise<IChat | undefined> {
//     const protocol = this.getProtocol(environment, config);
//     if (!protocol.generateText) return;
//     const response = await protocol.generateText();
//     return {
//       id: response.id,
//       prompt: response.text,
//       role: 'bot',
//       experience: 'text-generation',
//     };
//   }

//   protected getProtocol(environment: Environment, config: FormData): IAudioToTextProtocol {
//     switch (environment.name) {
//       case 'openai':
//         return new OpenAiAudioToTextProtocol(environment, config);
//       case 'localai':
//         return new LocalAIAudioToTextProtocol(environment, config);
//       default:
//         return new ProxyAudioToTextProtocol();
//     }
//   }
// }

// export function useAudioToTextProtocol() {
//   const [protocol, setProtocol] = useState<IAudioToTextHandler | undefined>(undefined);
//   useEffect(() => {
//     setProtocol(new AudioToTextProtocol());
//   }, []);
//   return protocol;
// }

// export interface IAudioToTextProtocol {
//   generateText?(): Promise<any | undefined>;
// }

// export interface IAudioToTextHandler {
//   autoGenerate(environment: Environment, config: FormData): Promise<any | undefined>;
// }

import { models } from 'core/models';
import { OpenAIAudioToTextProtocol } from './openai/audio-to-text';
import { LocalAIAudioToTextProtocol } from './localai/audio-to-text';

export class AudioProtocol {
  constructor() {}

  static getProtocolEnvironmentByModel(model?: string) {
    if (model) {
      const modelData = models.get(model);
      console.log('modelData', modelData);
      return modelData && this.getSupportedEnvironment(modelData.environment);
    }
    return this.getSupportedEnvironment('openai');
  }

  private static getSupportedEnvironment(environment: string) {
    switch (environment.toLowerCase()) {
      case 'openai':
        return new OpenAIAudioToTextProtocol();
      case 'localai':
        return new LocalAIAudioToTextProtocol();
      default:
        return new OpenAIAudioToTextProtocol();
    }
  }
}

export interface IAudioProtocol {
  audioTranslations(formData: FormData, apiKey: string, experience: string): Promise<any>;
}

export interface AudioPayload {
  model?: string;
  id?: string;
  role?: string;
  prompt?: string;
  audioBlob: Blob | null;
  responseText?: string | null;
}
