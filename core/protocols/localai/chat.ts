/*
 *  File: /protocols/localai/chat.ts
 *  Project: app
 *  File Created: 19-08-2023 12:01:58
 *  Author: Lalit Rajak
 *  -----
 * Last Modified: Th-09-2023 12:40:39
 * Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ChatPayload, IChatProtocol } from '../chat';
import axios from 'axios';

export class LocalAIChatProtocol implements IChatProtocol {
  async chatCompletion(payload: ChatPayload) {
    return (await axios.post('http://localhost:9999/v1/chat/completions', payload)).data;
  }
}
