/*
 *  File: /protocols/localai/audio-to-text.ts
 *  Project: app
 *  File Created: 01-09-2023 17:08:54
 *  Author: Lalit Rajak
 *  -----
 * Last Modified: Tu-10-2023 15:11:01
 * Modified By: Clay Risser
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { IAudioProtocol, AudioPayload } from '../audio-to-text';
import axios from 'axios';

export class LocalAIAudioToTextProtocol implements IAudioProtocol {
  async audioTranslations(payload: FormData, apiKey?: string) {
    const response = await axios.post('http://localhost:9999/v1/audio/translations', payload);
    return response.data;
  }
}
