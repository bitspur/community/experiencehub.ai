import pkg from '../../package.json';
import type { NestExpressApplication } from '@nestjs/platform-express';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { patchNestJsSwagger } from 'nestjs-zod';

patchNestJsSwagger();

export async function registerSwagger(app: NestExpressApplication) {
  const config = app.get(ConfigService);
  const scopes = [
    ...new Set([
      'openid',
      ...(config.get('KEYCLOAK_SCOPES') || '')
        .split(' ')
        .filter((scope: string) => scope),
    ]),
  ];
  if (config.get('SWAGGER') === '1' || config.get('DEBUG') === '1') {
    const options = new DocumentBuilder()
      .setTitle(pkg.name)
      .setDescription(pkg.description)
      .setVersion(pkg.version)
      .addOAuth2({
        name: 'Keycloak',
        type: 'oauth2',
        flows: {
          implicit: {
            authorizationUrl: `${config.get(
              'KEYCLOAK_BASE_URL',
            )}/realms/${config.get(
              'KEYCLOAK_REALM',
            )}/protocol/openid-connect/auth?nonce=1`,
            scopes: scopes.reduce(
              (scopes: Record<string, string | boolean>, scope: string) => {
                scopes[scope] = true;
                return scopes;
              },
              {},
            ),
          },
        },
      })
      .addBearerAuth()
      .addCookieAuth()
      .build();
    const openApiObject = SwaggerModule.createDocument(app, options);
    const clientSecret = config.get('KEYCLOAK_CLIENT_SECRET');
    SwaggerModule.setup(
      config.get('SWAGGER_BASE_PATH') || '/docs',
      app,
      openApiObject,
      {
        // customJs: '/swagger.js',
        swaggerOptions: {
          persistAuthorization: true,
          initOAuth: {
            ...(clientSecret ? { clientSecret } : {}),
            appName: pkg.name,
            clientId: config.get('KEYCLOAK_CLIENT_ID'),
            scopes,
          },
        },
      },
    );
  }
}
