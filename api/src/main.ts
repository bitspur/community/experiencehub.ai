import { start } from 'app/bootstrap';

(async () => {
  await start();
})();
