/*
 *  File: /src/modules/experiences/imageGeneration/ImageGeneration.dto.ts
 *  Project: experiencehub-api
 *  File Created: 15-09-2023 18:14:09
 *  Author: dharmendra
 *  -----
 *  BitSpur (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';

const GenerateImageRequestPayloadSchema = z.object({
  n: z.number().default(1),
  prompt: z.string().default('A photo of a cat'),
  size: z.string().default('1024x1024'),
});

export class GenerateImageRequestPayloadDto extends createZodDto(
  GenerateImageRequestPayloadSchema,
) {}
