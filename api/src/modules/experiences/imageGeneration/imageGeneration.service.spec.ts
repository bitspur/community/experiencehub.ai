/*
 *  File: /src/modules/experiences/imageGeneration/imageGeneration.service.spec.ts
 *  Project: experiencehub-api
 *  File Created: 15-09-2023 18:10:36
 *  Author: dharmendra
 *  -----
 *  BitSpur (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Test, TestingModule } from '@nestjs/testing';
import { ImageGenerationService } from './imageGeneration.service';

describe('ImageGenerationService', () => {
  let service: ImageGenerationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ImageGenerationService],
    }).compile();

    service = module.get<ImageGenerationService>(ImageGenerationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
