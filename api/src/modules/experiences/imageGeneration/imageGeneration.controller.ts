/*
 *  File: /src/modules/experiences/imageGeneration/imageGeneration.controller.ts
 *  Project: experiencehub-api
 *  File Created: 16-09-2023 09:44:17
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Body, Controller, Post } from '@nestjs/common';
import { GenerateImageRequestPayloadDto } from './ImageGeneration.dto';
import { ImageGenerationService } from './imageGeneration.service';

@Controller('experience/image-generation')
export class ImageGenerationController {
  constructor(
    private readonly imageGenerationService: ImageGenerationService,
  ) {}

  @Post()
  async generateImage(@Body() body: GenerateImageRequestPayloadDto) {
    return this.imageGenerationService.generateImage(body);
  }
}
