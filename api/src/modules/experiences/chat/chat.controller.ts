/*
 *  File: /src/modules/experiences/chat/chat.controller.ts
 *  Project: experiencehub-api
 *  File Created: 14-09-2023 07:30:17
 *  Author: Clay Risser
 *  -----
 *  Last Modified: 15-09-2023 05:53:59
 *  Modified By: Clay Risser
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Body, Controller, Post } from '@nestjs/common';
import { GenerateTextRequestPayloadDto } from './chat.dto';
import { ChatService } from './chat.service';

@Controller('experience/chat')
export class ChatController {
  constructor(private readonly chatService: ChatService) {}

  @Post()
  async generateText(@Body() body: GenerateTextRequestPayloadDto) {
    return this.chatService.generateText(body);
  }
}
