/*
 *  File: /src/modules/experiences/chat/chat.dto.ts
 *  Project: experiencehub-api
 *  File Created: 14-09-2023 07:00:35
 *  Author: Clay Risser
 *  -----
 *  Last Modified: 15-09-2023 12:24:00
 *  Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';

const MessageDto = z.object({
  role: z.string().default('user'),
  content: z.string(),
});

const GenerateTextRequestPayloadSchema = z.object({
  model: z.string().default('gpt-3.5-turbo'),
  messages: z.array(MessageDto),
  temperature: z.number().optional(),
});

export class GenerateTextRequestPayloadDto extends createZodDto(
  GenerateTextRequestPayloadSchema,
) {}
