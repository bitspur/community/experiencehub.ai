/*
 *  File: /src/modules/experiences/chat/chat.service.ts
 *  Project: experiencehub-api
 *  File Created: 15-09-2023 06:24:16
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Injectable } from '@nestjs/common';
import { GenerateTextRequestPayloadDto } from './chat.dto';
import { ChatProtocol } from 'core/protocols/chat';
import sharp from 'sharp';
import https from 'https';
import fs from 'fs';

@Injectable()
export class ChatService {
  private apiKey = process.env.OPENAI_API_KEY;
  async generateText(payload: GenerateTextRequestPayloadDto) {
    // const protocol = await ChatProtocol.getProtocolEnvironmentByModel(
    //   payload.model,
    // );

    // return protocol && protocol.chatCompletion(payload, this.apiKey as string);

    // async function convertImageToRGBA(
    //   inputPath: string,
    //   outputPath: string,
    // ): Promise<void> {
    //   try {
    //     await sharp(inputPath)
    //       .ensureAlpha() // Ensure there's an alpha channel (transparency) in the output
    //       .toFile(outputPath);
    //     console.log(`Image converted and saved to ${outputPath}`);
    //   } catch (error) {
    //     console.error('Error converting the image:', error);
    //   }
    // }

    // // Usage
    // const inputImagePath = '/home/dboddeda/test.png';
    // const outputImagePath = '/home/dboddeda/image_rgba.png';

    // convertImageToRGBA(inputImagePath, outputImagePath);

    // const imageUrl =
    //   'https://oaidalleapiprodscus.blob.core.windows.net/private/org-jwLZs0441pYsEFFlJToIscQh/user-uK6wsqOtcc6TYiUyNFdNQabW/img-iwzEVkIjkvSsHgjZxFgq8F8y.png';
    // const file = fs.createWriteStream('image.png');
    // https
    //   .get(imageUrl, (response) => {
    //     response.pipe(file);

    //     file.on('finish', () => {
    //       file.close();

    //       console.log('Image downloaded successfully.');
    //     });
    //   })
    //   .on('error', (error) => {
    //     console.error('Error downloading image:', error);
    //   });

    return payload;
  }
}
