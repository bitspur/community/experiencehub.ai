/*
 *  File: /src/app.module.ts
 *  Project: experiencehub-api
 *  File Created: 14-09-2023 07:00:35
 *  Author: Clay Risser
 *  -----
 * Last Modified: Sa-09-2023 09:27:04
 * Modified By: dharmendra
 *  -----
 *  Your Company (c) Copyright 2023
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import type { DynamicModule } from '@nestjs/common';
import path from 'path';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Global, Module } from '@nestjs/common';
import modules from './modules';
import coreModules from './modules/core';
import { ZodValidationPipe } from 'nestjs-zod';
import { APP_PIPE } from '@nestjs/core';

@Global()
@Module({
  imports: [],
})
export class AppModule {
  public static register(_config: RegisterAppModuleConfig = {}): DynamicModule {
    return {
      global: true,
      controllers: [],
      imports: [
        ConfigModule.forRoot({
          envFilePath: path.resolve(process.cwd(), '../.env'),
        }),
        ...coreModules,
        ...modules,
      ],
      module: AppModule,
      providers: [
        ConfigService,
        {
          provide: APP_PIPE,
          useClass: ZodValidationPipe,
        },
      ],
      exports: [ConfigService],
    };
  }
}

export interface RegisterAppModuleConfig {}
