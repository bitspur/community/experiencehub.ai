export { getServerSideProps } from 'multiplatform.one/next';
import ZeroShotClassification from 'app/screens/nlp/zeroShotClassification';

export default ZeroShotClassification;
