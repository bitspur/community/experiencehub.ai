export { getServerSideProps } from 'multiplatform.one/next';

import Conversational from 'app/screens/nlp/conversational';

export default Conversational;
