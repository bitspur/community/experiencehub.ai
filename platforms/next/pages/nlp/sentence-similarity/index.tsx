export { getServerSideProps } from 'multiplatform.one/next';

import SentenceSimilarity from 'app/screens/nlp/sentenceSimilarity';

export default SentenceSimilarity;
