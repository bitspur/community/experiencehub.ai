export { getServerSideProps } from 'multiplatform.one/next';
import TextGeneration from 'app/screens/nlp/textGeneration';

export default TextGeneration;
