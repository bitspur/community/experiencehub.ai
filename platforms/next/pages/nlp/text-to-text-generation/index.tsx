export { getServerSideProps } from 'multiplatform.one/next';

import TextToTextGeneration from 'app/screens/nlp/textToTextGeneration';

export default TextToTextGeneration;
