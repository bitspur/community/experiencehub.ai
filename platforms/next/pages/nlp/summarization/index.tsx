export { getServerSideProps } from 'multiplatform.one/next';
import Summarization from 'app/screens/nlp/summarization';

export default Summarization;
