export { getServerSideProps } from 'multiplatform.one/next';
import TextClassification from 'app/screens/nlp/textClassification';

export default TextClassification;
