export { getServerSideProps } from 'multiplatform.one/next';

import TokenClassification from 'app/screens/nlp/tokenClassification';

export default TokenClassification;
