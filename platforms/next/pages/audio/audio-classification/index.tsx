export { getServerSideProps } from 'multiplatform.one/next';
import AudioClassification from 'app/screens/audio/audioClassification';

export default AudioClassification;
