export { getServerSideProps } from 'multiplatform.one/next';

import TextToSpeech from 'app/screens/audio/textToSpeech';

export default TextToSpeech;
