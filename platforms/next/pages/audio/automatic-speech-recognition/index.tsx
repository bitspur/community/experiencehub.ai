export { getServerSideProps } from 'multiplatform.one/next';

import AutomaticSpeechRecognition from 'app/screens/audio/automaticSpeechRecognition';

export default AutomaticSpeechRecognition;
