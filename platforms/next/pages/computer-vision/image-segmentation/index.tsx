export { getServerSideProps } from 'multiplatform.one/next';
import ImageSegmentation from 'app/screens/computerVision/imageSegmentation';

export default ImageSegmentation;
