export { getServerSideProps } from 'multiplatform.one/next';
import ZeroShotImageClassification from 'app/screens/computerVision/zeroShotImageClassification';

export default ZeroShotImageClassification;
