export { getServerSideProps } from 'multiplatform.one/next';

import ObjectDetection from 'app/screens/computerVision/objectDetection';

export default ObjectDetection;
