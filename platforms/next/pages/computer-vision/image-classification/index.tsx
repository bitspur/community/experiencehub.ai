export { getServerSideProps } from 'multiplatform.one/next';

import ImageClassification from 'app/screens/computerVision/imageClassification';

export default ImageClassification;
